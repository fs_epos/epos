/**
*	Contact Information, Contact Information Data Provider
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerProfileDataProvider = function() {
		this.options = $.extend({}, CustomerProfileDataProvider.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.CustomerProfilePanelView';
	}
	
	//Define default attributes
	CustomerProfileDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	CustomerProfileDataProvider.prototype = {
		constructor : CustomerProfileDataProvider,
		retrieveContactInfo: function(queryObject, successCallback, failureCallback){
			// cordova call
			cordova.exec(
	    		function(data){
	    			successCallback(data);
	    		},
	    		function(error){
	    			failureCallback(error);
	    		},
	    		this.PLUGIN_NAME,
	    		'retrieveContactInfo',
	            (queryObject ? queryObject : [])
	    	);
		},
		retrievePersonalInfo: function(queryObject, successCallback, failureCallback){
			// cordova call
			cordova.exec(
	    		function(data){
	    			successCallback(data);
	    		},
	    		function(error){
	    			failureCallback(error);
	    		},
	    		this.PLUGIN_NAME,
	    		'retrievePersonalInfo',
	            (queryObject ? queryObject : [])
	    	);
		}	
	}
	//Register the object inside ePOS
	ePos.widget.CustomerProfileDataProvider = CustomerProfileDataProvider;
}(ePos, $));