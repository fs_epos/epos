/**
*	PICS, PICS Event Listener from the Dashboard
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var PICSEventListener = function() {
		this.options = $.extend({}, PICSEventListener.DEFAULTS);
	}
	//Define default attributes
	PICSEventListener.DEFAULTS = {
		initializeData: true
	}
	
	PICSEventListener.prototype = {
		constructor : PICSEventListener,
		onSavePICSInfo: function(information) {
			//phonegap
			console.log('onSavePICSInfo data: ' + JSON.stringify(information));
		}			
	}	
	//Register the object inside ePOS
	ePos.widget.PICSEventListener = PICSEventListener;
}(ePos, $));

