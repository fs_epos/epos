/**
*	Dashboard, Customer List Panel View
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerListPanelView = function(widgetReference, options, callback) {
		// used the current element instace if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $)? widgetReference: $('.' + widgetReference);
		this.dataprovider = new ePos.widget.CustomerListDataProvider();
		this.customerRenderer = new ePos.widget.CustomerListRenderer();
		this.eventListener = new ePos.widget.CustomerListEventListener();
		this.options = $.extend({}, CustomerListPanelView.DEFAULTS, options);
		this.callback = callback;
		this._buildInitialDOM();
	}
	
	//Define default attributes
	CustomerListPanelView.DEFAULTS = {
		initializeData: true
	}
	
	CustomerListPanelView.prototype = {
		constructor : CustomerListPanelView,
		options: null,
		$widget: null,
		callback: null,
		
		//html template
		_template: 	'<div class="customer-lists"></div>',
		
		_buildInitialDOM: function() {
			this.$widget.append(this._template);
			if(this.options.initializeData)
				this.loadData();
		},
		
		loadData: function() {
			var _self = this;
			if(typeof this.dataprovider.getCustomerList == 'function') {
				this.dataprovider.getCustomerList(null, 
				function(data) {
					$(data).each(function(index, cust){
						var $clickCustomer = _self.customerRenderer.buildCustomerList(cust);
						$(".customer-lists", self.$widget).append($clickCustomer);
						$clickCustomer.on('click', function(evt){
							console.log(cust);
	    					_self.eventListener.onOpenCustomerProfile(cust);
	    				});
					});
				}, 
				function(error){
					
				}
				);
				
			}
		}
		
	}
	//Register the object inside ePOS
	ePos.widget.CustomerListPanelView = CustomerListPanelView;
}(ePos, $));