//
//  CustomerListCDVPlugin.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import "CustomerListCDVPlugin.h"
#import "CustomerDetailsDAO.h"

@implementation CustomerListCDVPlugin

+ (NSString *)pluginName {
    return @"ePos.widget.CustomerListDataProvider";
}

- (void)getCustomerList:(CDVInvokedUrlCommand *)command {
    
    CustomerDetailsDAO *customerListDAO = [CustomerDetailsDAO sharedPersistenceStore];
    NSArray *customers = [customerListDAO loadAllCustomers];
    
    NSLog(@"sending Customers %@",customers);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:customers] callbackId:command.callbackId];

}

@end
