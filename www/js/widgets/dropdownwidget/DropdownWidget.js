/**
* Dropdown widget
*/
var dropDownWidget = function(widgetReference) {
	this.$widget = (widgetReference instanceof $)? widgetReference: $('#' + widgetReference);
	this.settings = {};
	
	this.init = function(selections, name) {
		var _self = this;
		var defaults = {
			onBeforeInit: null,
			onAfterInit: null,
			onClick: function(){
				_self.onClickDefault();
			},
			/**
			 * options must be in this format
			 * [{value:"", label:""}]
			 * 
			 * e.g.
			 * [
			 * {value: '1', label: 'Italy'},
			 * {value: '2', label: 'Philippines'},
			 * ...
			 * ]
			 */
			options: selections,
			selected: '',
			placeholder: name,
			selectClass: ''
		}
		this.settings = defaults;
		
		this.selectedOption = null;
		
		this._buildInitialDOM();
	};
	this._template = '<div class="dropdownValue"></div>';
		
	this._buildInitialDOM = function() {
		var _self = this;
		this.$widget.append(this._template);
		
		if(typeof this.settings.onBeforeInit === 'function'){
			this.settings.onBeforeInit();
		}		
		
		this.$select = $('div.dropdownValue', this.$widget).text(this.settings.placeholder);
		this.$select.addClass(this.settings.selectClass);
		
		// create the options for the custom select
		this.$option = $('<ul></ul>');
		this.$option.hide();
		
		this._populateList();
		
		this.$widget.append(this.$option);
		
		if(typeof this.settings.onAfterInit === 'function'){
			this.settings.onAfterInit();
		}
		
		this.$select.on('click', function(evt){
			if(!_self.$widget.hasClass('disabled')){
				if(!_self.$widget.hasClass('isDropdownActive')){
					$('.isDropdownActive ul').hide();
					$('.dropdownListView').removeClass('isDropdownActive');
				}
				_self.$option.stop( true, true ).slideToggle();
				_self.$option.parent().toggleClass('isDropdownActive');
				
				return false;
			} else {
				 evt.preventDefault();
				 evt.stopPropagation();
			}
		});
		
		this.setSelected(this.settings.selected);
		this._initDomListener();
	};
	
	this.onClickDefault = function(){
		var $target = $(event.target);
		var data = $target.data('option');
		this.setSelected(data.label);
		$target.siblings().removeAttr('class');
		$target.addClass('selected');
		this.$option.hide();
	};
	 
	this._populateList = function(){
		var _self = this;
		$(this.settings.options).each(function(indx, option){
			var $li = '', $selected = '';
			$li = $('<li value="'+ option.value +'"' + $selected + '>' + option.label + '</li>');
			if(_self.settings.selected.trim().toLowerCase() == option.label.trim().toLowerCase()){
				$li.addClass('selected');
			}
			$li.data('option',option);
			_self.$option.append($li);
			
			// add an event listener for each option
			$li.on('click', function(evt){
				_self.settings.onClick();
			});
		});
	};
	
	this.setSelected = function(value){
		var _self = this;
		$(this.settings.options).each(function(indx, option){
			if(option.label.toLowerCase() == value.toLowerCase()){
				_self.$select.text(value);
				_self.selectedOption = option;
				_self.$option.find('li').removeClass('selected');
				_self.$option.children('li:contains("'+ value +'")').addClass('selected');
				return false;
			}
		});
	};
	
	this.setSelectedByValue = function(value){
		var _self = this;
		$(this.settings.options).each(function(indx, option){
			if(String(option.value).toLowerCase() == String(value).toLowerCase()){
				_self.$select.text(option.label);
				_self.selectedOption = option;
				_self.$option.find('li').removeClass('selected');
				_self.$option.children('li[value="'+ value +'"]').addClass('selected');
				return false;
			}
		});
	};
	
	this.getSelected = function(){
		return this.selectedOption;
	};
	
	this._initDomListener = function(){
		var _self = this;
				
		$('body').on('click', function(evt){
			$('.dropdownListView').removeClass('isDropdownActive');
			_self.$select.siblings('ul').each(function(indx, ul){
				if($(ul).is(':visible') && $(ul).parent('div').hasClass('isDropdownActive') == false) $(ul).hide();
			});
		});
	};
};
