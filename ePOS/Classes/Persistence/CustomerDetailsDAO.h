//
//  PersistenceStore.h
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CustomerDetails.h"

@interface CustomerDetailsDAO : NSObject

+(instancetype) sharedPersistenceStore;

//Create customer
-(CustomerDetails *)createCustomerDetails:(NSDictionary*) customerDetails;

//Update customer
- (CustomerDetails *)updateCustomerDetails:(NSDictionary *)customerDetails error:(NSError *)error;

//Get customer managed object
- (CustomerDetails *)getCustomerWithCustomerId:(NSString *)customerId;

//Remove customer
-(void) removeCustomerWithId:(NSString*) customerId;

//retrieveCustomerByID
-(NSDictionary *)retrieveCutomerWithCustomerId:(NSString*)customerId;

//Load all customers @json
-(NSArray*) loadAllCustomers;

//Load all customers @MO
-(NSArray*) loadAllCustomersMO;

//Intialize data in the db upon first launch
-(void) createInitialCustomerList;

-(NSDictionary*) encodeManagedObjectToJSon:(CustomerDetails*) objects;

@end
