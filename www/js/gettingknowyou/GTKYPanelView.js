/**
*	GTKY - Getting to Know You
*	Copyright Accenture Inc., ePOS 2015
*	@author Ma. Victoria Ocay
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';
	function GTKYPanelView(widgetReference, options, callback) {
		//use the current element instance if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.dataProvider = new ePos.widget.GTKYDataProvider();
		this.eventlistener = new ePos.widget.GTKYEventListener();
		this.validation = new ePos.widget.ValidationUtility();
		this.options = $.extend({}, GTKYPanelView.DEFAULTS, options);
		this._generalInformation = {};
		this._country = [];
		this.callback = callback;
		this._buildInitialDOM();
	}
	
	//Define default attributes
	GTKYPanelView.DEFAULTS = {
		initializeData: true
	}
	
	GTKYPanelView.prototype = {
		constructor : GTKYPanelView,
		options: null,
		$widget: null,
		callback: null,
		
		//html template
		_template: //'<div id="gtkycontent">'
						'<div id="tabs">'
			+				'<ul>'
			+					'<li><a id="genInfo" href="#tabs-1"></a></li>'
			+					'<li><a id="finInfo" href="#tabs-2"></a></li>'
			+				'</ul>'
			+ 				'<div id="tabs-1"></div>'
			+ 				'<div id="tabs-2"></div>' 
			+			'</div>'
			//+		'</div>'
		,
		
		_genInfoTemplate1: '<div class="general-information-form">'
				+'		<div id="genInfoFirstPanelView" class="general-information-view">'
				+					'<div class="personal-info">'
				+					'<h4 class="personal-info-heading"></h4>'
				+					'<div class="personal-info-content">'
				+						'<div class="personalInfo-surname clearfix">'
				+							'<span class="personalInfo-surname-label"></span> '
				+							'<span class="personalInfo-surname-value text-value"> </span>'
				+						'</div>'
				+						'<div class="personalInfo-givenName clearfix">'
				+							'<span class="personalInfo-givenName-label"></span> '
				+							'<span class="personalInfo-givenName-value text-value"> </span>'
				+						'</div>'
				+						'<div class="personalInfo-gender clearfix">'
				+							'<span class="personalInfo-gender-label"></span> '
				+							'<span class="personalInfo-gender-value text-value">'
				+							'<input type="radio" id="view-male" data-name="Gender" name="gender" data-req="1" value="MALE"/>'
				+							'<label for="view-male">Male</label>'
				+ 							'<input type="radio" id="view-female" data-name="Gender" name="gender" data-req="1" value="FEMALE"/>'
				+							'<label for="view-female">Female</label>'
				+							'</span>'
				+						'</div>'
				+						'<div class="personalInfo-DOB clearfix">'
				+							'<span class="personalInfo-DOB-label"></span> '
				+							'<span class="personalInfo-DOB-value text-value"> </span>'
				+						'</div>'
				+						'<div class="personalInfo-age clearfix">'
				+							'<span class="personalInfo-age-label"></span> '
				+							'<span class="personalInfo-age-value text-value"> </span>'				
				+						'</div>'
				+					'</div>'
				+					'</div>'
				+					'<div class="contact-info">' 
				+					'<h4 class="contact-info-heading"></h4>'
				+					'<div class="contact-info-content">'
				+						'<div class="contactInfo-mobile clearfix">'
				+							'<span class="contactInfo-mobile-label"></span> '
				+							'<span class="contactInfo-mobile-value text-value"> </span>'
				+						'</div>'
				+						'<div class="contactInfo-email clearfix">'
				+							'<span class="contactInfo-email-label"></span> '
				+							'<span class="contactInfo-email-value text-value"> </span>'
				+						'</div>'
				+						'<div class="contactInfo-country clearfix">'
				+							'<span class="contactInfo-country-label"></span> '
				+							'<span class="contactInfo-country-value text-value"> </span>'
				+						'</div>'
				+						'<div class="contactInfo-homeAddress clearfix">'
				+							'<span class="contactInfo-homeAddress-label"></span> '
				+							'<span class="contactInfo-homeAddress-value text-value"> </span>'
				+						'</div>'
				+						'<div class="contactInfo-workAddress clearfix">'
				+							'<span class="contactInfo-workAddress-label"></span> '
				+							'<span class="contactInfo-workAddress-value text-value"> </span>'
				+						'</div>'
				+					'</div>'
				+					'</div>' 
				+'		</div>'
		+					'<div id="genInfoFirstPanelEdit" class="general-information-edit">'
		+					'<div class="personal-info">'
		+					'<h4 class="personal-info-heading"></h4>'
		+					'<div class="personal-info-content">'
		+						'<div class="personalInfo-surname clearfix">'
		+							'<span class="personalInfo-surname-label"></span> '
		+							'<span class="personalInfo-surname-value text-value"> <input data-req="1" type="text" data-name="Surname"/> </span>'
		+						'</div>'
		+						'<div class="personalInfo-givenName clearfix">'
		+							'<span class="personalInfo-givenName-label"></span> '
		+							'<span class="personalInfo-givenName-value text-value"><input data-req="1" type="text" data-name="Given Name"/>'
		+						'</div>'
		+						'<div class="personalInfo-gender clearfix">'
		+							'<span class="personalInfo-gender-label"></span> '
		+							'<span class="personalInfo-gender-value text-value">'
		+							'<input type="radio" id="male-edit" data-name="Gender" name="gender" data-req="1" value="MALE"/>'
		+							'<label for="male-edit">Male</label>'
		+ 							'<input type="radio" id="female-edit" data-name="Gender" name="gender" data-req="1" value="FEMALE"/>'
		+							'<label for="female-edit">Female</label>'
		+							'</span>'
		+						'</div>'
		+						'<div class="personalInfo-DOB clearfix">'
		+							'<span class="personalInfo-DOB-label"></span> '
		+							'<span class="personalInfo-DOB-value text-value"> <input data-req="1" type="date" data-name="Date of Birth"/></span>'
		+						'</div>'
		+						'<div class="personalInfo-age clearfix">'
		+							'<span class="personalInfo-age-label"></span> '
		+							'<span class="personalInfo-age-value text-value"><input data-req="0" type="text" data-name="Age"/></span>'
		+						'</div>'
		+					'</div>'
		+					'</div>'
		+					'<div class="contact-info">' 
		+					'<h4 class="contact-info-heading"></h4>'
		+					'<div class="contact-info-content">'
		+						'<div class="contactInfo-mobile clearfix">'
		+							'<span class="contactInfo-mobile-label"></span> '
		+							'<span class="contactInfo-mobile-value text-value"><input data-req="1" type="tel" data-name="Mobile Number"/></span>'
		+						'</div>'
		+						'<div class="contactInfo-email clearfix">'
		+							'<span class="contactInfo-email-label"></span> '
		+							'<span class="contactInfo-email-value text-value"><input data-req="1" type="email" data-name="Email Address"/></span>'
		+						'</div>'
		+						'<div class="contactInfo-country clearfix">'
		+							'<span class="contactInfo-country-label"></span> '
		+							'<span class="contactInfo-country-value text-value"> <div id="contactInfo-country" class="dropdown"></div> </span>'
		+						'</div>'
		+						'<div class="contactInfo-homeAddress clearfix">'
		+							'<span class="contactInfo-homeAddress-label"></span> '
		+							'<span class="contactInfo-homeAddress-value text-value"><input data-req="1" type="textarea" data-name="Home Address"/></span>'
		+						'</div>'
		+						'<div class="contactInfo-workAddress clearfix">'
		+							'<span class="contactInfo-workAddress-label"></span> '
		+							'<span class="contactInfo-workAddress-value text-value"><input type="textarea" data-name="Work Address"/></span>'
		+						'</div>'
		+					'</div>'
		+					'</div>' 
		+					'</div>'
		+					'<div class="general-information-buttons">'
		+'					<span class="button-span button-edit"> <button id = "button-edit-contact-information" class="button positive" /></span>'
		+'					<span class="button-span button-save"> <button id = "button-save-contact-information" class="button positive" /></span>'
		+'					<span class="button-span button-cancel"> <button id = "button-cancel-contact-information" class="button positive" /></span>'
		+'					</div>'
		+					'</div>' ,
		
		
		_genInfoDependentViewTemplate: 
				'<div id="genInfoSecondPanelView" class="general-information-view">'
		+			'<div class="dependents-info">'
		+				'<div class="dependent-header row">'
		+					'<span class="dependents-info-heading"></span>'
		+					'<span class="dependent-number"></span>'
		+				'</div>'
		+				'<div class="dependents-info-content">'
		+					'<div id="child-tabs-view">'
		+						'<ul>'
		+							'<li class="child-tabs-" id="child-tabs-1-view"><a href="#child-tabs-1-content-view">1st Child</a></li>'
		+							'<li class="child-tabs-" id="child-tabs-2-view"><a href="#child-tabs-2-content-view">2nd Child</a></li>'
		+							'<li class="child-tabs-" id="child-tabs-3-view"><a href="#child-tabs-3-content-view">3rd Child</a></li>'
		+							'<li class="child-tabs-" id="child-tabs-4-view"><a href="#child-tabs-4-content-view">4th Child</a></li>'
		+						'</ul>'
		+ 						'<div class="child-tabs-content child-view" id="child-tabs-1-content-view"></div>'
		+ 						'<div class="child-tabs-content child-view" id="child-tabs-2-content-view"></div>'
		+ 						'<div class="child-tabs-content child-view" id="child-tabs-3-content-view"></div>'
		+ 						'<div class="child-tabs-content child-view" id="child-tabs-4-content-view"></div>'
		+					'</div>'
		+				'</div>'
		+			'</div>'
		+		'</div>',
			
		_genInfoDependentEditTemplate: 
				'<div id="genInfoSecondPanelEdit" class="general-information-edit">'
		+			'<div class="dependents-info">'
		+				'<h4 class="dependents-info-heading"></h4>'
		+				'<div class="dependents-info-content">'
		+ 					'<div id="numOfChildren">'
		+						'<input type="radio" id="0" name="numOfChildren" value="0" class="dependents-numOfChildren-value"/>'
		+						'<label for="0">0</label>'
		+						'<input type="radio" id="1" name="numOfChildren" value="1" class="dependents-numOfChildren-value"/>'
		+						'<label for="1">1</label>'
		+ 						'<input type="radio" id="2" name="numOfChildren" value="2" class="dependents-numOfChildren-value"/>'
		+						'<label for="2">2</label>'
		+ 						'<input type="radio" id="3" name="numOfChildren" value="3" class="dependents-numOfChildren-value"/>'
		+						'<label for="3">3</label>'
		+ 						'<input type="radio" id="4" name="numOfChildren" value="4" class="dependents-numOfChildren-value"/>'
		+						'<label for="4">4</label>'
		+ 					'</div>'
		+					'<div id="child-tabs">'
		+						'<ul>'
		+							'<li class="child-tabs-" id="child-tabs-1"><a href="#child-tabs-1-content">1st Child</a></li>'
		+							'<li class="child-tabs-" id="child-tabs-2"><a href="#child-tabs-2-content">2nd Child</a></li>'
		+							'<li class="child-tabs-" id="child-tabs-3"><a href="#child-tabs-3-content">3rd Child</a></li>'
		+							'<li class="child-tabs-" id="child-tabs-4"><a href="#child-tabs-4-content">4th Child</a></li>'
		+						'</ul>'
		+ 						'<div class="child-tabs-content child-edit" id="child-tabs-1-content"></div>'
		+ 						'<div class="child-tabs-content child-edit" id="child-tabs-2-content"></div>'
		+ 						'<div class="child-tabs-content child-edit" id="child-tabs-3-content"></div>'
		+ 						'<div class="child-tabs-content child-edit" id="child-tabs-4-content"></div>'
		+					'</div>'
		+				'</div>'
		+			'</div>'
		+		'</div>',
		
		_dependentInfoEditTemplate: 
			'<div class="general-information-edit">'
		+		'<div class="dependent-personal-info">'
		+			'<h5 class="dependent-personal-info-heading"></h5>'
		+			'<div class="dependent-personal-info-content">'
		+				'<div class="dependent-personalInfo-surname clearfix">'
		+					'<span class="personalInfo-surname-label"></span> '
		+					'<span class="dependent-personalInfo-surname-value text-value"> <input data-req="1" type="text" data-name="Surname"/> </span>'
		+				'</div>'
		+				'<div class="dependent-personalInfo-givenName clearfix">'
		+					'<span class="personalInfo-givenName-label"></span> '
		+					'<span class="dependent-personalInfo-givenName-value text-value"><input data-req="1" type="text" data-name="Given Name"/>'
		+				'</div>'
		+				'<div class="dependent-personalInfo-gender clearfix">'
		+					'<span class="personalInfo-gender-label"></span> '
		+					'<span class="dependent-personalInfo-gender-value text-value">'
		+						'<input type="radio" id="male" data-name="Gender" name="gender-child" data-req="1" value="MALE"/>'
		+						'<label for="male">Male</label>'
		+ 						'<input type="radio" id="female" data-name="Gender" name="gender-child" data-req="1" value="FEMALE"/>'
		+						'<label for="female">Female</label>'
		+					'</span>'
		+				'</div>'
		+				'<div class="dependent-personalInfo-DOB clearfix">'
		+					'<span class="personalInfo-DOB-label"></span> '
		+					'<span class="dependent-personalInfo-DOB-value text-value"> <input data-req="1" type="date" data-name="Date of Birth"/></span>'
		+				'</div>'
		+				'<div class="dependent-personalInfo-age clearfix">'
		+					'<span class="personalInfo-age-label"></span> '
		+					'<span class="dependent-personalInfo-age-value text-value"><input data-req="0" type="text" data-name="Age"/></span>'
		+				'</div>'
		+			'</div>'
		+		'</div>'
		+	'</div>',
		
		_dependentInfoViewTemplate: 
				'<div class="general-information-view">'
			+		'<div class="dependent-personal-info">'
			+			'<h5 class="dependent-personal-info-heading"></h5>'
			+			'<div class="dependent-personal-info-content">'
			+				'<div class="dependent-personalInfo-surname clearfix">'
			+					'<span class="personalInfo-surname-label"></span> '
			+					'<span class="dependent-personalInfo-surname-value text-value"></span>'
			+				'</div>'
			+				'<div class="dependent-personalInfo-givenName clearfix">'
			+					'<span class="personalInfo-givenName-label"></span> '
			+					'<span class="dependent-personalInfo-givenName-value text-value">'
			+				'</div>'
			+				'<div class="dependent-personalInfo-gender clearfix">'
			+					'<span class="personalInfo-gender-label"></span> '
			+					'<span class="dependent-personalInfo-gender-value text-value"></span>'
			+				'</div>'
			+				'<div class="dependent-personalInfo-DOB clearfix">'
			+					'<span class="personalInfo-DOB-label"></span> '
			+					'<span class="dependent-personalInfo-DOB-value text-value"></span>'
			+				'</div>'
			+				'<div class="dependent-personalInfo-age clearfix">'
			+					'<span class="personalInfo-age-label"></span> '
			+					'<span class="dependent-personalInfo-age-value text-value"></span>'
			+				'</div>'
			+			'</div>'
			+		'</div>'
			+	'</div>' ,
		
		_financialInfoTemplate: '<div></div>',
		
		_buildInitialDOM: function() {
			var $page = $(this._template);
			var $genInfoPage1 = $(this._genInfoTemplate1);
			var $genInfoDependentEdit = $(this._genInfoDependentEditTemplate);
			var $genInfoDependentView = $(this._genInfoDependentViewTemplate);
			
			$("#genInfo", $page).text("General Information");
			$("#finInfo", $page).text("Financial Information");
			
			$(".personal-info-heading", $genInfoPage1).text("Personal Information");
			$(".contact-info-heading", $genInfoPage1).text("Contact Information");
			$(".dependents-info-heading", $genInfoDependentEdit).text("Number of Dependents");
			$(".dependents-info-heading", $genInfoDependentView).text("Number of Dependents");
			
			$genInfoPage1.append($genInfoDependentEdit);
			
			$('#genInfoFirstPanelView', $genInfoPage1).after($genInfoDependentView);
			$('#tabs-1', $page).append($genInfoPage1);
			
			$page.tabs(); //initalize tabbing
			
			this.$widget.append($page);
			
			//add financial information
			var  finInfoPage = new ePos.widget.FinancialInformationPanelView(
					'tabs-2', 
					{initializeData: true}, 
					function customAboutMeCallback(){});
			
			this._initializeLabels();
			this._initializePersonDetailLabels();
			this._initializeEvents();
			
			if(this.options.initializeData) 
				this.loadData();
			
			this._transferToViewMode();
		},
		
		loadData: function() {
			var _self = this;
			$('.general-information-view', _self.$widget).show();
			$('.general-information-buttons .button-edit .button', _self.$widget).show();
			$('.general-information-edit', _self.$widget).hide();
			$('.general-information-buttons .button-save .button', _self.$widget).hide();
			$('.general-information-buttons .button-cancel .button', _self.$widget).hide();
			
			this._loadGenInfoData();		
		},
		
		capitalizeString: function(string) {
        	return string.charAt(0).toUpperCase() + string.slice(1);
    	},
    	
    	_loadGenInfoData : function(){
	    	var _self = this;
	    	this.dataProvider.retrieveGeneralInfo(null,
				function(genInfoData){
					_self._generalInformation = genInfoData.generalInfo;
					_self._country = genInfoData.country;
					_self.dropDown.init(genInfoData.country, "Select");
					_self.dropDown.setSelectedByValue(genInfoData.generalInfo.country);
//					_self._loadGeneralInfoView(generalInfo);
//					_self._loadGeneralInfoEdit(generalInfo);
		    	},
		    	function(){
		    		console.log('_loadGeneralInfo: something went wrong');
		    	}
	    	);
   		},
   		
		_initializeLabels: function() {
			$('.button-edit .button', this.$widget).text('EDIT');
			$('.button-save .button', this.$widget).text('SAVE');
			$('.button-cancel .button', this.$widget).text('CANCEL');
						
			$('.contactInfo-mobile-label', this.$widget).html('Mobile: ');
			$('.contactInfo-email-label', this.$widget).html('Email: ');
			$('.contactInfo-country-label', this.$widget).text('Country: ');
			$('.contactInfo-homeAddress-label', this.$widget).html('Home Address: ');
			$('.contactInfo-workAddress-label', this.$widget).html('Work Address: ');
			
			this.$surname = $('.general-information-edit .personalInfo-surname-value input', this.$widget);
   			this.$givenName = $('.general-information-edit .personalInfo-givenName-value input', this.$widget);
   			this.$gender = $('.general-information-edit .personalInfo-gender-value input:radio[name="gender"]', this.$widget);
   			this.$dateOfBirth = $('.general-information-edit .personalInfo-DOB-value input', this.$widget);
   			this.$age = $('.general-information-edit .personalInfo-age-value input', this.$widget);
   			
			this.$mobile = $('.general-information-edit .contactInfo-mobile-value input', this.$widget);
   			this.$email = $('.general-information-edit .contactInfo-email-value input', this.$widget);
   			this.$homeAddr = $('.general-information-edit .contactInfo-homeAddress-value input', this.$widget);
   			this.$workAddr = $('.general-information-edit .contactInfo-workAddress-value input', this.$widget);
   			this.$numberOfChildren = $('#numOfChildren', this.$widget);
   			
   			//Dropdown	
			this.dropDown = new dropDownWidget('contactInfo-country');
		},

		_initializePersonDetailLabels: function() {
			$('.personalInfo-surname-label', this.$widget).html('Surname: ');
			$('.personalInfo-givenName-label', this.$widget).html('Given Name: ');
			$('.personalInfo-gender-label', this.$widget).html('Sex: ');
			$('.personalInfo-DOB-label', this.$widget).html('Date of Birth: ');
			$('.personalInfo-age-label', this.$widget).html('Age: ');			
		},
		
		_initializeEvents: function() {
			var _self = this;
			
			$('.general-information-buttons .button-edit .button', this.$widget).on('click', function(evt) {
		    	$('p.error', _self.$widget).remove();
		    	$('input', _self.$widget).removeClass('error-field');
				_self._transferToEditMode();
			});

			$('.general-information-buttons .button-cancel .button', this.$widget).on('click', function(evt) {
				_self._transferToViewMode();
			});

			$('.general-information-buttons .button-save .button', this.$widget).on('click', function(evt) {
				_self._saveGeneralInfo();
			});
			
			this.$dateOfBirth.on('change', function(evt) {
				var computedAge = _self._computeAge(_self.$dateOfBirth.val());
				_self.$age.val(computedAge);
			});
			
			//dependent tabs
			$('.child-tabs-content').hide();
			$('.child-tabs-').hide();
			
			$('#numOfChildren input:radio',  _self.$widget).on('click', function(evt) {
				 $('.child-tabs-content').hide();
				 $('.child-tabs-').hide();
				 $('.child-edit').empty();
				 
				if ($(this).val() === '0') {
					$('#child-tabs').hide();
				}else if ($(this).val() === '1') {
				  _self.editChildDetails(1);
				}else if ($(this).val() === '2') {
				  _self.editChildDetails(2);
				}else if($(this).val() === '3') {
				  _self.editChildDetails(3);
				}else{
				 _self.editChildDetails(parseInt($(this).val()));
				}
				
				if ($(this).val() != '0') {
					$("#child-tabs").tabs("refresh");
					$("#child-tabs").tabs("option", "active", 0);
				}
			});
			
			$("#child-tabs").tabs();
			
		},
		
		_computeAge: function(dateOfBirth) {
	    	//Compute age from dateOfBirth
	    	var a = moment(dateOfBirth);
			var b = moment(new Date());
			var computedAge = b.diff(a, 'years');
			
			return computedAge;
		},
		
		editChildDetails: function(numChildren){
			var _self = this;

			if(numChildren != 0) {
				$('#child-tabs').show();
				for(var i = 0; i < numChildren; i++){
					var tabContent = $('#child-tabs-' + (i+1) + '-content');
					$('#child-tabs-' + (i+1)).show();
					tabContent.empty();
					tabContent.append(_self._dependentInfoEditTemplate);
										
					var genderButton = $('.dependent-personal-info-content', tabContent).find('.dependent-personalInfo-gender').find('input');
					var genderLabel = $('.dependent-personal-info-content',tabContent).find('.dependent-personalInfo-gender').find('label');
					for(var j = 0; j < genderButton.length; j++){
						if($(genderButton[j]).val() == "MALE"){
							$(genderButton[j]).attr("id", "dependent-male"+(i+1));
							$(genderButton[j]).attr("name", "gender-child"+(i+1));
							$(genderLabel[j]).attr("for", "dependent-male"+(i+1));
						}else{
							$(genderButton[j]).attr("id", "dependent-female"+(i+1));
							$(genderButton[j]).attr("name", "gender-child"+(i+1));
							$(genderLabel[j]).attr("for", "dependent-female"+(i+1));
						}
					}
					
					if (_self._generalInformation.children && (i < _self._generalInformation.children.length)) {
						$('.dependent-personalInfo-surname-value input', tabContent).addClass('child'+(i+1)+'-surname').val(_self._generalInformation.children[i].surname || '');
						$('.dependent-personalInfo-givenName-value input', tabContent).addClass('child'+(i+1)+'-givenName').val(_self._generalInformation.children[i].givenname || '');
						$('.dependent-personalInfo-gender-value input:radio[name="gender-child'+(i+1)+'"][value='+_self._generalInformation.children[i].gender.toUpperCase()+']').attr('checked', 'checked');
						$('.dependent-personalInfo-DOB-value input', tabContent).addClass('child'+(i+1)+'-DOB').val(_self._generalInformation.children[i].dateOfBirth? moment(_self._generalInformation.children[i].dateOfBirth).format('YYYY-MM-DD') : '');
						$('.dependent-personalInfo-age-value input', tabContent).addClass('child'+(i+1)+'-age').val(_self._generalInformation.children[i].age || '');
					}else {
						$('.dependent-personalInfo-surname-value input', tabContent).addClass('child'+(i+1)+'-surname');
						$('.dependent-personalInfo-givenName-value input', tabContent).addClass('child'+(i+1)+'-givenName');				
						$('.dependent-personalInfo-DOB-value input', tabContent).addClass('child'+(i+1)+'-DOB');
						$('.dependent-personalInfo-age-value input', tabContent).addClass('child'+(i+1)+'-age');
					}					
					
					//Add on change event
					_self._attachDateOfBirthEvent(i);					
					
					_self._initializePersonDetailLabels();
//					$('#child-tabs-' + i + '-content').show();
				}
				$('#child-tabs-1-content').show();
			}
		},
		
		viewChildDetails: function(data){
			var _self = this;
			$('.child-tabs-content').hide();
			$('#child-tabs-view').show();
			
			if(data.children && data.children.length != 0) {
				for(var i = 0; i < data.children.length; i++){
					var tabContent = $('#child-tabs-' + (i+1) + '-content-view');
					$('#child-tabs-' + (i+1) + '-view').show();
					tabContent.empty();
					tabContent.append(_self._dependentInfoViewTemplate);
	
					$('.dependent-personalInfo-surname-value', tabContent).html(data.children[i].surname || '');
					$('.dependent-personalInfo-givenName-value', tabContent).html(data.children[i].givenname || '');
					$('.dependent-personalInfo-gender-value', tabContent).html(data.children[i].gender || '');
					$('.dependent-personalInfo-DOB-value', tabContent).html(data.children[i].dateOfBirth? moment(data.children[i].dateOfBirth).format('YYYY-MM-DD') : '');
					$('.dependent-personalInfo-age-value', tabContent).html(data.children[i].age);
					
					this._initializePersonDetailLabels();
//					$('#child-tabs-' + (i+1) + '-content-view').show();
				}
				$('#child-tabs-1-content-view').show();
				$("#child-tabs-view").tabs();
				$("#child-tabs-view").tabs("option", "active", 0);
			}else {
				$('#child-tabs-view').hide();
			}
		},	
		
		/**
		 * Event for Child date of birth on change
		 */
		_attachDateOfBirthEvent: function(ctr) {
			var _self = this;
			var $dob = $('.child'+(ctr+1)+'-DOB');
			var $age = $('.child'+(ctr+1)+'-age');
			
			//Date Handler
			$dob.on('change', function(event) {
				var computedAge = _self._computeAge($dob.val());
				$age.val(computedAge);
			});
		},
		
		_transferToViewMode: function() {
			$('.general-information-edit', this.$widget).hide();
			$('.general-information-buttons .button-edit .button', this.$widget).show();
			$('.general-information-view', this.$widget).show();
			$('.general-information-buttons .button-save .button', this.$widget).hide();
			$('.general-information-buttons .button-cancel .button', this.$widget).hide();
			this._loadGeneralInfoView(this._generalInformation);
		},

		_transferToEditMode: function() {
			var selectedCountry = '';
			var _self = this;
			
			$('.general-information-edit', this.$widget).show();
			$('.general-information-buttons .button-edit .button', this.$widget).hide();
			$('.general-information-view', this.$widget).hide();
			$('.general-information-buttons .button-save .button', this.$widget).show();
			$('.general-information-buttons .button-cancel .button', this.$widget).show();
			this._loadGeneralInfoEdit(this._generalInformation);
			
			$(this._country).each(function(ctr, countryData){
   				if(countryData.value == _self._generalInformation.country) {
   					selectedCountry = countryData.value;
   				}
   			});
			this.dropDown.setSelectedByValue(selectedCountry);
		},
		
   		_loadGeneralInfoView: function(data) {
   			var selectedCountry = '';
   			
   			$('.general-information-view .personalInfo-surname-value', this.$widget).text(data.surname);
   			$('.general-information-view .personalInfo-givenName-value', this.$widget).text(data.givenname);
   			$('.general-information-view .personalInfo-gender-value', this.$widget).text(data.gender);
   			$('.general-information-view .personalInfo-DOB-value', this.$widget).text(moment(data.dateOfBirth).format('YYYY-MM-DD'));
   			$('.general-information-view .personalInfo-age-value', this.$widget).text(data.age);
   			
   			$('.general-information-view .contactInfo-mobile-value', this.$widget).text(data.mobile);
   			$('.general-information-view .contactInfo-email-value', this.$widget).text(data.email);
//   			$('.general-information-view .contactInfo-country-value', this.$widget).text(this.dropDown.getSelected().label);
   			$('.general-information-view .contactInfo-homeAddress-value', this.$widget).text(data.homeAddress);
   			$('.general-information-view .contactInfo-workAddress-value', this.$widget).text(data.workAddress);
   			$(".dependent-number", this.$widget).text(data.numberOfChildren);
   			
   			$(this._country).each(function(ctr, countryData){
   				if(countryData.value == data.country) {
   					selectedCountry = countryData.label;
   				}
   			});
   			$('.general-information-view .contactInfo-country-value', this.$widget).text(selectedCountry);
	    		
   			
   			this.viewChildDetails(data);
   		},

   		_loadGeneralInfoEdit: function(data) {
   			this.$mobile.val(data.mobile);
   			this.$email.val(data.email);
   			this.$homeAddr.val(data.homeAddress);
   			this.$workAddr.val(data.workAddress);
   			this.$surname.val(data.surname);
   			this.$givenName.val(data.givenname);
   			$('.general-information-edit .personalInfo-gender-value input:radio[name="gender"][value='+data.gender.toUpperCase()+']').attr('checked', 'checked');
   			this.$dateOfBirth.val(moment(data.dateOfBirth).format('YYYY-MM-DD'));
   			this.$age.val(data.age);
//   		this.$numberOfChildren.find('input[name="numOfChildren"][value='+data.children.length+']').attr('checked', 'checked');
   			this.$numberOfChildren.find('input[name="numOfChildren"][value='+data.children.length+']').trigger("click");
   			this.editChildDetails(data.children.length);
   		},

		// Saving
	    _saveGeneralInfo : function(){
	    	var validationResult = this.validation.validateInput(this, $('input', this.$widget));
	    	var validateClientBirthdate = this.validation.validateBirthdate(this, this.$dateOfBirth, this.$age);
	    	var validateChildrenBirthDate = true;
	    	
	    	var newChildrenData = [];
	    	
	    	for(var i = 0; i < $('input[name="numOfChildren"]:checked').val(); i++){
	    		var result = this.validation.validateBirthdate(this, $('.child'+(i+1)+'-DOB'), $('.child'+(i+1)+'-age'));
	    		if (!result) {
	    			validateChildrenBirthDate = result;
	    		}
			}
	    	
			if(validationResult && validateClientBirthdate && validateChildrenBirthDate){
				$('.contact-information-form', this.$widget).find('.error').parent().find('input').removeClass('error-field');
				for(var j = 0; j < $('input[name="numOfChildren"]:checked').val(); j++){
					newChildrenData.push(
						{
							'age' : $('.child'+(j+1)+'-age').val(),
							'dateOfBirth' : $('.child'+(j+1)+'-DOB').val(),
							'gender' : $('input[name="gender-child'+(j+1)+'"]:checked').val(),	
							'givenname' : $('.child'+(j+1)+'-givenName').val(),
							'surname' : $('.child'+(j+1)+'-surname').val()													
						}
					);						
				}
	    		var newGeneralInfoData = {};
	    			newGeneralInfoData = {
    				'customerId': this._generalInformation.customerId,
    				'surname': this.capitalizeString(this.$surname.val().trim()),
    				'givenname': this.capitalizeString(this.$givenName.val().trim()),
    				'gender': $('input[name="gender"]:checked').val(),
    				'dateOfBirth': moment(this.$dateOfBirth.val()).format(),
    				'age': this.$age.val(),
    				'mobile': this.$mobile.val().trim(),
    				'email': this.$email.val().trim(),
    				'country': this.dropDown.getSelected().value,
    				'homeAddress': this.$homeAddr.val(),
    				'workAddress': this.$workAddr.val(),
    				'numberOfChildren': $('input[name="numOfChildren"]:checked').val(),
    				'children': newChildrenData
    			};
    			this.eventlistener.onSubmitGeneralInfoForm(newGeneralInfoData);

				this._generalInformation = newGeneralInfoData;
//				this._loadGeneralInfoEdit(newGeneralInfoData);
				this._transferToViewMode();
//    			this._loadGeneralInfoView(newGeneralInfoData);
	    	}else{
	    		console.log('_saveGeneralInfo: failed validation');
	    	}
	    }	
	}
	//Register the object inside ePOS
	ePos.widget.GTKYPanelView = GTKYPanelView;
}(ePos, $));
