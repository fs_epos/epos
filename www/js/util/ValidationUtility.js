/**
*	Validation, Validation Utility
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var ValidationUtility = function() {
		this.options = $.extend({}, ValidationUtility.DEFAULTS);
	}
	
	//Define default attributes
	ValidationUtility.DEFAULTS = {
		initializeData: true
	}
	
	ValidationUtility.prototype = {
		constructor : ValidationUtility,
	    
		_errorTemplate: '<p class="error"></p>',
		
		//Validation of user input goes here
    	validateInput: function(source, inputs) {
	    	var _self = this;
	    	var hasNoError = true;
	    	var checkedRadioBtn = false;
	    	var radioInputNames = {};
	    	
	    	// clear all error messages
	    	$('p.error', source.$widget).remove();
	    	
	    	inputs.each(function(ctr, input){
	    		var error_elem = $(_self._errorTemplate);
	    		var elem = $(input);
	    		var type = elem.attr('type');
	    		var req = parseInt(elem.attr('data-req'));
	    		if (!req) type = 'default'; //not required
	    			  
	    		var name = elem.attr('data-name');
	    		elem.removeClass('error-field');
	    		
	    		switch(type){
	    			case 'text':
	    				if(elem.val().trim().toString().match(/[\u3400-\u9FBF]/) || elem.val().trim().match(/[^a-z ]/gi) !== null){
	    					error_elem.text("Please enter alphabetical characters only.");
	    					elem.after(error_elem);
	    					hasNoError = false;
	    					elem.addClass('error-field');
	    				} else if(elem.val().trim().length <= 0) {
	    					error_elem.text("Please enter "+ name +'.');
	    					elem.after(error_elem);
	    					hasNoError = false;
	    					elem.addClass('error-field');
	    				}
	    				break;
	    			case 'email':
	    				if(elem.val().trim().length <= 0){
	    					error_elem.text("Please enter "+ name +".");
	    					elem.after(error_elem);
	    					hasNoError = false;
	    					elem.addClass('error-field');
	    				} else if(elem.val().trim().match(/^[\w.-]+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}(\.[a-zA-Z]{2,6})?$/) === null || elem.val().trim().match(/\.(\.|@)/gi) != null){
	    					error_elem.text("Please enter a valid email address.");
	    					elem.after(error_elem);
	    					hasNoError = false;
	    					elem.addClass('error-field');
	    				}
	    				break;
	    			case 'tel':
	    				if(elem.val().trim() !== '' && elem.val().trim().match(/[^0-9]/g) !== null){
	    					error_elem.text("Please enter numeric characters only.");
	    					elem.after(error_elem);
	    					hasNoError = false;
	    					elem.addClass('error-field');
	    				} else if(elem.val().trim().length <= 0){
	    					error_elem.text("Please enter "+ name +".");
	    					elem.after(error_elem);
	    					hasNoError = false;
	    					elem.addClass('error-field');
	    				}
	    				break;
	    			case 'radio':	    				
//	    				if(checkedRadioBtn === false){
	    				if(!(radioInputNames[elem.attr('name')])){
	    					radioInputNames[elem.attr('name')] = true;
	    					if($('input[name="'+elem.attr('name')+'"]', source.$widget).is(':checked') === false){
	    						error_elem.text("Please enter "+ name +".");
	        					elem.closest('span').append(error_elem);
	    						hasNoError = false;
	    					}
//	    					checkedRadioBtn = true;
	    				}
	    				break;
	    			case 'date':
	    				if(elem.val().trim().length <= 0){
	    					error_elem.text("Please enter " + name + ".");
	    					elem.after(error_elem);
	    					hasNoError = false;
	    					elem.addClass('error-field')
	    				}
	    				break;
	    			case 'textarea':
	    				if(elem.val().trim().length <= 0){
	    					error_elem.text("Please enter "+ name +".");
	    					elem.after(error_elem);
	    					hasNoError = false;
	    					elem.addClass('error-field');
	    				}
	    				break;
	    			case 'checkbox':
	    				if(!($('input[name="'+elem.attr('name')+'"]', source.$widget).is(':checked'))){
    						error_elem.text(elem.attr('data-msg'));
        					elem.parent().find('label').after(error_elem);
    						hasNoError = false;
    					}
	    				break;
	    			default:
	    				break;
	    		}
	    	});
    	
			return hasNoError;
    	},
    	
    	/**
    	 * Validation of dropdown selection
    	 * 
    	 * @param source
    	 * @param dropdowns - list of dropdown parents
    	 * @returns hasNoError
    	 */
    	validateDropdown: function(source, dropdowns) {
	    	var _self = this;
	    	var hasNoError = true;
	    	
	    	dropdowns.each(function(ctr, dropdown){
	    		var error_elem = $(_self._errorTemplate);
	    		var $elem = $(dropdown);
	    		var elemVal = $(dropdown).find('li.selected').attr('value');

	    		$elem.removeClass('error-field');
	    		
		    	if(elemVal === null || elemVal === "" || elemVal === undefined) {
					if($elem.next(".error").length == 0) {
						error_elem.text("Please select one.");
						$elem.after(error_elem);
    					hasNoError = false;
    					$elem.addClass('error-field');
					}
				}	  
	    	});
	    	
			return hasNoError;
    	},
    	
    	/**
    	 * Validation of date of birth and age
    	 * 
    	 * @param source
    	 * @param dateOfBirth - Birthdate
    	 * @param age - age
    	 * @returns hasNoError
    	 */
    	validateBirthdate: function(source, dateOfBirth, age) {
	    	var _self = this;
	    	var hasNoError = true;
	    	
	    	//Compute age from dateOfBirth
	    	var a = moment(dateOfBirth.val());
			var b = moment(new Date());
			var computedAge = b.diff(a, 'years');
			
			if(age.val().trim() !== '' && age.val().trim().match(/[^0-9]/g) !== null) {
				var error_elem = $(_self._errorTemplate);
				
				age.removeClass('error-field');
				error_elem.text("Please enter numeric characters only.");				
				age.after(error_elem);
				
				hasNoError = false;
				age.addClass('error-field');
			} else if(!(isNaN(parseInt(age.val()))) && computedAge != age.val()) {
	    		var error_elem1 = $(_self._errorTemplate);
	    		var error_elem2 = $(_self._errorTemplate);
	    		
	    		dateOfBirth.removeClass('error-field');
	    		age.removeClass('error-field');
	    	    		
	    		error_elem1.text("Sync with Age.");
	    		error_elem2.text("Sync with Date of Birth.");

	    		if(dateOfBirth.next(".error").length == 0) {
		    		dateOfBirth.after(error_elem1);
				}		
	    		age.after(error_elem2);
	    		
				hasNoError = false;
				
				dateOfBirth.addClass('error-field');
				age.addClass('error-field');
			}	    	
			return hasNoError;
    	}
	}
	//Register the object inside ePOS
	ePos.widget.ValidationUtility = ValidationUtility;
}(ePos, $));