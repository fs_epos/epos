//
//  CountryDAO.h
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Country.h"

@interface CountryDAO : NSObject

+(instancetype) sharedPersistenceStore;

//For initial data listing in the database
-(void) createInitialCountryList;

//Get all countries @json
-(NSArray*) loadAllCountries;

//Get all countries @MO
-(NSArray*) loadAllCountriesMO;

@end
