//
//  Country.m
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import "Country.h"


@implementation Country

@dynamic countryName;
@dynamic countryCode;
@dynamic value;
@dynamic label;

@end
