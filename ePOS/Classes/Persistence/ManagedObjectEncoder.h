//
//  ManagedObjectEncoder.h
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import <Foundation/Foundation.h>

@interface ManagedObjectEncoder : NSObject

+(NSArray*) encodeManagedObjects:(NSArray*) objects entityClass:(id) managedObject entityDescription:(NSEntityDescription*) entityDesc;

+(NSDictionary*) encodeManagedObjects:(id) managedObj entityDescription:(NSEntityDescription*) entityDesc;

@end
