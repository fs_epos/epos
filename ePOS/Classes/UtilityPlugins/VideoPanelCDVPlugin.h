//
//  VideoPanelCDVPlugin.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import <Cordova/CDVPlugin.h>

@class VideoPanelCDVPlugin;

@protocol VideoPanelCDVPluginDelegate <NSObject>
@required
- (void)videoPanelCDVPluginDelegate:(VideoPanelCDVPlugin *)plugin onOpenVideo:(NSString *)params;
@end

@interface VideoPanelCDVPlugin : CDVPlugin
@property (nonatomic, weak) id <VideoPanelCDVPluginDelegate> delegate;
+ (NSString *)pluginName;

- (void)retrieveVideo:(CDVInvokedUrlCommand *)command;
- (void)onOpenVideo:(CDVInvokedUrlCommand *)command;

@end
