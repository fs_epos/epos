//
//  ResourcePanelCDVPlugin.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import <Cordova/CDVPlugin.h>

@class ResourcePanelCDVPlugin;

@protocol ResourcePanelCDVPluginDelegate <NSObject>
@required
- (void)resourcePanelCDVPlugin:(ResourcePanelCDVPlugin *)plugin onOpenResource:(NSString *)params;

@end

@interface ResourcePanelCDVPlugin : CDVPlugin
@property (nonatomic, weak) id <ResourcePanelCDVPluginDelegate> delegate;
+ (NSString *)pluginName;
- (void)retrieveResource:(CDVInvokedUrlCommand *)command;
- (void)onOpenResource:(CDVInvokedUrlCommand *)command;

@end
