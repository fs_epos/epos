//
//  ResourcePanelCDVPlugin.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import "ResourcePanelCDVPlugin.h"

@implementation ResourcePanelCDVPlugin

+ (NSString *)pluginName {
    return @"ePos.widget.ResourcePanelView";
}

- (void)retrieveResource:(CDVInvokedUrlCommand *)command {
    
}

- (void)onOpenResource:(CDVInvokedUrlCommand *)command {
    NSLog(@"onOpenResource");
    if (self.delegate && [self.delegate respondsToSelector:@selector(resourcePanelCDVPlugin:onOpenResource:)]) {
        [self.delegate resourcePanelCDVPlugin:self onOpenResource:@"pdf"];
    }
}

@end
