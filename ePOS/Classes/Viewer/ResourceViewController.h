//
//  ResourceViewController.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import <UIKit/UIKit.h>

@interface ResourceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *resourceWebView;
- (id)initWithResourceType:(NSString *)params;

@end
