//
//  MainMenuViewController.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 10/30/15.
//
//

#import "MainMenuViewController.h"

#import "MainMenuCell.h"

#import "DashboardCDVViewController.h"
#import "AddProspectCDVViewController.h"
#import "CustomerListViewController.h"
#import "GTKYCDVViewController.h"
#import "AboutMeCDVViewController.h"
#import "AboutCompanyCDVViewController.h"

#define MENU_DASHBOARD      0
#define MENU_ADDPROSPECT    1
#define MENU_CUSTOMERLIST   2
#define MENU_GTKY           3
#define MENU_ABOUTUS        4

@interface MainMenuViewController () <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *menuItems;
}
@end

@implementation MainMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    menuItems = [NSMutableArray arrayWithArray:@[
                                                 @"Dashboard",
                                                 @"Add Prospect",
                                                 @"Customer List",
                                                 @"GTKY",
                                                 @"About Us"
                                                 ]];
    
    _loggedUser.text = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERNAME_KEY];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuItems.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellIdentifier";
    
    MainMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"MainMenuCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    cell.cellTitle.text = [menuItems objectAtIndex:indexPath.row];
    cell.cellImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"menu_%d",indexPath.row]];
    
    CGRect cellFrame = cell.frame;
    cellFrame.size.height = cellFrame.size.height + 1;
    UIView *selectionBG = [[UIView alloc] initWithFrame:cell.frame];
    selectionBG.backgroundColor = [UIColor colorWithRed:0.0f green:183.0f/255.0f blue:242.0f/255.0f alpha:1];
    
    UIView *imageLine = [[UIView alloc] initWithFrame:CGRectMake(46, 0, 1, cell.frame.size.height)];
    [imageLine setBackgroundColor:[UIColor whiteColor]];
    [selectionBG addSubview:imageLine];
    
    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 1, 320, 1)];
    [topLine setBackgroundColor:[UIColor whiteColor]];
    [selectionBG addSubview:topLine];
    
    UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, selectionBG.frame.size.height, 320, 1)];
    [bottomLine setBackgroundColor:[UIColor whiteColor]];
    [selectionBG addSubview:bottomLine];
    
    cell.selectedBackgroundView = selectionBG;
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self selectMenu:indexPath.row];
}

- (void)selectMenu:(NSInteger)menu {
    
    UIViewController *viewController = nil;
    
    switch (menu) {
        case MENU_DASHBOARD:
            viewController = [[DashboardCDVViewController alloc] init];
            break;
        case MENU_ADDPROSPECT:
            viewController = [[AddProspectCDVViewController alloc] init];
            break;
        case MENU_CUSTOMERLIST:
            viewController = [[CustomerListViewController alloc] initWithTitle:@"Customer List"];
            break;
        case MENU_GTKY:
            viewController = [[CustomerListViewController alloc] initWithTitle:@"Customer Selection"];
            break;
        case MENU_ABOUTUS:
            viewController = [[AboutCompanyCDVViewController alloc] init];
            break;
        default:
            break;
    }
    
    if (viewController) {
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
        self.viewDeckController.centerController = nav;
        [self.viewDeckController closeLeftViewAnimated:YES];
    }
}

#pragma mark - TableView Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0;
}

- (IBAction)logOutPressed:(UIButton *)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(mainMenuViewController:userDidLogout:)]) {
        [self.delegate mainMenuViewController:self userDidLogout:nil];
    }
}

- (IBAction)homePressed:(UIButton *)sender {
    [self selectMenu:MENU_DASHBOARD];
}

@end
