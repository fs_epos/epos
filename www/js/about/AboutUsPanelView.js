var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $){
	'use strict';
	function AboutUsPanelView(widgetReference, options, callback) {
		//use the current element instance if defined, othewise, get the object by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.options = $.extend({}, AboutUsPanelView.DEFAULTS, options);
		this.callback = callback;
		this._buildInitialDOM();
	}
	
	AboutUsPanelView.DEFAULTS = {
			initializeData: true
	}
	
	AboutUsPanelView.prototype = {
			constructor : AboutUsPanelView,
			options: null,
			$widget: null,
			callback: null,
			
			//html template
			_template: 	'<div id="about-us-tabs">'
					+'		<ul>'
	    			+'			<li><a href="#about-company">About Company</a></li>'
	    			+'			<li><a href="#about-me">About Me</a></li>'
	 				+'		</ul>'
	 				+'		<div id="about-company"></div>'
					+'		<div id="about-me"></div>'
					+'	</div>',
					
			_buildInitialDOM: function() {
				this.$widget.append(this._template);
				$("#about-us-tabs", this.$widget).tabs();
				if(this.options.initializeData) 
					this._loadTabs();
			},
			
			_loadTabs: function() {
				var  aboutCompany = new ePos.widget.AboutCompanyPanelView(
					'about-company', 
					{initializeData: true}, 
					function customAboutCompanyCallback(){});

				var  aboutMe = new ePos.widget.AboutMePanelView(
						'about-me', 
						{initializeData: true}, 
						function customAboutMeCallback(){});
			}
	},
	
	
	//Register the About Us Panel View
	ePos.widget.AboutUsPanelView = AboutUsPanelView;
}(ePos, $));