//
//  CustomerDetails.h
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define CUSTOMER_ID             @"customerId"
#define CUSTOMER_GIVENNAME      @"givenname"
#define CUSTOMER_DATEOFBIRTH    @"dateOfBirth"
#define CUSTOMER_HOMEADDRESS    @"homeAddress"
#define CUSTOMER_AGE            @"age"
#define CUSTOMER_WORKADDRESS    @"workAddress"
#define CUSTOMER_GENDER         @"gender"
#define CUSTOMER_SURNAME        @"surname"
#define CUSTOMER_EMAIL          @"email"
#define CUSTOMER_COUNTRY        @"country"
#define CUSTOMER_MOBILE         @"mobile"
#define CUSTOMER_NOCHILDREN     @"numberOfChildren"
#define CUSTOMER_CHILDREN       @"children"

@interface CustomerDetails : NSManagedObject

@property (nonatomic, retain) NSString *surname;
@property (nonatomic, retain) NSString *givenname;
@property (nonatomic, retain) NSString *gender;
@property (nonatomic, retain) NSString *dateOfBirth;
@property (nonatomic, retain) NSNumber *age;
@property (nonatomic, retain) NSString *customerId;
@property (nonatomic, retain) NSString *homeAddress;
@property (nonatomic, retain) NSString *workAddress;
@property (nonatomic, retain) NSString *mobile;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *country;
@property (nonatomic, retain) NSNumber *numberOfChildren;
@property (nonatomic, retain) NSSet    *children;

@end
