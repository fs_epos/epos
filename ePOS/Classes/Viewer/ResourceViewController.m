//
//  ResourceViewController.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import "ResourceViewController.h"
#import <AVFoundation/AVFoundation.h>

#define MOVMimeType     @"video/quicktime"
#define PDFMimeType     @"application/pdf"

@interface ResourceViewController ()
@property (nonatomic, assign) NSString *resourceType;
@end

@implementation ResourceViewController

- (id)initWithResourceType:(NSString *)params {
    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        _resourceType = params;
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    
//    if (self) {
//        
//    }
    
    return [self initWithResourceType:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    //BOOL flag;
    NSError *setCategoryError = nil;
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    [tapGestureRecognizer setCancelsTouchesInView:NO];
    [tapGestureRecognizer setNumberOfTapsRequired:1];
    
    [self.navigationController.view.window addGestureRecognizer:tapGestureRecognizer];
    self.resourceWebView.scrollView.bounces = NO;
    
    self.resourceWebView.scalesPageToFit = YES;
    self.resourceWebView.contentMode = UIViewContentModeScaleAspectFit;
    
    //resets webview
    [self.resourceWebView loadHTMLString:@"" baseURL:nil];
    
    if ([_resourceType isEqualToString:@"video"]) {
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSString *myFile = [mainBundle pathForResource: @"vid_1" ofType: @"mov"];
        
        [self.resourceWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:myFile]]];
    } else {
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSString *myFile = [mainBundle pathForResource: @"pdf" ofType: @"pdf"];
        [self.resourceWebView loadData:[NSData dataWithContentsOfFile:myFile] MIMEType:PDFMimeType textEncodingName:@"utf-8" baseURL:nil];
    }
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewTapped:(id)sender {
    
    for (UIGestureRecognizer *recognizer in self.navigationController.view.window.gestureRecognizers) {
        [self.navigationController.view.window removeGestureRecognizer:recognizer];
    }
    [self.resourceWebView loadHTMLString:@"" baseURL:nil];
    [self.navigationController removeFromParentViewController];
    [self.view removeFromSuperview];
}


@end
