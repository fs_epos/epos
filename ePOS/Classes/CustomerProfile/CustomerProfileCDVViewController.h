//
//  CustomerProfileCDVViewController.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/9/15.
//
//

#import <Cordova/CDVViewController.h>
#import "CustomerDetails.h"

@interface CustomerProfileCDVViewController : CDVViewController
- (id)initWithCustomer:(CustomerDetails *)customer;
@end
