/**
*	Dashboard, Video Panel Event Listener Mobile
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var VideoPanelEventListener = function() {
		this.options = $.extend({}, VideoPanelEventListener.DEFAULTS);	
		this.PLUGIN_NAME = 'ePos.widget.VideoPanelView';
	}
	//Define default attributes
	VideoPanelEventListener.DEFAULTS = {
		initializeData: true
	}
	
	VideoPanelEventListener.prototype = {
		constructor : VideoPanelEventListener,
		onOpenVideo: function(information) {
			var self = this;
    		// cordova call
    		cordova.exec(
				function(success){
					console.log('Video Panel: Video opened successfully.');
				},
				function(error){
					console.log('Video Panel: Something went wrong during the opening.');
				},
				this.PLUGIN_NAME,
				'onOpenVideo',
				(information ? [information] : [])
    		);
		}			
	}	
	//Register the object inside ePOS
	ePos.widget.VideoPanelEventListener = VideoPanelEventListener;
}(ePos, $));

