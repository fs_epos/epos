//
//  CustomerListCollectionViewCell.m
//  ePOS
//
//  Created by Karen Roque on 11/3/15.
//
//

#import "CustomerListCollectionViewCell.h"

@implementation CustomerListCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"CustomerListCollectionViewCell" owner:self options:nil];
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        if (![[arrayOfViews firstObject] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        self = [arrayOfViews firstObject];
    }
    return self;
}

@end
