/**
*	Dashboard, Resource Panel Data Provider Mobile
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var ResourcePanelDataProvider = function() {
		this.options = $.extend({}, ResourcePanelDataProvider.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.ResourcePanelView';
	}
	
	//Define default attributes
	ResourcePanelDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	ResourcePanelDataProvider.prototype = {
		constructor : ResourcePanelDataProvider,
		retrieveResource: function(queryObject, successCallback, failureCallback){
			//CORDOVA CALL
			cordova.exec(
				function(data){
					successCallback(data);
				},
				function(error){
					failureCallback(error);
				},
				this.PLUGIN_NAME,
				'retrieveResource',
				(queryObject ? queryObject : [])
			);
		}	
	}
	//Register the object inside ePOS
	ePos.widget.ResourcePanelDataProvider = ResourcePanelDataProvider;
}(ePos, $));

