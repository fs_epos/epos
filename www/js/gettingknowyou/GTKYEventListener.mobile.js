/**
*	GTKY, GTKY Event Listener mobile
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var GTKYEventListener = function() {
		this.options = $.extend({}, GTKYEventListener.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.GTKYPanelView';
	}
	
	//Define default attributes
	GTKYEventListener.DEFAULTS = {
		initializeData: true
	}

	GTKYEventListener.prototype = {
		constructor : GTKYEventListener,
		onSubmitGeneralInfoForm: function(information) {
			var self = this;
    		// cordova call
    		cordova.exec(
				function(success){
					console.log('GTKY-General Information: Form submitted successfully.');
				},
				function(error){
					console.log('GTKY-General Information: Something went wrong during form submission.');
				},
				this.PLUGIN_NAME,
				'onSubmitGeneralInfoForm',
				(information ? [information] : [])
    		);
		},
		onSubmitFinancialForm: function(data) {
			//phonegap
			var self = this;
    		// cordova call
    		cordova.exec(
				function(success){
					console.log('GTKY-Financial Information: Form submitted successfully.');
				},
				function(error){
					console.log('GTKY-Financial Information: Something went wrong during form submission.');
				},
				this.PLUGIN_NAME,
				'onSubmitFinancialForm',
				(data ? [data] : [])
    		);
		}
	}	
	//Register the object inside ePOS
	ePos.widget.GTKYEventListener = GTKYEventListener;
}(ePos, $));

