//
//  CustomerListCollectionViewCell.h
//  ePOS
//
//  Created by Karen Roque on 11/3/15.
//
//

#import <UIKit/UIKit.h>

@interface CustomerListCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *customerName;
@property (strong, nonatomic) IBOutlet UILabel *customerDescription;
@property (strong, nonatomic) IBOutlet UIImageView *customerImage;
@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumber;

@end
