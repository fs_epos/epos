//
//  AddProspectCDVPlugin.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/9/15.
//
//

#import <Cordova/CDVPlugin.h>

@class AddProspectCDVPlugin;

@protocol AddProspectCDVPluginDelegate <NSObject>
@required
- (void)addProspectCDVPlugin:(AddProspectCDVPlugin *)plugin prospectAdded:(id)params;
@end

@interface AddProspectCDVPlugin : CDVPlugin
@property (nonatomic, weak) id <AddProspectCDVPluginDelegate> delegate;
+ (NSString *)pluginName;
- (void)retrievePICS:(CDVInvokedUrlCommand *)command;
- (void)onSavePICSInfo:(CDVInvokedUrlCommand *)command;
@end
