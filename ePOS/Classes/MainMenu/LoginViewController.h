//
//  ViewController.h
//  FS_Asset
//
//  Created by Apuli, Alexis E. on 10/20/15.
//  Copyright (c) 2015 Accenture_FS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginViewController;
@protocol LoginViewControllerDelegate <NSObject>
@required
- (void)loginViewController:(LoginViewController *)sender loginDidSucceed:(id)params;
@end

@interface LoginViewController : UIViewController
@property (nonatomic, weak) id <LoginViewControllerDelegate> delegate;
@end
