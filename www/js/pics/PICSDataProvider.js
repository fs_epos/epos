/**
*	PICS, PICS Data Provider from the Dashboard
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var PICSDataProvider = function() {
		this.options = $.extend({}, PICSDataProvider.DEFAULTS);
		this.defaults = {
			data : { 
    					country: [
							{
								value: '1',
								label: 'Italy'
							},
							{
								value: '2',
								label: 'Philippines'
							},
							{
								value: '3',
								label: 'Hong Kong'
							}
						],
						term: "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus"						
			}
		};
	}
	
	//Define default attributes
	PICSDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	PICSDataProvider.prototype = {
		constructor : PICSDataProvider,
		retrievePICS: function(queryObject, successCallback, failureCallback) {
			if(typeof successCallback == 'function'){
    			console.log("retrievePICS execution")
    			successCallback(this.defaults.data);
    		}
		}
		
	}
	//Register the object inside ePOS
	ePos.widget.PICSDataProvider = PICSDataProvider;
}(ePos, $));

