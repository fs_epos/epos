/**
*	PICS, PICS Data Provider mobile
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var PICSDataProvider = function() {
		this.options = $.extend({}, PICSDataProvider.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.PICSPanelView';
	}
	
	//Define default attributes
	PICSDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	PICSDataProvider.prototype = {
		constructor : PICSDataProvider,
		retrievePICS: function(queryObject, successCallback, failureCallback) {
			//CORDOVA CALL
			cordova.exec(
				function(data){
					successCallback(data);
				},
				function(error){
					failureCallback(error);
				},
				this.PLUGIN_NAME,
				'retrievePICS',
				(queryObject ? queryObject : [])
			);
		}		
	}
	//Register the object inside ePOS
	ePos.widget.PICSDataProvider = PICSDataProvider;
}(ePos, $));

