//
//  CustomerListEventCDVPlugin.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/11/15.
//
//

#import "CustomerListEventCDVPlugin.h"

@implementation CustomerListEventCDVPlugin

+ (NSString *)pluginName {
    return @"ePos.widget.CustomerListEventListener";
}

- (void)onOpenCustomerProfile:(CDVInvokedUrlCommand *)command {
    
    if (command.arguments.count != 1) {
        NSLog(@"onOpenCustomerProfile invalid arguments");
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
        return;
    }
    
    NSDictionary *customerJSON = command.arguments[0];
    
    CustomerDetailsDAO *customerDAO = [CustomerDetailsDAO sharedPersistenceStore];
    CustomerDetails *customer = [customerDAO getCustomerWithCustomerId:[customerJSON objectForKey:CUSTOMER_ID]];
    
    if (customer && self.delegate && [self.delegate respondsToSelector:@selector(customerListEventCDVPlugin:onOpenCustomerProfile:)]) {
        NSLog(@"onOpenCustomerProfile %@",customer);
        
        [self.delegate customerListEventCDVPlugin:self onOpenCustomerProfile:customer];
        
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
        
    } else {
        NSLog(@"onOpenCustomerProfile no customer found");
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
    }
    
}

@end
