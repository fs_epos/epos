/**
*	Contact Information, Contact Information Data Provider
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var ContactInformationDataProvider = function() {
		this.options = $.extend({}, ContactInformationDataProvider.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.ContactInformationPanelView';
	}
	//Define default attributes
	ContactInformationDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	ContactInformationDataProvider.prototype = {
		constructor : ContactInformationDataProvider,
		
		retrieveContactInfo: function(queryObject, successCallback, failureCallback){
			// cordova call
			cordova.exec(
	    		function(data){
	    			successCallback(data);
	    		},
	    		function(error){
	    			failureCallback(error);
	    		},
	    		this.PLUGIN_NAME,
	    		'retrieveContactInfo',
	            (queryObject ? queryObject : [])
	    	);
		}		
	}
	//Register the object inside ePOS
	ePos.widget.ContactInformationDataProvider = ContactInformationDataProvider;
}(ePos, $));