/**
*	Dashboard, Video Panel Data Provider
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var VideoPanelDataProvider = function() {
		this.options = $.extend({}, VideoPanelDataProvider.DEFAULTS);
		this.defaults = {
			videoId : '0001'
		};
	}
	
	//Define default attributes
	VideoPanelDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	VideoPanelDataProvider.prototype = {
		constructor : VideoPanelDataProvider,
		retrieveVideo: function(queryObject, successCallback, failureCallback) {
			if(typeof successCallback == 'function'){
    			console.log("retrieveResource execution")
    			successCallback(this.defaults.videoId);
    		}
		}
	}
	//Register the object inside ePOS
	ePos.widget.VideoPanelDataProvider = VideoPanelDataProvider;
}(ePos, $));

