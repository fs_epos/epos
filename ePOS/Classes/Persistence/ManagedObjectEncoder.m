//
//  ManagedObjectEncoder.m
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import "ManagedObjectEncoder.h"

@implementation ManagedObjectEncoder
/*
 Returns array of json objects
 */
-(NSArray*) encodeManagedObjects:(NSArray*) objects entityClass:(id) managedObject entityDescription:(NSEntityDescription*) entityDesc {
    NSMutableArray *encodedObjects = [[NSMutableArray alloc] init];
    if (objects.count > 0) {
        for (NSManagedObject *managedObj in objects) {
            NSArray *keys = [[entityDesc attributesByName] allKeys];
            NSDictionary *dict = [managedObj dictionaryWithValuesForKeys:keys];
            [encodedObjects addObject:dict];
        }
    }
    
    return encodedObjects;
}

/*
 Return encoded managed object
 */
-(NSDictionary*) encodeManagedObjects:(id) managedObj entityDescription:(NSEntityDescription*) entityDesc {

    NSArray *keys = [[entityDesc attributesByName] allKeys];
    NSDictionary *encodedObject = [managedObj dictionaryWithValuesForKeys:keys];
    
    return encodedObject;
}


@end
