//
//  CustomerDetails.m
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import "CustomerDetails.h"


@implementation CustomerDetails

@dynamic surname;
@dynamic givenname;
@dynamic homeAddress;
@dynamic workAddress;
@dynamic gender;
@dynamic dateOfBirth;
@dynamic age;
@dynamic customerId;
@dynamic email;
@dynamic mobile;
@dynamic country;
@dynamic numberOfChildren;
@dynamic children;

@end
