//
//  GTKYCDVPlugin.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import <Cordova/CDVPlugin.h>
#import "CustomerDetailsDAO.h"
@interface GTKYCDVPlugin : CDVPlugin

+ (NSString *)pluginName;
- (void)retrieveGeneralInfo:(CDVInvokedUrlCommand *)command;
- (id)initWithCustomer:(CustomerDetails *)customer;
- (void)onSubmitGeneralInfoForm:(CDVInvokedUrlCommand *)command;

@end
