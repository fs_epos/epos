//
//  GTKYCDVViewController.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import <Cordova/CDVViewController.h>
#import "CustomerDetails.h"

@interface GTKYCDVViewController : CDVViewController
- (id)initWithCustomer:(CustomerDetails *)customer;
@end
