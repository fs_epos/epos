//
//  MainMenuCell.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/3/15.
//
//

#import <UIKit/UIKit.h>

@interface MainMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UIImageView *cellImage;

@end
