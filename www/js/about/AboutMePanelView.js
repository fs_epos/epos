/**
About Me, About Us Panel View
@author Neil Conocono
*/

var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';
	function AboutMePanelView(widgetReference, options, callback) {
		//use the current element instance if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.options = $.extend({}, AboutMePanelView.DEFAULTS, options);
		this.dataprovider = new ePos.widget.AboutUsDataProvider();
		this.callback = callback;
		this._buildInitialDOM();
		if(this.options.initializeData) 
			this._loadData();
	}
	
	AboutMePanelView.DEFAULTS = {
		initializeData: true
	}
	
	AboutMePanelView.prototype = {
		constructor: AboutMePanelView,
		options: null,
		$widget: null,
		callback: null,
		_template : 	'<div class="about-me-form">'
			+		'<div class="about-me-view">'
			+			'<div class="about-me-title"> <h3 class="about-me-title-value"> </h3> </div>'
			+			'<div class="about-me-name"> <h4 class="about-me-name-value"> </h3> </div>'
			+			'<div class="about-me-description"><div class="about-me-licenses"> </div>'
			+			'<div class="about-me-content"> </div></div>'
			+		'</div>'
			+	'</div>',
			
		_initializeLabels: function() {
			$('.about-me-view .about-me-title .about-me-title-value', this.$widget).html("About Me");
		},
		
		_buildInitialDOM: function() {
			this.$widget.append(this._template);
			this._initializeLabels();
		},
		
		
		_loadData: function() {
			var _self = this;
	    	this.dataprovider.retrieveAgentInfo(null,
				function(agentinfo){
		    		$('.about-me-view .about-me-name-value', _self.$widget).html(agentinfo.givenname + " " + agentinfo.surname);	
					$('.about-me-view .about-me-licenses', _self.$widget).html(agentinfo.description);	
					$('.about-me-view .about-me-content', _self.$widget).html(agentinfo.description);
		    	},
		    	function(){
		    		console.log('retrieveAgentInfo: something went wrong');
		    	}
	    	);
		}
	}
	
	ePos.widget.AboutMePanelView = AboutMePanelView;
}(ePos, $));