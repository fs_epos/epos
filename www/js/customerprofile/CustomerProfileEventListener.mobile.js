/**
*	Customer Profile, Customer Profile Event Listener
*	Copyright Accenture Inc., ePOS 2015
*	@author Neil Conocono, Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerProfileEventListener = function() {
		this.options = $.extend({}, CustomerProfileEventListener.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.CustomerProfilePanelView';
	}
	
	//Define default attributes
	CustomerProfileEventListener.DEFAULTS = {
		initializeData: true
	}
	
	CustomerProfileEventListener.prototype = {
		constructor : CustomerProfileEventListener,
		onSubmitContactForm: function(data) {
			var self = this;
    		// cordova call
    		cordova.exec(
				function(success){
					console('['+self.uuid+'] Form submitted successfully.');
				},
				function(error){
					console('['+self.uuid+'] Something went wrong during form submission.');
				},
				this.PLUGIN_NAME,
				'onSubmitContactForm',
				(data ? [data] : [])
    		);
		},
		onSubmitPersonalForm: function(data) {
			var self = this;
    		// cordova call
    		cordova.exec(
				function(success){
					console('['+self.uuid+'] Form submitted successfully.');
				},
				function(error){
					console('['+self.uuid+'] Something went wrong during form submission.');
				},
				this.PLUGIN_NAME,
				'onSubmitPersonalForm',
				(data ? [data] : [])
    		);
		}
	}
	//Register the object inside ePOS
	ePos.widget.CustomerProfileEventListener = CustomerProfileEventListener;
}(ePos, $));

