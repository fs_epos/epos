var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $){
	'use strict';
	
	function AboutCompanyPanelView(widgetReference, options, callback) {
		//use the current element instance if defined, otherwise, get the objec by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.options = $.extend({}, AboutCompanyPanelView.DEFAULTS, options);
		this.callback = callback;
		this._buildInitialDOM();
	}
	
	AboutCompanyPanelView.DEFAULTS = {
			initializeData : true
	}
	
	AboutCompanyPanelView.prototype = {
			constructor : AboutCompanyPanelView,
			options: null,
			$widget: null,
			callback: null,
			
			_template : 	'<div class="about-company-form">'
						+		'<div class="about-company-view">'
						+			'<div class="about-company-title"> <h3 class="about-company-title-value"> </h3> </div>'
						+			'<div class="about-company-content"> </div>'
						+			'<div class="about-company-videos videos-panel"> </div>'
						+			'<div class="about-company-resources resources-panel"> </div>'
						+		'</div>'
						+	'</div>',
						
			_companyContent : 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc',
						
			_buildInitialDOM: function() {
				this.$widget.append(this._template);
				this._initializeLabels();
				
				var resource = new ePos.widget.ResourcePanelView(
						'about-company-resources', 
						{
							initializeData: true
						}, 
						function customResourceCallback(){}
					);
				
				var video = new ePos.widget.VideoPanelView(
					'about-company-videos', 
					{
						initializeData: true
					}, 
					function customVideoCallback(){}
				);
			},
			
			_initializeLabels: function() {
				$('.about-company-view .about-company-title .about-company-title-value', this.$widget).html("About Company");
				$('.about-company-view .about-company-content', this.$widget).html(this._companyContent);
				
			}
	}
	
	//Register the object inside ePOS
	ePos.widget.AboutCompanyPanelView = AboutCompanyPanelView;
}(ePos, $));