//
//  Child.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/12/15.
//
//

#import <CoreData/CoreData.h>

#define CHILD_DOB           @"dateOfBirth"
#define CHILD_GENDER        @"gender"
#define CHILD_GIVENNAME     @"givenname"
#define CHILD_SURNAME       @"surname"
#define CHILD_AGE           @"age"

@interface Child : NSManagedObject

@property (nonatomic, retain) NSString *dateOfBirth;
@property (nonatomic, retain) NSString *gender;
@property (nonatomic, retain) NSString *givenname;
@property (nonatomic, retain) NSString *surname;
@property (nonatomic, retain) NSNumber *age;

@end
