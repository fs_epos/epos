/**
*	PICS, PICS form for the Prospect
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';
	function PICSPanelView(widgetReference, options, callback) {
		//use the current element instance if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.dataProvider = new ePos.widget.PICSDataProvider(),
		this.eventListener = new ePos.widget.PICSEventListener(),
		this.validation = new ePos.widget.ValidationUtility();
		this.options = $.extend({}, PICSPanelView.DEFAULTS, options);
		this.callback = callback;
		this._termsText = "";
		this._countries = [];
		this._buildInitialDOM();
	}
	
	//Define default attributes
	PICSPanelView.DEFAULTS = {
		initializeData: true
	}
	
	PICSPanelView.prototype = {
		constructor : PICSPanelView,
		options: null,
		$widget: null,
		callback: null,
		
		//html template
		_template1: '<div class="pics-form">'
			+			'<div class="pics-view">'
			+				'<div class="client-surname clearfix">'
			+					'<span class="client-surname-label left-align"></span> '
			+					'<input data-req="1" class="client-surname-value left-align" data-name="Surname" type="text"/>'
			+				'</div>'
			+				'<div class="client-givenname clearfix">'
			+					'<span class="client-givenname-label left-align"> </span> '
			+					'<input data-req="1" class="client-givenname-value left-align" data-name="Given Name" type="text"/>'
			+				'</div>'
			+				'<div class="client-DOB clearfix">'
			+					'<span class="client-DOB-label"></span> '
			+					'<input data-req="1" type="date" class="client-DOB-value text-value" data-name="Date of Birth"/>'
			+				'</div>'
			+				'<div class="client-age clearfix">'
			+					'<span class="client-age-label"></span> '
			+					'<input data-req="0" class="client-age-value text-value" type="text" data-name="Age"/>'
			+				'</div>'
			+				'<div class="client-country clearfix">'
			+					'<span class="client-country-label left-align"></span> '
			+					'<span class="client-country-value left-align"><div id="client-country" class="dropdown"></div></span>'
			+				'</div>'
			+				'<div class="client-address clearfix">'
			+					'<span class="client-address-label left-align"></span> '
			+					'<input data-req="1" class="client-address-value left-align" data-name="Address" type="textarea"/>'
			+				'</div>'
			+				'<div class="client-phonenumber clearfix">'
			+					'<span class="client-mobile-label left-align"></span> '
			+					'<input data-req="1" class="client-mobile-value left-align" data-name="Phone Number" type="tel"/>'
			+				'</div>'
			+				'<div class="client-email clearfix">'
			+					'<span class="client-email-label left-align"></span> '
			+					'<input data-req="1" class="client-email-value left-align" data-name="Email" type="email"/>'
			+				'</div>'
			+				'<div class="client-sex clearfix">'
			+					'<span class="client-sex-label left-align"></span>'
			+					'<div class="client-sex-value left-align">'
			+						'<input data-req="1" data-name="Sex" type="radio" id="gender-male" name="sex" value="MALE" checked> <label for="gender-male">Male</label>'
			+						'<input id="gender-female" data-req="1" data-name="Sex" type="radio" name="sex" value="FEMALE"> <label for="gender-female">Female</label>'
//			+						'<input type="radio" name="sex" checked value="male"/>Male'
//			+						'<input type="radio" name="sex" value="female"/>Female'
			+					'</div>'
			+				'</div>'
			+			'</div>'
			+		'</div>'
		,
		
		_template2: '<div class="pics-terms">'			
			+			'<div class="pics-address clearfix">'
			+				'<p class="pics-terms-label"></p>'
			+			'</div>'
			+			'<div class="pics-checkbox clearfix">'
			+				'<input id="pics-checkbox-agree" type="checkbox" data-req="1" data-msg="Please confirm that you have read and accept the Terms." name="terms" class="checkbox-agree">'
			+				'<label for="pics-checkbox-agree">I have read and agree to the terms and conditions</label>'
//			+				'<span class="pics-terms-checkbox"></span>'
			+			'</div>'
			+			'<div class="pics-button">'
			+				'<span class="button-span row button-add"> <button id="button-add-prospect" class="button add-prospect" /></span>'
			+			'</div>'
			+		'</div>'
		,
		
		_buildInitialDOM: function() {
			var $page1 = $(this._template1);
			var $page2 = $(this._template2);
			
			var StepPage = new StepPagePanelView();
			StepPage.init($('.steppage'), {showDropDown: true});
			StepPage.addPage($page1);
			StepPage.addPage($page2);
			
			this.$widget.append(StepPage);
			
			if(this.options.initializeData) 
				this.loadData();

			this._initializeLabels();
			this._initializeEvents();
			
			var placeHolder = 'Select';			
			var dropDown = new dropDownWidget('client-country');
			dropDown.init(this._countries, placeHolder);
			
			$('.pics-view', this.$widget).show();
		},
				
		loadData: function() {
			var _self = this;
			if(typeof this.dataProvider.retrievePICS == 'function') {
				this.dataProvider.retrievePICS(null,
					function(picsData){
						_self._countries = picsData.country;
						_self._termsText = picsData.term;
					},
					function(){
						console.log('PICS loadData: Data retrieval unseccessful');
					}
				);				
			}			
		},
				
		_initializeLabels: function() {
			$('.client-surname-label', this.$widget).html('Surname: ');
			$('.client-givenname-label', this.$widget).html('Given Name: ');
			$('.client-country-label', this.$widget).html('Country: ');
			$('.client-address-label', this.$widget).html('Home Address: ');
			$('.client-mobile-label', this.$widget).html('Phone Number: ');
			$('.client-email-label', this.$widget).html('Email: ');
			$('.client-sex-label', this.$widget).html('Sex: ');
			$('.client-age-label', this.$widget).html('Age: ');
			$('.client-DOB-label', this.$widget).html('Date of Birth: ');
			$('.button-add .button', this.$widget).html('Add Prospect');
			$('.pics-terms-label', this.$widget).html(this._termsText);
//			$('.pics-terms-checkbox', this.$widget).html('I have read and agree to the terms and conditions');
			
			this.$surname = $('.client-surname-value', this.$widget);
			this.$givenname = $('.client-givenname-value', this.$widget);
			this.$country = $('#client-country', this.$widget).parent();
			this.$address = $('.client-address-value', this.$widget);
			this.$mobile = $('.client-mobile-value', this.$widget);
			this.$email = $('.client-email-value', this.$widget);
			this.$sex = $('input[name=sex]', this.$widget);
			this.$dateOfBirth = $('.client-DOB-value', this.$widget);
			this.$age = $('.client-age-value', this.$widget);
			this.$sex = $('input[name=sex]', this.$widget);
			
			
//			this.$termCheckbox = $('.pics-terms-checkbox', this.$widget);			
		}, 
		
		_initializeEvents: function() {
			var _self = this;
			
			$('.add-prospect.button', this.$widget).on('click', function(evt) {
				_self.validateInputs(_self);
				// TODO: Event listener
			});
			
			//Date Handler
			this.$dateOfBirth.on('change', function(event) {
				var computedAge = _self._computeAge(_self.$dateOfBirth.val());
				_self.$age.val(computedAge);
			});
			/*Validation upon input*/
//			this.$surname.on('input', function(evt) {
//				var $field = $('.client-surname-value', this.$widget);
//				var surname = $field.val();
//				_self.validateAlphabetInput($field, surname);
//			});
//			
//			this.$givenname.on('input', function(evt) {
//				var $field = $('.client-givenname-value', this.$widget);
//				var givenName = $field.val();
//				_self.validateAlphabetInput($field, givenName);
//			});
//			
//			this.$address.on('input', function(evt) {
//				var $field = $('.client-address-value', this.$widget);
//				var address = $field.val();
//				_self.validateEmptyInput($field, address, 'Enter Address');
//			});
//			
//			this.$mobile.on('input', function(evt) {
//				var $field = $('.client-mobile-value', this.$widget);
//				var mobile = $field.val();
//				_self.validateMobileNumberInput($field, mobile);
//			});
//			
//			this.$country.on('click', function(evt) {
//				var $field = _self.$country;
//				var country = $field.find('div.dropdownValue').html();
//				_self.validateEmptyInput($field.parent(), country, 'Select Country');
//			});
//						
//			this.$email.on('change', function(evt) {
//				var $field = $('.client-email-value', this.$widget);
//				var email = $field.val();
//				_self.validateEmailInput($field, email);
//			});					
		},
		
		/**
		 * Validation of inputs once 'Add Prospect' button is click
		 */
		validateInputs: function(_self) {
			var validationInputResult = _self.validation.validateInput(_self, $('input', _self.$widget));
			var validateClientBirthdate = this.validation.validateBirthdate(this, this.$dateOfBirth, this.$age);
			var validationDropdownResult = _self.validation.validateDropdown(_self, $('.dropdown', _self.$widget).parent());
			var picsData = {};
			
			if(validationInputResult && validationDropdownResult && validateClientBirthdate) {				
				picsData.surname = _self.$surname.val();
				picsData.givenname = _self.$givenname.val();
				picsData.country = _self.$country.find('li.selected').attr('value');
				picsData.homeAddress = _self.$address.val();
				picsData.mobile = _self.$mobile.val();
				picsData.email = _self.$email.val();
				picsData.gender = _self.$sex.val();
				picsData.age = _self.$age.val();
				picsData.dateOfBirth = moment(_self.$dateOfBirth.val()).format();
								
				// TODO: Call even listener to pass values
				_self.eventListener.onSavePICSInfo(picsData);
			}
		},
		
		_computeAge: function(dateOfBirth) {
	    	//Compute age from dateOfBirth
	    	var a = moment(dateOfBirth);
			var b = moment(new Date());
			var computedAge = b.diff(a, 'years');
			
			return computedAge;
		},
		
		/**
		 * Validate input if null/empty/undefined
		 */
		validateEmptyInput: function($container, text, message) {
			if(text === null || text === "" || text === undefined) {
				if($container.next(".error").length == 0) {
					$container.after('<p class="error">'+message+'</p>');
					$container.addClass('error-mask');
				}
				return 1;
			}
			else {
				$container.next(".error").remove();
				$container.removeClass('error-mask');
				return 0;
			}
		}			
	}
	//Register the object inside ePOS
	ePos.widget.PICSPanelView = PICSPanelView;
}(ePos, $));
