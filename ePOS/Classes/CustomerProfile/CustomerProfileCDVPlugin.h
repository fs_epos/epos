//
//  CustomerProfileCDVPlugin.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/9/15.
//
//

#import <Cordova/CDVPlugin.h>
#import "CustomerDetailsDAO.h"

@interface CustomerProfileCDVPlugin : CDVPlugin

@property (nonatomic, retain) CustomerDetails *customerDetails;

+ (NSString *)pluginName;

- (void)retrieveContactInfo:(CDVInvokedUrlCommand *)command;
- (void)onSubmitContactForm:(CDVInvokedUrlCommand *)command;

@end
