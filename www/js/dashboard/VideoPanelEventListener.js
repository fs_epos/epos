/**
*	Dashboard, Video Panel Event Listener
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var VideoPanelEventListener = function() {
		this.options = $.extend({}, VideoPanelEventListener.DEFAULTS);		
	}
	//Define default attributes
	VideoPanelEventListener.DEFAULTS = {
		initializeData: true
	}
	
	VideoPanelEventListener.prototype = {
		constructor : VideoPanelEventListener,
		onOpenVideo: function(information) {
			//phonegap
			console.log('onOpenVideo data: ' + information);
		}			
	}	
	//Register the object inside ePOS
	ePos.widget.VideoPanelEventListener = VideoPanelEventListener;
}(ePos, $));

