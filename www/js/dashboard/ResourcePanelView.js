/**
*	Dashboard, Resource Panel View
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';
	function ResourcePanelView(widgetReference, options, callback) {
		//use the current element instance if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('.' + widgetReference);
		this.dataProvider = new ePos.widget.ResourcePanelDataProvider(),
		this.eventListener = new ePos.widget.ResourcePanelEventListener(),
		this.options = $.extend({}, ResourcePanelView.DEFAULTS, options);
		this.callback = callback;
		this._resourceId = null;
		
		this._buildInitialDOM();
	}
	
	//Define default attributes
	ResourcePanelView.DEFAULTS = {
		initializeData: true,
		// resourceId: null,
		// resourceEventListener: null
	}
	
	ResourcePanelView.prototype = {
		constructor : ResourcePanelView,
		options: null,
		$widget: null,
		callback: null,
		
		//html template
		_template:	'<div class="resource-holder"></div>',
				
		_buildInitialDOM: function() {
			
			this.$widget.append(this._template);
			this._initializeEvents();
			if(this.options.initializeData) 
				this.loadData();
						
			// this._initializeLabels();
		},
				
		loadData: function() {
			var _self = this;
			
			if(typeof this.dataProvider.retrieveResource == 'function') {				
				this.dataProvider.retrieveResource(null,
					function(resourceId){
						_self._resourceId = resourceId;
					},
					function(){
						console.log('Resource Panel loadData: Data retrieval unseccessful');
					}
				);	
			}
		},
				
		// _initializeLabels: function() {
			// $('.resource-title', this.$widget).html('Resources');
		// }, 
		
		_initializeEvents: function() {
			var _self = this;
						
			$('.resource-holder', this.$widget).on('click', function(evt) {
				_self.openResource(_self);
			});			
		},
		
		openResource: function(_self){
			// TODO: eventlistener
			// _self.options.resourceEventListener.onOpenResource(_self.options.resourceId);
			_self.eventListener.onOpenResource(_self._resourceId);
		}		
	}
	//Register the object inside ePOS
	ePos.widget.ResourcePanelView = ResourcePanelView;
}(ePos, $));