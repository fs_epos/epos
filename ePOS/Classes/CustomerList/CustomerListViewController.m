//
//  CustomerListViewController.m
//  ePOS
//
//  Created by Karen Roque on 11/3/15.
//
//

#import "CustomerListViewController.h"
#import "CustomerListCollectionViewCell.h"
#import "CustomerListFlowLayout.h"
#import <QuartzCore/QuartzCore.h>
#import "CustomerDetails.h"
#import "CustomerDetailsDAO.h"
#import "CustomerProfileCDVViewController.h"
#import "GTKYCDVViewController.h"

@interface CustomerListViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collection;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UISearchBar *customerSearchbar;
@property (nonatomic, strong) NSArray *customerList;
@property (nonatomic, strong) NSArray *filteredCustomers;
@property (nonatomic, strong) UILabel *noCustomers;
@property (nonatomic, retain) NSString *navTitle;
@end

@implementation CustomerListViewController {
    BOOL isFirstLoad;
}

- (id)initWithTitle:(NSString *)title {
    
    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        _navTitle = title;
    }
    
    return self;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    return [self initWithTitle:@""];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [button setImage:[UIImage imageNamed:@"sideMenu"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(toggleMenu) forControlEvents:UIControlEventAllTouchEvents];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    [self.navigationItem setLeftBarButtonItem:item];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.0f green:183.0f/255.0f blue:242.0f/255.0f alpha:1]];
    
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:self.navigationController.navigationBar.frame];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setTextAlignment:NSTextAlignmentCenter];
    [titleLabelView setFont:[UIFont boldSystemFontOfSize:22]];
    titleLabelView.text = [NSString stringWithFormat:@"%@",_navTitle];
    
    self.navigationController.navigationBar.topItem.titleView = titleLabelView;
    
    isFirstLoad = YES;
    self.customerSearchbar.delegate = self;
    self.customerList = [self loadCustomers];
    [self renderCollectionView];
    [self layoutSearchBar];
    
    
}

//toggles side menu
- (void)toggleMenu {
    
    if (!self.navigationItem.leftBarButtonItem.enabled) {
        return;
    }
    
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success) {
        if (success) {
            [self.navigationItem.leftBarButtonItem setEnabled:YES];
        }
    }];
    
}

- (void) layoutSearchBar {
    self.customerSearchbar.layer.cornerRadius = 10;
    self.customerSearchbar.layer.masksToBounds = YES;
    self.customerSearchbar.tintColor = [UIColor whiteColor];
}

-(void) renderCollectionView {
    
    CustomerListFlowLayout *layout=[[CustomerListFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(30, 95, self.parentViewController.view.frame.size.width - 60, 600) collectionViewLayout:layout];
    [self.collectionView setCollectionViewLayout:layout];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumInteritemSpacing = 5.0f;
    layout.minimumLineSpacing = 30.0f;
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.scrollEnabled = YES;
    self.collectionView.pagingEnabled = YES;
    
    [self.collectionView registerClass:[CustomerListCollectionViewCell class] forCellWithReuseIdentifier:@"CellIdentifier"];
    
    [self.view addSubview:self.collectionView];
}


-(NSArray*) loadCustomers {
    NSArray * customers = nil;
    
    CustomerDetailsDAO *dao = [CustomerDetailsDAO sharedPersistenceStore];
    
    customers = [dao loadAllCustomersMO];
    
    return customers;
}

#pragma mark - CollectionView Delegates 

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.filteredCustomers.count > 0) {
        [self hideNoCustomerLabel];
        return  self.filteredCustomers.count;
    } else if (self.customerList.count > 0 && isFirstLoad) {
        isFirstLoad = NO;
        
        [self hideNoCustomerLabel];
        
        return self.customerList.count;
    }
    
    [self showNoCustomerLabel];
    return 0;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CustomerListCollectionViewCell *cell= (CustomerListCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"CellIdentifier" forIndexPath:indexPath];
    
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    CustomerDetails *customer = nil;
    
    if (self.filteredCustomers.count > 0 && self.filteredCustomers != nil) {
        customer = (CustomerDetails*)[self.filteredCustomers objectAtIndex:indexPath.row];
    } else {
        customer = (CustomerDetails*)[self.customerList objectAtIndex:indexPath.row];
    }

    cell.customerName.text = [NSString stringWithFormat:@"%@, %@", customer.surname, customer.givenname];
    cell.customerDescription.text = [NSString stringWithFormat:@"%@", customer.gender];
    cell.phoneNumber.text = [NSString stringWithFormat:@"%@", customer.mobile];
    cell.email.text = [NSString stringWithFormat:@"%@", customer.email];
    
    
    cell.layer.borderWidth = 0.8f;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomerDetails *customer = nil;
    
    if (self.filteredCustomers.count > 0 && self.filteredCustomers != nil) {
        customer = (CustomerDetails*)[self.filteredCustomers objectAtIndex:indexPath.row];
    } else if (indexPath.row >= 0) {
        customer = (CustomerDetails*)[self.customerList objectAtIndex:indexPath.row];
    }

    if (customer && [_navTitle isEqualToString:@"Customer List"]) {
        CustomerProfileCDVViewController *cdvViewController = [[CustomerProfileCDVViewController alloc] initWithCustomer:customer];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cdvViewController];
        self.viewDeckController.centerController = nav;
    } else {
        GTKYCDVViewController *gtkyViewController = [[GTKYCDVViewController alloc] initWithCustomer:customer];
        [self.navigationController pushViewController:gtkyViewController animated:YES];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(350, 170);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(20, 40, 20, 40);
}


#pragma mark - searchBar delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText  {
    NSLog(@"searchText %@", searchText);
    
    if (searchText.length == 0 || [searchText isEqualToString:@""]) {
        self.filteredCustomers = self.customerList;
    } else {
        NSPredicate *bySurname = [NSPredicate predicateWithFormat:@"surname contains[cd] %@", searchText];
        NSPredicate *byGivenName = [NSPredicate predicateWithFormat:@"givenname contains[cd] %@", searchText];
        NSPredicate *customerSearch = [NSCompoundPredicate orPredicateWithSubpredicates:@[bySurname, byGivenName]];
        
        self.filteredCustomers = [self.customerList filteredArrayUsingPredicate:customerSearch];
        
        if (self.filteredCustomers.count > 0) {
            NSSortDescriptor *sortBySurname = [[NSSortDescriptor alloc] initWithKey:@"surname" ascending:YES];
            NSSortDescriptor *sortByGivenName = [[NSSortDescriptor alloc] initWithKey:@"givenname" ascending:NO];
            
            [self.filteredCustomers sortedArrayUsingDescriptors:@ [sortBySurname, sortByGivenName]];
        }
    }
    
   
    
    [self.collectionView reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Show/Hide No Customer label

-(void) showNoCustomerLabel {
    self.noCustomers = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x + 80, self.view.frame.origin.y + 180, 180, 40)];
    self.noCustomers.text = @"No customers found!";
    
    [self.view addSubview:self.noCustomers];
}

-(void) hideNoCustomerLabel {    
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
