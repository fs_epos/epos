/**
*	Contact Information, Contact Information Data Provider
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var AboutUsDataProvider = function() {
		this.options = $.extend({}, AboutUsDataProvider.DEFAULTS);
		this.defaults = {
				agentInfo : {
					surname: "Smith",
					givenname: "John",
					licenses: ["PCF", "PCS"],
					description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc"
					
				}
		};
	}
	
	//Define default attributes
	AboutUsDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	AboutUsDataProvider.prototype = {
		constructor : AboutUsDataProvider,
		retrieveAgentInfo: function(queryObject, successCallback, failureCallback){
			if(typeof successCallback == 'function'){
    			console.log("retrieveContactInfo execution")
    			successCallback(this.defaults.agentInfo);
    		}
		}
	}
	//Register the object inside ePOS
	ePos.widget.AboutUsDataProvider = AboutUsDataProvider;
}(ePos, $));