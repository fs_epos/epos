//
//  GTKYCDVPlugin.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import "GTKYCDVPlugin.h"
#import "CountryDAO.h"

@interface GTKYCDVPlugin ()
@property (nonatomic, retain) CustomerDetails *customer;
@end

@implementation GTKYCDVPlugin

+ (NSString *)pluginName {
    return @"ePos.widget.GTKYPanelView";
}

- (id)initWithCustomer:(CustomerDetails *)customer {
    
    self = [super init];
    
    if (self) {
        _customer = customer;
    }
    
    return self;
    
}

- (void)retrieveGeneralInfo:(CDVInvokedUrlCommand *)command {
    
    NSLog(@"retrieveGeneralInfo %@",command.arguments);
    
    CustomerDetailsDAO *DAO = [CustomerDetailsDAO sharedPersistenceStore];
    NSDictionary *customerDetails = [DAO encodeManagedObjectToJSon:_customer];
    
    NSDictionary *genInfo = [NSDictionary dictionaryWithObjectsAndKeys:customerDetails, @"generalInfo", [self countries], @"country", nil];
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:genInfo] callbackId:command.callbackId];
    
    NSLog(@"sending gen info %@",genInfo);
    
}

- (void)onSubmitGeneralInfoForm:(CDVInvokedUrlCommand *)command {

    if (!command.arguments && command.arguments.count != 1) {
        NSLog(@"onSubmitGeneralInfoForm Invalid number of arguments");
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
        return;
    }
    
    NSDictionary *customer = command.arguments[0];
    NSLog(@"Received Customer Data %@",customer);
    
    CustomerDetailsDAO *customerDAO = [CustomerDetailsDAO sharedPersistenceStore];
    NSError *err = nil;
    
    CustomerDetails *customerDetails = [customerDAO updateCustomerDetails:customer error:err];
    
    if (!err) {
        NSLog(@"updated successfully %@",customerDetails);
        //        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
    } else {
        NSLog(@"failed to update %@",err);
        //        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
    }
}

- (NSArray *)countries {
    //retrieve Countries
    CountryDAO *countryDao = [CountryDAO sharedPersistenceStore];
    return [countryDao loadAllCountries];
}

@end
