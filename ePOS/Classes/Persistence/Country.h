//
//  Country.h
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Country : NSManagedObject

@property (nonatomic, retain) NSString * countryName;
@property (nonatomic, retain) NSString * countryCode;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) NSString * label;

@end
