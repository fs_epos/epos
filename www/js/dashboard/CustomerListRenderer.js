/**
*	Dashboard, Customer List Renderer
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerListRenderer = function() {
		this.options = $.extend({}, CustomerListRenderer.DEFAULTS);
	}
	
	//Define default attributes
	CustomerListRenderer.DEFAULTS = {
		initializeData: true
	}
	
	CustomerListRenderer.prototype = {
		constructor : CustomerListRenderer,		

		//html template
		_template: 	'<div class="customer-data clearfix">'
					+'<div class="name"></div>'
					+'<div class="left"><div class="mobile"></div>'
					+'<div class="email"></div></div>'
					+'</div>',
		
		buildCustomerList: function(data) {
			var $widget = $(this._template);
			$(".name", $widget).append(data.surname+", "+data.givenname);
			$(".mobile", $widget).append(data.mobile);
			$(".email", $widget).append(data.email);
			return $widget;
		}
		
	}
	//Register the object inside ePOS
	ePos.widget.CustomerListRenderer = CustomerListRenderer;
}(ePos, $));