/**
*	Dashboard, Customer Event Listener
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerListEventListener = function() {
		this.options = $.extend({}, CustomerListEventListener.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.CustomerListEventListener';
	}
	
	//Define default attributes
	CustomerListEventListener.DEFAULTS = {
		initializeData: true
	}
	
	CustomerListEventListener.prototype = {
		constructor : CustomerListEventListener,
		onOpenCustomerProfile: function(data) {
			var self = this;
    		// cordova call
    		cordova.exec(
				function(success){
					console.log('Resource Panel: Resource opened successfully.');
				},
				function(error){
					console.log('Resource Panel: Something went wrong during the opening.');
				},
				this.PLUGIN_NAME,
				'onOpenCustomerProfile',
				(data ? [data] : [])
    		);
		}	
	}
	//Register the object inside ePOS
	ePos.widget.CustomerListEventListener = CustomerListEventListener;
}(ePos, $));

