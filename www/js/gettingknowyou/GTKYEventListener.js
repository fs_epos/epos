/**
*	Contact Information, Contact Information Event Listener
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var GTKYEventListener = function() {
		this.options = $.extend({}, GTKYEventListener.DEFAULTS);
	}
	
	//Define default attributes
	GTKYEventListener.DEFAULTS = {
		initializeData: true
	}
	
	GTKYEventListener.prototype = {
		constructor : GTKYEventListener,
		onSubmitGeneralInfoForm: function(data) {
			//phonegap
			console.log(JSON.stringify(data, null, 4));
//			alert(JSON.stringify(data, null, 4));
		},
		onSubmitFinancialForm: function(data) {
			//phonegap
			console.log(JSON.stringify(data, null, 4));
//			alert(JSON.stringify(data, null, 4));
		}
	}
	//Register the object inside ePOS
	ePos.widget.GTKYEventListener = GTKYEventListener;
}(ePos, $));

