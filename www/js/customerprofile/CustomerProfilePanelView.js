/**
*	Customer Profile, Customer Profile Panel
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';
	function CustomerProfilePanelView(widgetReference, options, callback) {
		//use the current element instance if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.options = $.extend({}, CustomerProfilePanelView.DEFAULTS, options);
		this.callback = callback;
		this._buildInitialDOM();
	}
	
	//Define default attributes
	CustomerProfilePanelView.DEFAULTS = {
		initializeData: true
	}
	
	CustomerProfilePanelView.prototype = {
		constructor : CustomerProfilePanelView,
		options: null,
		$widget: null,
		callback: null,

		//html template
		_template: 	'<div id="customer-profile-tabs">'
				+'		<ul>'
    			+'			<li><a href="#personal-info">Personal Information</a></li>'
    			+'			<li><a href="#contact-info">Contact Information</a></li>'
 				+'		</ul>'
 				+'		<div id="personal-info"></div>'
				+'		<div id="contact-info"></div>'
				+'	</div>'
		,
		
		_buildInitialDOM: function() {
			this.$widget.append(this._template);
			$("#customer-profile-tabs", this.$widget).tabs();
			if(this.options.initializeData) 
				this.loadData();
			
		},

		loadData: function() {
			var  personalInformation = new ePos.widget.PersonalInformationPanelView(
				'personal-info', 
				{initializeData: true}, 
				function customPersonalInfoCallback(){});

			var  contactInformation = new ePos.widget.ContactInformationPanelView(
				'contact-info', 
				{initializeData: true}, 
				function customContactInfoCallback(){});
		},

	}
	//Register the object inside ePOS
	ePos.widget.CustomerProfilePanelView = CustomerProfilePanelView;
}(ePos, $));