/**
*	Customer Profile, Customer Profile Event Listener
*	Copyright Accenture Inc., ePOS 2015
*	@author Neil Conocono, Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerProfileEventListener = function() {
		this.options = $.extend({}, CustomerProfileEventListener.DEFAULTS);
	}
	
	//Define default attributes
	CustomerProfileEventListener.DEFAULTS = {
		initializeData: true
	}
	
	CustomerProfileEventListener.prototype = {
		constructor : CustomerProfileEventListener,
		onSubmitContactForm: function(data) {
			//phonegap
			console.log(data);
			alert(JSON.stringify(data, null, 4));
		},
		onSubmitPersonalForm: function(data) {
			//phonegap
			console.log(data);
			alert(JSON.stringify(data, null, 4));
		}
	}
	//Register the object inside ePOS
	ePos.widget.CustomerProfileEventListener = CustomerProfileEventListener;
}(ePos, $));

