/**
	BarGraph, Single Graph Panel View
	Copyright Accenture Inc., ePOS 2015
	@author Neil Conocono
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $){
	'use strict';
	var BarGraphItemBasicWidget = function(widgetReference, options, callback) {
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.options = $.extend({}, BarGraphItemBasicWidget.DEFAULTS, options);
		this.callback = callback;
		this._buildInitialDOM();
	};
	
	BarGraphItemBasicWidget.DEFAULTS = {
			max: 0,
			min: 0,
			value: 0, 
			color: '#000000',
			prefix: ' ',
			title: ''
	};
	
	BarGraphItemBasicWidget.prototype = {
			constructor : BarGraphItemBasicWidget,
			options : null,
			$widget : null,
			callback : null,
			
			_template : '<div class="bargraph-title"> </div>'
				+ '<div class="graph-bar">'
				+ '<div class="short-fall">'
				+ '<div class="short-fall-up"> </div>'
				+ '<div class="analysis-value" > </div>'
				+ '<div class="arrow-line"><div class="arrow-up"></div><div class="arrow-down"></div></div>'
				+ '<div class="short-fall-down"> </div>'
				+ '</div>'
				+ '<div class="item-bargraph"> </div>'
				+ '</div>'
				+ '<div class="item-value"> </div>',
	
	_buildInitialDOM: function() {
		this.$widget.append(this._template);
		this.$bargraphitem = $('.item-bargraph', this.$widget)
		this.$shortfall = $('.short-fall', this.$widget);
		this.$bargraphvalue = $('.item-value', this.$widget);
		this.$bargraphtitle = $('.bargraph-title', this.$widget);
		this.$arrowline = $('.arrow-line', this.$widget);
		this.$analysisvaluebefore = $('.analysis-value:before', this.$widget);
		this.$graphbar = $('.graph-bar', this.$widget);
		this.$bargraphtitle.html(this.options.title);
		this.bargraphitem(this.options);
	},
	
	/**
	 * retrieves the height based on the values saved inside the widget
	 * height = value / (max-minimum)
	 * @return Percentage
	 */
	height: function() {
		this.options.max = this.options.min < this.options.max ? this.options.max : this.options.min;
		this.options.min = this.options.max < this.options.min ? this.options.max : this.options.min;
		var height = 1;
		if((parseFloat(this.options.max) - parseFloat(this.options.min))!=0) {
			height = (parseFloat(this.options.value) / ((parseFloat(this.options.max) - parseFloat(this.options.min))))*100;
			height = parseFloat(height) <= parseFloat(1) ? 1 : parseFloat(height);
		}
		height = height > 100 ? 100 : height;
		return Math.floor(height);
	},
	
	/**
	 * Sets the color of the bargraph item
	 * returns black color if not valid hexacolor
	 * @param {number} [color] - color of the bargraphitem
	 * @return color
	 */
	_isHexaColor: function(color){
		  if ((typeof color === "string") && color.length === 6 
	         && ! isNaN( parseInt(color, 16) )) {
			  return "#"+color;
		  } else {
			  return '#000000';
		  }
	},
	
	/**
	 * sets the bargraphitem values
	 * @param bargraphitem
	 * @param [bargraphitem.value] - value of the bargraph item
	 * @param [bargraphitem.min] - minimum possible value of the bargraph
	 * @param [bargraphitem.max] - maximum possible value of the bargraph
	 * @param [bargraphitem.color] - color of the bargraph
	 */
	bargraphitem: function(bargraphitem) {
		var _self = this;
		bargraphitem = typeof bargraphitem === 'undefined' || bargraphitem == null ? this.settings : bargraphitem;
		this.options.value = typeof bargraphitem.value === 'undefined' || bargraphitem.value == null ? this.options.value : bargraphitem.value;
		this.options.min = typeof bargraphitem.min === 'undefined' || bargraphitem.min == null ? this.options.min : bargraphitem.min;
		this.options.max = typeof bargraphitem.max === 'undefined' || bargraphitem.max == null ? this.options.max : bargraphitem.max;
		this.options.prefix = typeof bargraphitem.prefix === 'undefined' || bargraphitem.prefix == null ? this.options.prefix : bargraphitem.prefix;
		
		var displayvalue = Math.round(this.options.value);
		this.$bargraphvalue.html(this.options.prefix + " " + displayvalue.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ","));
		if(bargraphitem.color) {
			var height = this.height();
			
			if(this.$shortfall.is(':visible') && parseFloat(height) > 92) {
				this.$arrowline.hide();
			} else { 
				this.$arrowline.show();
			}
			
			this.$bargraphitem.height(height+"%").css({
				backgroundColor: _self._isHexaColor(bargraphitem.color)
			});
			//this.$bargraphvalue.css('color',_self._isHexaColor(bargraphitem.color));
		}
		
		var shortFallHeight = (100 - height) - 1;
		this.$shortfall.css('height',shortFallHeight+'%');
		// Adjust the bar graph if header if text too long
		var deductHeight = ''
		if (this.$bargraphtitle.height() > 22 && !this.$shortfall.is(':visible')) {
			deductHeight = 440 - (this.$bargraphtitle.height() - 22);
			this.$graphbar.css('height', deductHeight+'px');
		}
		//this.$bargraphvalue.css('color',_self._isHexaColor(bargraphitem.color));
	}, 
	
	/**
	 * @param value - value of the bargraph item
	 * @return int 
	 * sets value of the bargraph item
	 * returns value if param is null
	 */
	value: function(value) {
		if(typeof value === 'undefined') {
			return this.options.value;
		} else {
			this.options.value = value < this.options.min ? this.options.min : value;
			this.options.value = value > this.options.max ? this.options.max : value;
			this.bargraphitem({value: value});
		}
	},
	
	/**
	 * @param min - minimum value of the bargraph item
	 * @return int 
	 * sets minimum value of the bargraph item
	 * returns minimum value if param is null
	 */
	min: function(min) {
		if(typeof min === 'undefined') {
			return this.options.min;
		} else {
			this.options.min = min;
			this.bargraphitem({min: min});
		}
	},
	
	/**
	 * @param max - maximum value of the bargraph item
	 * @return int 
	 * sets maximum value of the bargraph item
	 * returns maximum value if param is null
	 */
	max: function(max) {
		if(typeof max === 'undefined') {
			return this.options.max;
		} else {
			this.options.max = max;
			this.bargraphitem({max: max});
		}
	},
	
	/**
	 * @param prefix - sets prefix to be appended on displayed value
	 * @return String 
	 * sets maximum value of the bargraph item
	 * returns maximum value if param is null
	 */
	prefix: function(prefix) {
		if(typeof prefix === 'undefined') {
			return this.options.prefix;
		} else {
			this.options.prefix = prefix;
			this.bargraphitem({prefix: prefix});
		}
	},
	}
	
	ePos.widget.BarGraphItemBasicWidget = BarGraphItemBasicWidget;
}(ePos, $));
