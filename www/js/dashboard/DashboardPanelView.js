/**
*	Dashboard, Dashboard Panel View
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var DashboardPanelView = function(widgetReference, options, callback) {
		// used the current element instace if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $)? widgetReference: $('#' + widgetReference);
		this.options = $.extend({}, DashboardPanelView.DEFAULTS, options);
		this.callback = callback;
		this._buildInitialDOM();
	}
	
	//Define default attributes
	DashboardPanelView.DEFAULTS = {
		initializeData: true
	}
	
	DashboardPanelView.prototype = {
		constructor : DashboardPanelView,
		options: null,
		$widget: null,
		callback: null,
		
		//html template
		_template: 	'<div class="dashboard-panel">'
			+		'	<div class="customer-panel"></div>'
			+		'	<div class="dashboard-panel-right">'
			+		'		<div class="videos-panel">'
			+		'		</div>'
			+		'		<div class="resources-panel">'
			+		'		</div>'
			+		'		<div class="notification-panel">'
			+		'		</div>'
			+		'	</div>'
			+		'</div>'
		,
		
		_buildInitialDOM: function() {
			this.$widget.append(this._template);
			if(this.options.initializeData)
				this.loadData();
		},
		
		loadData: function() {
			var customerList = new ePos.widget.CustomerListPanelView(
					'customer-panel', 
					{
						initializeData: true
					}, 
					function customCustomerCallback(){}
				);
			
			var resource = new ePos.widget.ResourcePanelView(
					'resources-panel', 
					{
						initializeData: true
					}, 
					function customResourceCallback(){}
				);
			
			var video = new ePos.widget.VideoPanelView(
				'videos-panel', 
				{
					initializeData: true
				}, 
				function customVideoCallback(){}
			);
			
			/*var _self = this;
			if(typeof this.dataprovider.getCustomerList == 'function') {
				var data = this.dataprovider.getCustomerList();
				$(data).each(function(index, cust){
					var $clickCustomer = _self.customerRenderer.buildCustomerList(cust);
					$(".customer-lists", _self.$widget).append($clickCustomer);
					$clickCustomer.on('click', function(evt){
						console.log(cust);
    					_self.customerEventListener.onOpenCustomerProfile(cust);
    				});

				})
			}*/			
		}
		
	}
	//Register the object inside ePOS
	ePos.widget.DashboardPanelView = DashboardPanelView;
}(ePos, $));