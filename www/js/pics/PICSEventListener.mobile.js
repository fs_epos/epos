/**
*	PICS, PICS Event Listener mobile
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var PICSEventListener = function() {
		this.options = $.extend({}, PICSEventListener.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.PICSPanelView';
	}
	//Define default attributes
	PICSEventListener.DEFAULTS = {
		initializeData: true
	}
	
	PICSEventListener.prototype = {
		constructor : PICSEventListener,
		onSavePICSInfo: function(information) {
			var self = this;
    		// cordova call
    		cordova.exec(
				function(success){
					console.log('PICS: Form submitted successfully.');
				},
				function(error){
					console.log('PICS: Something went wrong during form submission.');
				},
				this.PLUGIN_NAME,
				'onSavePICSInfo',
				(information ? [information] : [])
    		);
		}			
	}	
	//Register the object inside ePOS
	ePos.widget.PICSEventListener = PICSEventListener;
}(ePos, $));

