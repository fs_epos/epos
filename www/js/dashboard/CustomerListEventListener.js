/**
*	Dashboard, Customer Event Listener
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerListEventListener = function() {
		this.options = $.extend({}, CustomerListEventListener.DEFAULTS);
	}
	
	//Define default attributes
	CustomerListEventListener.DEFAULTS = {
		initializeData: true
	}
	
	CustomerListEventListener.prototype = {
		constructor : CustomerListEventListener,
		onOpenCustomerProfile: function(data) {
			//phonegap
			alert(data);
		}	
	}
	//Register the object inside ePOS
	ePos.widget.CustomerListEventListener = CustomerListEventListener;
}(ePos, $));

