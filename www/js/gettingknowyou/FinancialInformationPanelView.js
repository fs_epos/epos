/**
	GTKY, Financial Information Panel View
	Copyright Accenture Inc., ePOS 2015
	@author Neil Conocono
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $){
	'use strict';
	
	var FinancialInformationPanelView = function(widgetReference, options, callback) {
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.options = $.extend({}, FinancialInformationPanelView.DEFAULTS);
		this._generalInformation = {};
		this.validation = new ePos.widget.ValidationUtility();
		this.callback = callback;
		this._buildInitialDOM();
		if(this.options.initializeData)
			this._loadData();
	}
	
	FinancialInformationPanelView.DEFAULTS = {
		initializeData : true,
		dataprovider : new ePos.widget.GTKYDataProvider(),
		eventlistener: new ePos.widget.GTKYEventListener(),
	},
	FinancialInformationPanelView.prototype = {
		constructor : FinancialInformationPanelView,
		options : null,
		$widget : null,
		callback : null,
		
		//html template
		_template : 	'<div class="financial-information-panel">'
			+			'<div class="detail-panel">'
			+				'<div class="heading"> <h4 class="financial-information-title"> </h4></div>'
			+					'<div class="edit-mode">'
			+						'<div class="occupation-field field-container"> <span class="occupation-label"> </span><input type="text" id="occupation-value" class="input-text"/> </div>'
			+						'<div class="monthly-income-range field-container"> <span class="income-label"> </span> <input type="text" id="income-range-text" data-mask="000,000,000" />  <div class="slider-income-range"> </div> </div>'
			+						'<div class="target-amount-range field-container"> <span class="target-label"> </span> <input type="text" id="target-range-text" data-mask="000,000,000" />  <div class="slider-target-range"> </div> </div>'
			+						'<div class="age-fields-range field-container">'
			+							'<span class="age-input"> <span class="age-label"> </span><input type="text" id="age-range-text" />  </span> <span class="target-age-input"> <span class="target-age-label"> </span><input type="text" id="targetage-range-text" /> </span> <div class="slider-age-range"> </div>  </div>'
			+					'</div>'
			+					'<div class="view-mode">'
			+						'<div class="client-occupation clearfix"> <span class="occupation-label text-label"> </span> <span class="occupation-display-value"> </span> </div>'
			+						'<div class="client-income clearfix"> <span class="income-label text-label"> </span> <span class="income-display-value"> </span> </div>'
			+						'<div class="client-target clearfix"> <span class="target-label text-label"> </span> <span class="target-display-value"> </span> </div>'
			+						'<div class="client-age clearfix"> <span class="age-label text-label"> </span> <span class="age-display-value"> </span> </div>'
			+						'<div class="client-targetage clearfix"> <span class="targetage-label text-label"> </span> <span class="targetage-display-value"> </span> </div>'
			+					'</div>'
			+				'</div>'
			+			'<div class="graph-panel">'
			+ 				'<div class="graph-container" id="monthlysavings-graph"> </div>'
			+			'</div>'
			+			'<div class="financial-information-buttons">'
			+				'<span class="button-span button-edit"> <button id = "button-edit" class="button positive" /></span>'
			+				'<span class="button-span button-save"> <button id = "button-save" class="button positive" /></span>'
			+				'<span class="button-span button-cancel"> <button id = "button-cancel" class="button positive" /></span>'
			+			'</div>'
			+		'</div>',
			
		_buildInitialDOM: function() {
			this.$widget.append(this._template);
			this._initializeLabels();
			this.$age = $('.financial-information-panel .slider-age-range', this.$widget);
			this.$income = $('.financial-information-panel .slider-income-range', this.$widget);
			this.$target = $('.financial-information-panel .slider-target-range', this.$widget);
			this.$incometext =  $('.financial-information-panel #income-range-text', this.$widget);
			this.$targettext =  $('.financial-information-panel #target-range-text', this.$widget);
			this.$agetext =  $('.financial-information-panel #age-range-text', this.$widget);
			this.$targetagetext =  $('.financial-information-panel #targetage-range-text', this.$widget);
			this.$bargraph = new ePos.widget.BarGraphItemBasicWidget(
               		'monthlysavings-graph',
               		{title: "Target Monthly Savings",
               		color: '1471ad',
               		prefix: '$'},
               		function() {}
			);
			this.$viewmode = $('.financial-information-panel .view-mode', this.$widget);
			this.$editmode = $('.financial-information-panel .edit-mode', this.$widget);
		},
		
		/**
		 * @param {JSON} data - data to be displayed
		 * @param int mode - 0 - view mode 1 - edit mode 
		 */
		_switchMode: function(data, mode) {
			var values = data ? data : 0;
			if(mode) {
				this.$editmode.show();
				this.$viewmode.hide();
				if(values){
					$('.financial-information-panel #occupation-value', this.$widget).val(this._sanitizeString(values.occupation));
					this.$agetext.val(values.age);
					this.$targettext.val(values.target);
					this.$incometext.val(values.monthlyincome);
					this.$targetagetext.val(values.targetage);
				}
				$('.financial-information-panel .button-save', this.$widget).show();
				$('.financial-information-panel .button-cancel', this.$widget).show();
				$('.financial-information-panel .button-edit', this.$widget).hide();
			} else {
				this.$viewmode.show();
				this.$editmode.hide();
				if(values) {
					$('.financial-information-panel .occupation-display-value', this.$widget).html(this._sanitizeString(values.occupation));
					$('.financial-information-panel .income-display-value', this.$widget).html(this._sanitizeString(values.monthlyincome));
					$('.financial-information-panel .target-display-value', this.$widget).html(this._sanitizeString(values.target));
					$('.financial-information-panel .age-display-value', this.$widget).html(this._sanitizeString(values.age));
					$('.financial-information-panel .targetage-display-value', this.$widget).html(this._sanitizeString(values.targetage));
				}
				$('.financial-information-panel .button-save', this.$widget).hide();
				$('.financial-information-panel .button-cancel', this.$widget).hide();
				$('.financial-information-panel .button-edit', this.$widget).show();
			}
		},
		
		_saveData: function() {
			var validationResult = this.validation.validateInput(this, $('input', this.$widget));
			if(validationResult) {
				$('.personal-information-form', this.$widget).find('.error').parent().find('input').removeClass('error-field');
				var newFinancialInfoData = {};
				newFinancialInfoData = {
						'occupation' : $('.financial-information-panel #occupation-value', this.$widget).val(),
						'target' : this.$targettext.val(),
						'age' : this.$agetext.val(),
						'targetage' : this.$targetagetext.val(),
						'monthlyincome' : this.$incometext.val()
				}
				this.options.eventlistener.onSubmitFinancialForm(newFinancialInfoData);
			
				this._switchMode(newFinancialInfoData, 0);
			}
		},
		
		_loadData: function() {
			var _self = this;
			if(typeof this.options.dataprovider.getFinancialInfo == 'function') {
				this.options.dataprovider.getFinancialInfo(null, 
				function(data){
					_self._initializeSliders(data);
					_self._initializeEventListeners();
					_self._switchMode(data.values, 1);
					_self._switchMode(data.values, 0);
					_self._calculateMonthlySavings();
				},
				function(err) {
					console.log(err);
				});
			}
		},
		
		_initializeLabels: function() {
			$('.financial-information-panel .occupation-label', this.$widget).html("Occupation: ");
			$('.financial-information-panel .income-label', this.$widget).html("Monthly Income: ");
			$('.financial-information-panel .target-label', this.$widget).html("Target Savings: ");
			$('.financial-information-panel .age-label', this.$widget).html("Age: ");
			$('.financial-information-panel .targetage-label', this.$widget).html("Target Age: ");
			$('.financial-information-panel .financial-information-title', this.$widget).html("Financial Information");
		},
		
		_initializeSliders: function(data) {
			var maxincome = 999999999;
			var minincome = 0;
			var maxtarget = 99999999;
			var mintarget = 0;
			var income = 0;
			var target = 0;
			var age = 0;
			var targetage = 99;
			var defaults = data ? data.defaults : null;
			var values = data ? data.values : null;
			if(defaults) {
				maxincome = defaults.income && defaults.income.max ? defaults.income.max : 0;
				minincome = defaults.income && defaults.income.min ? defaults.income.min : 0;
				maxtarget = defaults.target && defaults.target.max ? defaults.target.max : 0;
				mintarget = defaults.target && defaults.target.min ? defaults.target.min : 0;
			}
			
			if(values) {
				income = values.income;
				target = values.target;
				targetage = values.targetage;
				age = values.age;
			}
			
			this.$income.noUiSlider({
				start: [ income],
				range: {
					'min': minincome,
					'max': maxincome
				}, format: {
				  to: function (value) {
					return parseFloat(value).toFixed(0);
				  },
				  from: function (value) {
					return parseFloat(value).toFixed(0);
				  }
				},
				
			});
			
			this.$target.noUiSlider({
				start: [ target],
				range: {
					'min': mintarget,
					'max': maxtarget
				}, format: {
				  to: function (value) {
					return parseFloat(value).toFixed(0);
				  },
				  from: function (value) {
					return parseFloat(value).toFixed(0);
				  }
				},
				
			});
			
		
			this.$age.noUiSlider({
				start: [age, targetage],
				connect: true,
				range: {
					'min': 0,
					'max': 99
				},
			
				format: {
				  to: function (value) {
					return parseFloat(value).toFixed(0);
				  },
				  from: function (value) {
					return parseFloat(value).toFixed(0);
				  }
				},
			});
			
		},
		
		_initializeEventListeners: function() {
			var _self = this;
			this.$income.Link('lower').to(this.$incometext);
			this.$target.Link('lower').to(this.$targettext);
			this.$age.Link('lower').to(this.$agetext);
			this.$age.Link('upper').to(this.$targetagetext);
			this.$age.on({
				change: function(){
					_self._calculateMonthlySavings();
				},
			});
			this.$target.on({
				change: function(){
					_self._calculateMonthlySavings();
				},
			});
			this.$income.on({
				change: function(){
					_self._calculateMonthlySavings();
				},
			});
			
			$('.financial-information-panel .button-edit', this.$widget).on("click", function(evt) {
				_self._switchMode(null, 1);
			});
			
			$('.financial-information-panel .button-save', this.$widget).on("click", function(evt) {
				_self._saveData();
			});
			
			$('.financial-information-panel .button-cancel', this.$widget).on("click", function(evt) {
				_self._switchMode(null, 0);
			});
		},
		
		_updateGraph: function(max, min, value) {
			this.$bargraph.bargraphitem({
				max: max,
				min: min,
				value: value, 
				title: 'Target Monthly Savings'
			});
		},
		
		_calculateMonthlySavings: function(){
			var monthlysavings = this.$targettext.val() / (this.$targetagetext.val() - this.$agetext.val());
			this._updateGraph(10000, 0, monthlysavings);
		},
		
		_sanitizeString : function(object) {
			return object ? object : "";
		}
		
	},
	
	
	ePos.widget.FinancialInformationPanelView = FinancialInformationPanelView;
}(ePos, $));