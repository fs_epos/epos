//
//  DashboardCDVViewController.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/4/15.
//
//

#import "DashboardCDVViewController.h"
#import "CustomerListCDVPlugin.h"
#import "VideoPanelCDVPlugin.h"
#import "ResourcePanelCDVPlugin.h"
#import "ResourceViewController.h"
#import "CustomerListEventCDVPlugin.h"
#import "CustomerProfileCDVViewController.h"

@interface DashboardCDVViewController () <VideoPanelCDVPluginDelegate, ResourcePanelCDVPluginDelegate, CustomerListEventCDVPluginDelegate>

@end

@implementation DashboardCDVViewController
- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
        
        self.startPage = @"Dashboard.html";
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    // View defaults to full size.  If you want to customize the view's size, or its subviews (e.g. webView),
    // you can do so here.
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [button setImage:[UIImage imageNamed:@"sideMenu"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(toggleMenu) forControlEvents:UIControlEventAllTouchEvents];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    [self.navigationItem setLeftBarButtonItem:item];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.0f green:183.0f/255.0f blue:242.0f/255.0f alpha:1]];
    
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:self.navigationController.navigationBar.frame];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setTextAlignment:NSTextAlignmentCenter];
    [titleLabelView setFont:[UIFont boldSystemFontOfSize:22]];
    titleLabelView.text = NSLocalizedString(@"Dashboard", @"");
    
    self.navigationController.navigationBar.topItem.titleView = titleLabelView;

    // Disable web view bouncing
    self.webView.scrollView.bounces = NO;
    
    CustomerListCDVPlugin *customerListPlugin = [[CustomerListCDVPlugin alloc] init];
    [self registerPlugin:customerListPlugin withPluginName:[CustomerListCDVPlugin pluginName]];
    
    VideoPanelCDVPlugin *videoPanelPlugin = [[VideoPanelCDVPlugin alloc] init];
    videoPanelPlugin.delegate = self;
    [self registerPlugin:videoPanelPlugin withPluginName:[VideoPanelCDVPlugin pluginName]];
    
    ResourcePanelCDVPlugin *resourcePlugin = [[ResourcePanelCDVPlugin alloc] init];
    resourcePlugin.delegate = self;
    [self registerPlugin:resourcePlugin withPluginName:[ResourcePanelCDVPlugin pluginName]];
    
    CustomerListEventCDVPlugin *eventPlugin = [[CustomerListEventCDVPlugin alloc] init];
    eventPlugin.delegate = self;
    [self registerPlugin:eventPlugin withPluginName:[CustomerListEventCDVPlugin pluginName]];
}

- (void)toggleMenu {
    
    if (!self.navigationItem.leftBarButtonItem.enabled) {
        return;
    }
    
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success) {
      if (success) {
          [self.navigationItem.leftBarButtonItem setEnabled:YES];
      }
  }];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


/* Comment out the block below to over-ride */

/*
 - (UIWebView*) newCordovaViewWithFrame:(CGRect)bounds
 {
 return[super newCordovaViewWithFrame:bounds];
 }
 */

#pragma mark - VideoPanelCDVPluginDelegate

- (void)videoPanelCDVPluginDelegate:(VideoPanelCDVPlugin *)plugin onOpenVideo:(NSString *)params {
    
    ResourceViewController *viewController = [[ResourceViewController alloc] initWithResourceType:params];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
}

#pragma mark - ResourcePanelCDVPluginDelegate

- (void)resourcePanelCDVPlugin:(ResourcePanelCDVPlugin *)plugin onOpenResource:(NSString *)params {
    
    ResourceViewController *viewController = [[ResourceViewController alloc] initWithResourceType:params];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
    
}

#pragma mark - CustomerListEventCDVPluginDelegate

- (void)customerListEventCDVPlugin:(CustomerListEventCDVPlugin *)plugin onOpenCustomerProfile:(CustomerDetails *)customer {
    CustomerProfileCDVViewController *cdvViewController = [[CustomerProfileCDVViewController alloc] initWithCustomer:customer];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cdvViewController];
    self.viewDeckController.centerController = nav;
}

@end