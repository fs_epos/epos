/**
*	Dashboard, Customer List Data Provider
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerListDataProvider = function() {
		this.options = $.extend({}, CustomerListDataProvider.DEFAULTS);
		this.defaults = {
			customerList : [ 
    					{
							"id" : 200,
							"surname" : "Son",
							"givenname" : "Ray",
							"mobile" : "09123456789",
							"email" : "ray.son@email.com"
						}, {
							"id" : 201,
							"surname" : "Doe",
							"givenname" : "John",
							"mobile" : "0923458789",
							"email" : "john.doe@email.com"
						}, {
							"id" : 202,
							"surname" : "Alice",
							"givenname" : "Maria",
							"mobile" : "0932154985",
							"email" : "alice.maria@email.com"
						}, {
							"id" : 203,
							"surname" : "Kun",
							"givenname" : "Kai",
							"mobile" : "0945646546",
							"email" : "kai.kun@email.com"
						}, {
							"id" : 204,
							"surname" : "Koide",
							"givenname" : "Arisa",
							"mobile" : "0932132654",
							"email" : "koide.arisa@email.com"
						},  {
							"id" : 205,
							"surname" : "Raymundo",
							"givenname" : "Ralph",
							"mobile" : "0932132654",
							"email" : "koide.arisa@email.com"
						}
						
					]
		};
	}
	
	//Define default attributes
	CustomerListDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	CustomerListDataProvider.prototype = {
		constructor : CustomerListDataProvider,
		getCustomerList: function(queryObject, successCallback, failureCallback) {
			successCallback(this.defaults.customerList);
		}
		
	}
	//Register the object inside ePOS
	ePos.widget.CustomerListDataProvider = CustomerListDataProvider;
}(ePos, $));

