//
//  CustomerListEventCDVPlugin.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/11/15.
//
//

#import <Cordova/CDVPlugin.h>
#import "CustomerDetailsDAO.h"

@class CustomerListEventCDVPlugin;

@protocol CustomerListEventCDVPluginDelegate <NSObject>
@required
- (void)customerListEventCDVPlugin:(CustomerListEventCDVPlugin *)plugin onOpenCustomerProfile:(CustomerDetails *)customer;
@end

@interface CustomerListEventCDVPlugin : CDVPlugin
@property (nonatomic, weak) id <CustomerListEventCDVPluginDelegate> delegate;
+ (NSString *)pluginName;
- (void)onOpenCustomerProfile:(CDVInvokedUrlCommand *)command;


@end
