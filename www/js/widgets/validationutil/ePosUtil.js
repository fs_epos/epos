/**
 * Common Utilities used in ePOS
 * © Accenture ePOS 2015
 * @author Neilbrian Bernardio
 */

// define namespace
var ePos = ePos || {};
ePos.util = ePos.util || {};

/**
 * Validates if the value is alphabet only
 * @param value (string) the name that needs to be validated
 * @return boolean
 */
ePos.util.isValidEnglishName = function(value){
		return value.toString().match(/[\u3400-\u9FBF]/) == null && value.toString().match(/[a-z ]/gi) != null;
};

/**
 * Validates if the value is a chinese character
 * @param value (string) the name that needs to be validated
 * @return boolean
 */
ePos.util.isChineseCharacter = function(value){
		return value.toString().match(/[\u3400-\u9FBF]/) != null;
};

/**
 * Validates if the value has a correct email format
 * @param value (string) the email that needs to be validated
 * @return boolean
 */
ePos.util.isValidEmail = function(value){
		return value.trim().match(/^[\w.-]+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}(\.[a-zA-Z]{2,6})?$/) != null && value.trim().match(/\.(\.|@)/gi) == null;
};

/**
 * Validates if the value is empty
 * @param value (string) the string that needs to be validated
 * @return boolean
 */
ePos.util.isNotEmptyString = function(value){
		return value.trim().length > 0;
};

/**
 * Determines if the value is in between the given range
 * @param value (int) the number to be look for if its within range
 * @param minRange (int) the minimum treshold
 * @param maxRange (int) the maximum treshold
 * @return boolean
 */
ePos.util.between = function(value, minRange, maxRange) {
	  return value >= minRange && value <= maxRange;
};

/**
 * Calculates the age with regards to the persons birthday and current date
 * @param birthday (string/Date Obj) the birthday of the person
 * @return int, the calculated age
 */
ePos.util.calculateAge = function(birthdate){
	var age = 0;
	if(birthdate){
		var a = moment(birthdate);
		var b = moment(new Date());
		age = b.diff(a, 'years');
	}
	
	return age;
};

/**
 * Determine if the given object is null or undefined
 * @param obj (Object) the object that needs to be determined
 * @return boolean
 */
ePos.util.isNullOrUndefined = function(obj){
	return (obj == null) || (typeof obj === 'undefined');
};

/**
 * Gets the value of the specified field, else return the default value
 * @param obj (Object) the object where the value is retrieved
 * @param path (string) the specific field
 * @param defaultValue (mixed) in case path is not found, this value will be returned
 * @return mixed, the field value
 */
ePos.util.getObjectValue = function(obj, path, defaultValue) {
	if( ePos.util.isNullOrUndefined(path) ){
		throw 'path not specified!';
	}
	var field = defaultValue;
	if( !ePos.util.isNullOrUndefined(obj) ){		
		var pathComponents = path.split('.');
		for(i=0; i<pathComponents.length; i++) { 
			var pathComponent = pathComponents[i];
			field = obj[pathComponent];
			obj = field;
			if( ePos.util.isNullOrUndefined(field) ){
				field = defaultValue;
				break;
			}
		}
	}
	return field;
};

/**
 * Format the number values with comma if thousands and 2 decimal places
 * @param val (numeric) the object where the value is retrieved
 * @return string, comma formatted number with 2 decimal places
 */
ePos.util.commaSeparatedNumber = function(val){
		val = parseFloat(val).toFixed(2);
	    while (/(\d+)(\d{3})/.test(val.toString())){
	      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
	    }
	    return val;
};

/**
 * This will capitalize the words in the string
 * @param str (string) all words contained in the string will be capitalize
 * @return string, capitalize words
 */
ePos.util.capitalizeWords = function(str){
	return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

/**
 * Determines if the given value is Interger or not
 * @param num (number) the value that will be evaluated
 * @return boolean
 */
ePos.util.isInt = function(num){
    return Number(num) === num && num % 1 === 0;
}


