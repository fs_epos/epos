/**
*	Dashboard, Video Panel View
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';
	function VideoPanelView(widgetReference, options, callback) {
		//use the current element instance if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('.' + widgetReference);
		this.dataProvider = new ePos.widget.VideoPanelDataProvider(),
		this.eventListener = new ePos.widget.VideoPanelEventListener(),
		this.options = $.extend({}, VideoPanelView.DEFAULTS, options);
		this.callback = callback;
		this._videoId = null;
		
		this._buildInitialDOM();
	}
	
	//Define default attributes
	VideoPanelView.DEFAULTS = {
		initializeData: true,
		// videoId: null,
		// videoEventListener: null
	}
	
	VideoPanelView.prototype = {
		constructor : VideoPanelView,
		options: null,
		$widget: null,
		callback: null,
		
		//html template
		_template:	'<div class="video-holder"></div>',
				
		_buildInitialDOM: function() {
			
			this.$widget.append(this._template);
			this._initializeEvents();
			if(this.options.initializeData) 
				this.loadData();
						
			// this._initializeLabels();
		},
				
		loadData: function() {
			var _self = this;
			
			if(typeof this.dataProvider.retrieveVideo == 'function') {
				this.dataProvider.retrieveVideo(null,
					function(videoId){
						_self._videoId = videoId;
					},
					function(){
						console.log('Video Panel loadData: Data retrieval unseccessful');
					}
				);	
			}
		},
				
		// _initializeLabels: function() {
			// $('.video-title', this.$widget).html('Videos');
		// }, 
		
		_initializeEvents: function() {
			var _self = this;
						
			$('.video-holder', this.$widget).on('click', function(evt) {
				_self.openVideo(_self);
			});			
		},
		
		openVideo: function(_self){
			// TODO: eventlistener
			// _self.options.videoEventListener.onOpenVideo(_self.options.videoId);
			_self.eventListener.onOpenVideo(_self._videoId);
		}		
	}
	//Register the object inside ePOS
	ePos.widget.VideoPanelView = VideoPanelView;
}(ePos, $));