/**
* StepPagePanelView Implementation
*/
var StepPagePanelView = function(){
	
	this.parentObj = new PagePanelView();
	
	this.TAG = "clientprofile.common.util.ui.steppagination.StepPagePanelView";
	
	this.init= function(widgetReference, options) {
		
		this.defaults = {
				show: false,
				errorClass: 'field-error',
				showDropDown: false
		};
		//this._super(widgetReference, $.extend({}, this.defaults, options));
		this.$widget = $(widgetReference);
		this.settings = $.extend({}, this.defaults, options);

		this.parentObj.init(widgetReference, this.settings);
		this._buildInitialDOM();
	};
	
	this._stepPageTemplate = '<footer class="step-view row">'
				+'	<div class="step-bar">'
				+'		<div class="step-label">'
				+			'<span class="step"></span>'
//				+			tds.util.Translator.translate(this.TAG+'.label.stepLabel')
				+			' <span class="current-step"></span>' 
				+			'<span class="out"></span>'
//				+			tds.util.Translator.translate(this.TAG+'.label.outLabel')
				+			'<span class="last-step"></span> <span class="up-arrow"></span>'
				+		'</div>'
				+'		<div class="status-bar"><span></span></div>'
				+'	</div>'
				+'	<div id="footer-menu" class="footer-menu"><ul class="step-tracker"></ul></div>'
				+'</footer>';	
				
	this._buildInitialDOM= function() {
		var _self = this;
		//this._super();
		//this.parentObj._buildInitialDOM();
		this.$widget.append(this._stepPageTemplate);
		this.$pageNext = $('.page-next', this.parentObj.$widget);
		this.$pagePrev = $('.page-prev', this.parentObj.$widget);
		this.$stepView = $('.step-view', this.$widget);
		this.$stepBar = $('.step-bar', this.$widget);
		this.$stepLabel = $('.step-label', this.$widget);	
		this.$stepTracker = $('.step-tracker', this.$widget);
		this.$arrow = $('.up-arrow', this.$widget);
		this.$statusBar = $('.status-bar span', this.$widget);
		this.$currentStep = $('.current-step', this.$widget);
		this.$lastStep = $('.last-step', this.$widget);
		this.$proceed = $('.button', this.$widget);
		$currentIndex = this.settings.activeIndex;
		var action = ('ontouchstart' in window) ? "tap" : "click";
		$('.page-view-slide', this.$widget).on(action, function(evt){
			_self.hideFooterMenu();
		});
		$('.step', this.$widget).html("Step");
		$('.out', this.$widget).html(" out of ");
		this.parentObj.swipeInstance.onSlideChangeEnd= function(swiper) {        		
    		_self.slideEndChangeHandler(swiper.activeIndex);
        };
        
        this.$pageNext.stop(true,true).on(action, function(evt){
			var action = _self.parentObj.getAction(this);
			_self.parentObj.doAction(action);
			_self.slideEndChangeHandler(_self.parentObj.getActiveIndex());
		});
		
		//when user clicks the previous       
		this.$pagePrev.stop(true,true).on(action, function(evt){
			var action = _self.parentObj.getAction(this);
			_self.parentObj.doAction(action);
			_self.slideEndChangeHandler(_self.parentObj.getActiveIndex());
		});

	};
	
	/**
	 * handler when slide change is ended
	 * @param  {[type]} index [description]
	 * @return {[type]}       [description]
	 */
	this.slideEndChangeHandler= function($currentIndex) {
		var _self = this;
		//this._super($currentIndex);
		this.parentObj.slideEndChangeHandler($currentIndex);
		this.$currentStep.text(parseInt($currentIndex) + 1);
		this.$lastStep.text(this.parentObj.getPageCount());
		var $proceedBtn = $('.button', this.$widget);
		if(($currentIndex + 1) == this.parentObj.getPageCount()) {
			$proceedBtn.show();
		} else {
			$proceedBtn.hide();
		}
		
		if( _self._calculateProgressBar() >= 100 )
			this.$statusBar.addClass('last');
		else
			this.$statusBar.removeClass('last');
		
		this.$statusBar.css('width', _self._calculateProgressBar()+'%');
		this.$stepTracker.find('li').removeClass('selected');
		this.$stepTracker.find('li:eq('+this.parentObj.getActiveIndex()+')').addClass('selected');
	};
	
	this.addPage= function(page) {
		var _self = this;
		//this._super(page);
		this.parentObj.addPage(page);
		this.$currentStep.text(parseInt(this.parentObj.getActiveIndex()) + 1);
		this.$lastStep.text(this.parentObj.getPageCount());
		if( _self._calculateProgressBar() >= 100 )
			this.$statusBar.addClass('last');
		else
			this.$statusBar.removeClass('last');
		this.$statusBar.css('width', _self._calculateProgressBar()+'%');
	};
	
	this.addStep= function(step, index) {
		var _self = this;
		if (this.settings.showDropDown === true) {
			
			var $tab = $('<li>');
			$tab.text(step);
			_self.$stepTracker.append($tab);
			
			if (index == 0) $tab.addClass('selected');
			
			_self.$stepLabel.off();
			var action = ('ontouchstart' in window) ? "tap" : "click";
			_self.$stepLabel.on(action, function(evt) {
				var $footerMenu = _self.$stepView.find('#footer-menu');
				if ($footerMenu.hasClass('showed')) {
					_self.hideFooterMenu();
				} else {
					_self.showFooterMenu();
				}
			});
			$tab.on(action, function(evt) {
				var $index = $(this).index(); // the selected index
				var $activeIndex = _self.parentObj.getActiveIndex(); // the active index
				var slideHtml = _self.parentObj.swipeInstance.slides[$activeIndex]; 
				var $errorIndex = $('.'+_self.settings.errorClass, _self.$pageUl).parent().length; // the error index
				
				if($errorIndex && $index > $errorIndex) {
					_self.settings.errorIndex = $errorIndex;
					$('.info-msg', _self.$pageUl).text('');
					_self.$stepTracker.find('li:eq('+_self.settings.errorIndex+')').addClass('selected');
					_self.hideFooterMenu();
					_self.parentObj.swipeInstance.slideTo(_self.settings.errorIndex);
					_self.slideEndChangeHandler(_self.settings.errorIndex);
				} else {
					_self.$stepTracker.find('li:eq('+$index+')').addClass('selected');
					_self.hideFooterMenu();
					_self.parentObj.swipeInstance.slideTo($index);
					_self.slideEndChangeHandler($index);
				}
				
			});
		}
	};
	
	/**
	 * Show footer menu
	 * @return {[type]} [description]
	 */
	this.showFooterMenu= function() {
		var _self = this;
		var $footer = _self.$stepView.find('#footer-menu');
		$footer.addClass('showed');
		var $height = $footer.find('.step-tracker').height();
		$footer.css('max-height',$height+'px');
		_self.$arrow.addClass('down-arrow');
	};

	/**
	 * hide footer menu
	 * @return {[type]} [description]
	 */
	this.hideFooterMenu= function() {
		var _self = this;
		var $footer = _self.$stepView.find('#footer-menu');
		$footer.removeClass('showed');
		$footer.css('max-height','0');
		_self.$arrow.removeClass('down-arrow');
	};
	
	/**
	 * Calculate to add the color in the progress bar
	 * @returns width - the width of the page step
	 */
	this._calculateProgressBar= function(){
		var _self = this;
		return ((parseInt(this.parentObj.getActiveIndex()) + 1) / parseInt(this.parentObj.getPageCount()) ) * 100;
	}
	
}

StepPagePanelView = $.extend(StepPagePanelView, PagePanelView);