//
//  PersistenceStore.m
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import "CustomerDetailsDAO.h"
#import "AppDelegate.h"
#import "Child.h"

@implementation CustomerDetailsDAO

+(instancetype) sharedPersistenceStore {
    
    static CustomerDetailsDAO *sharedPersistenceStore;
    
    if (!sharedPersistenceStore) {
        sharedPersistenceStore = [[self alloc] init];
    }
    
    return sharedPersistenceStore;
}

-(AppDelegate*) appDelegate {
    return [[UIApplication sharedApplication] delegate];
}

-(NSEntityDescription*) entityDescription {
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CustomerDetails" inManagedObjectContext:[[self appDelegate]managedObjectContext]];
    
    return entity;
}

#pragma mark - CRUD function

/**
 Checks if a customer exists in the database
 */
- (CustomerDetails *)getCustomerWithCustomerId:(NSString *)customerId {
    
    CustomerDetails *customerDetails;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *byCustomerId = [NSPredicate predicateWithFormat:@"customerId = %@", customerId];
    
    request.entity = [self entityDescription];
    request.predicate = byCustomerId;
    
    NSError *error = nil;
    
    NSArray *customers = [[[self appDelegate] managedObjectContext] executeFetchRequest:request error:&error];
    
    if (customers.count > 0) {
        customerDetails = [customers lastObject];
    }
    
    return customerDetails;
}

/**
 Retrieve the customer JSON Object
 */

-(NSDictionary *)retrieveCutomerWithCustomerId:(NSString*)customerId {
    CustomerDetails *customer = [self getCustomerWithCustomerId:customerId];
    return [self encodeManagedObjectToJSon:customer];
}

/**
 Fetch all customers in the database and returns MO
 */
-(NSArray*) loadAllCustomersMO {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSSortDescriptor *sortBySurname = [[NSSortDescriptor alloc] initWithKey:@"surname" ascending:YES];
    NSSortDescriptor *sortByGivenName = [[NSSortDescriptor alloc] initWithKey:@"givenname" ascending:YES];
    
    request.entity = [self entityDescription];
    request.sortDescriptors = @[sortBySurname, sortByGivenName];
    
    NSError *error = nil;
    
    NSArray *customers = [[[self appDelegate] managedObjectContext] executeFetchRequest:request error:&error];
    
    if (customers.count > 0) {
        return customers;
    }
    
    return nil;
}

/**
 Fetch all customers in the database and returns json
 */

-(NSArray*) loadAllCustomers {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSSortDescriptor *sortBySurname = [[NSSortDescriptor alloc] initWithKey:@"surname" ascending:YES];
    NSSortDescriptor *sortByGivenName = [[NSSortDescriptor alloc] initWithKey:@"givenname" ascending:YES];
    
    request.entity = [self entityDescription];
    request.sortDescriptors = @[sortBySurname, sortByGivenName];
    
    NSError *error = nil;
    
    NSArray *customers = [[[self appDelegate] managedObjectContext] executeFetchRequest:request error:&error];
    
    if (customers.count > 0) {
        
        customers = [self encodeManagedObject:customers];
        
        return customers;
    }
    
    return nil;
}

/**
 Creates customer details
 */

-(CustomerDetails *)createCustomerDetails:(NSDictionary*) customerDetails {
    NSError *error = nil;
    
    CustomerDetails *customer = [self getCustomerWithCustomerId:customerDetails[@"customerId"]];
    
    if (!customer) {
        CustomerDetails *newCustomer = [NSEntityDescription insertNewObjectForEntityForName:@"CustomerDetails" inManagedObjectContext:[[self appDelegate] managedObjectContext]];
        
        [newCustomer setSurname:    customerDetails[CUSTOMER_SURNAME]];
        [newCustomer setGivenname:  customerDetails[CUSTOMER_GIVENNAME]];
        [newCustomer setGender:     customerDetails[CUSTOMER_GENDER]];
        [newCustomer setEmail:      customerDetails[CUSTOMER_EMAIL]];
        [newCustomer setMobile:     customerDetails[CUSTOMER_MOBILE]];
        [newCustomer setCountry:    customerDetails[CUSTOMER_COUNTRY]];
        
        [newCustomer setWorkAddress:@""];
        [newCustomer setHomeAddress:@""];
        [newCustomer setDateOfBirth:@""];
        [newCustomer setAge:nil];
        
        if (customerDetails[CUSTOMER_WORKADDRESS]) {
            [newCustomer setWorkAddress:customerDetails[CUSTOMER_WORKADDRESS]];
        }
        
        if (customerDetails[CUSTOMER_HOMEADDRESS]) {
            [newCustomer setHomeAddress:customerDetails[CUSTOMER_HOMEADDRESS]];
        }
        
        if (customerDetails[CUSTOMER_DATEOFBIRTH]) {
            [newCustomer setDateOfBirth:customerDetails[CUSTOMER_DATEOFBIRTH]];
        }
        
        if (customerDetails[CUSTOMER_AGE]) {
            [customer setAge:[NSNumber numberWithInteger:[customerDetails[CUSTOMER_AGE] integerValue]]];
        }
        
        if (customerDetails[CUSTOMER_ID]) {
            [newCustomer setCustomerId:customerDetails[CUSTOMER_ID]];
        } else {
            [newCustomer setCustomerId:[self randomizeString]];
        }
        
        
        if (customerDetails[CUSTOMER_CHILDREN] && [customerDetails[CUSTOMER_CHILDREN] count] > 0) {
            
            // Create a Child Person
            Child *child = [NSEntityDescription insertNewObjectForEntityForName:@"Child" inManagedObjectContext:[[self appDelegate] managedObjectContext]];
            
            // Create Relationship
            NSMutableSet *children = [newCustomer mutableSetValueForKey:CUSTOMER_CHILDREN];
            
            for (NSDictionary *dict in customerDetails[CUSTOMER_CHILDREN]) {
                
                // Set First and Last Name
                [child setSurname:      dict[CHILD_SURNAME]];
                [child setGivenname:    dict[CHILD_GIVENNAME]];
                [child setGender:       dict[CUSTOMER_GENDER]];
                [child setDateOfBirth:  dict[CUSTOMER_DATEOFBIRTH]];
                
                [children addObject:child];
            }

            [newCustomer setChildren:children];
            
        } else {
            
            // Create Relationship
            NSMutableSet *children = [newCustomer mutableSetValueForKey:CUSTOMER_CHILDREN];
            [newCustomer setChildren:children];
        }
        
        if ([[[self appDelegate] managedObjectContext] save:&error]) {
            return newCustomer;
        }
        
        if (!error) {
            NSLog(@"Creation successful");
        }
        
        return nil;
        
        
    } else {
        NSLog(@"Customer already exists in the database");
        return customer;
    }
    
}

/**
 Update the existing customer details
 */
- (CustomerDetails *)updateCustomerDetails:(NSDictionary *)customerDetails error:(NSError *)error {
    
    NSMutableDictionary *existingCustomerDetails = [NSMutableDictionary dictionaryWithDictionary:[self retrieveCutomerWithCustomerId:customerDetails[CUSTOMER_ID]]];
    
    CustomerDetails *customer = [self getCustomerWithCustomerId:customerDetails[CUSTOMER_ID]];
    
    // validate items
    for (NSString *key in [customerDetails allKeys]) {
        if ([customerDetails objectForKey:key] && ![[customerDetails objectForKey:key] isEqual:[NSNull null]]) {
            [existingCustomerDetails setObject:[customerDetails objectForKey:key] forKey:key];
        }
    }
    
    if (customer) {
        [customer setSurname:       existingCustomerDetails[CUSTOMER_SURNAME]];
        [customer setGivenname:     existingCustomerDetails[CUSTOMER_GIVENNAME]];
        [customer setGender:        existingCustomerDetails[CUSTOMER_GENDER]];
        [customer setEmail:         existingCustomerDetails[CUSTOMER_EMAIL]];
        [customer setMobile:        existingCustomerDetails[CUSTOMER_MOBILE]];
        [customer setCountry:       existingCustomerDetails[CUSTOMER_COUNTRY]];
        [customer setWorkAddress:   existingCustomerDetails[CUSTOMER_WORKADDRESS]];
        [customer setHomeAddress:   existingCustomerDetails[CUSTOMER_HOMEADDRESS]];
        [customer setDateOfBirth:   existingCustomerDetails[CUSTOMER_DATEOFBIRTH]];
        
        [customer setNumberOfChildren:   [NSNumber numberWithInteger:[existingCustomerDetails[CUSTOMER_NOCHILDREN] integerValue]]];
        
        [customer setAge:[NSNumber numberWithInteger:[existingCustomerDetails[CUSTOMER_AGE] integerValue]]];
        
        
        if (existingCustomerDetails[CUSTOMER_CHILDREN] && [existingCustomerDetails[CUSTOMER_CHILDREN] count] > 0) {
            
            
            // Create Relationship
            NSMutableSet *children = [customer mutableSetValueForKey:CUSTOMER_CHILDREN];
            [children removeAllObjects];
            
            for (NSDictionary *childObj in customerDetails[CUSTOMER_CHILDREN]) {
                
                // Create a Child Person
                Child *child = [NSEntityDescription insertNewObjectForEntityForName:@"Child" inManagedObjectContext:[[self appDelegate] managedObjectContext]];
                
                // Set First and Last Name
                [child setSurname:      childObj[CHILD_SURNAME]];
                [child setGivenname:    childObj[CHILD_GIVENNAME]];
                [child setGender:       childObj[CHILD_GENDER]];
                [child setDateOfBirth:  childObj[CHILD_DOB]];
                [child setAge:[NSNumber numberWithInteger:[childObj[CHILD_AGE] integerValue]]];
                
                [children addObject:child];
            }
            
            [customer setChildren:children];
            
        }

        
        if ([[[self appDelegate] managedObjectContext] save:&error]) {
            NSLog(@"Update successful");
            return customer;
        }else {
            NSLog(@"Update FAILED : %@",error);
            return nil;
        }
        
    } else {
        return [self createCustomerDetails:customerDetails];
    }
}

/**
 Remove a customer
 */
-(void) removeCustomerWithId:(NSString*) customerId {
    CustomerDetails *customer  = [self getCustomerWithCustomerId:customerId];
    NSError *error = nil;
    
    if (customer) {
        [[[self appDelegate] managedObjectContext] delete:customer];
        [[[self appDelegate] managedObjectContext] save:&error];
        
        if (!error) {
            NSLog(@"Remove successful");
        }
        
    } else {
        NSLog(@"Cannot perform deletion because customer does not exist");
    }
}

#pragma mark - Encoder 

/**
 To return an array of json
 */
-(NSArray*) encodeManagedObject:(NSArray*) objects {
    NSMutableArray *encodedObjects = [[NSMutableArray alloc] init];
    if (objects.count > 0) {
        for (CustomerDetails *customer in objects) {
            NSArray *keys = [[[self entityDescription] attributesByName] allKeys];
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[customer dictionaryWithValuesForKeys:keys]];
            
            if (customer.children && customer.children.count > 0) {
                
                NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Child" inManagedObjectContext:[[self appDelegate]managedObjectContext]];
                NSArray *childKeys = [[entityDescription attributesByName] allKeys];
                
                NSMutableArray *children = [NSMutableArray new];
                for (Child *currentChild in customer.children) {
                    NSDictionary *child = [currentChild dictionaryWithValuesForKeys:childKeys];
                    [children addObject:child];
                }
                [dict setObject:children forKey:CUSTOMER_CHILDREN];
            }
            
            [encodedObjects addObject:dict];
        }
    }
    return encodedObjects;
}

/**
 To return json
 */
-(NSDictionary*) encodeManagedObjectToJSon:(CustomerDetails*) objects {
    
//    NSArray *keys = [[[self entityDescription] attributesByName] allKeys];
//    NSDictionary *encodedObject = [objects dictionaryWithValuesForKeys:keys];
//
//    return encodedObject;
    
    NSArray *keys = [[[self entityDescription] attributesByName] allKeys];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[objects dictionaryWithValuesForKeys:keys]];
    
    if (objects.children && objects.children.count > 0) {
        
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Child" inManagedObjectContext:[[self appDelegate]managedObjectContext]];
        NSArray *childKeys = [[entityDescription attributesByName] allKeys];
        
        NSMutableArray *children = [NSMutableArray new];
        for (Child *currentChild in objects.children) {
            NSDictionary *child = [currentChild dictionaryWithValuesForKeys:childKeys];
            [children addObject:child];
        }
        [dict setObject:children forKey:CUSTOMER_CHILDREN];
    }
    
    return dict;

}

#pragma mark - Initial Data
/**
 To initialize database
 */
-(void) createInitialCustomerList {
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"InitialCustomerList" ofType:@"plist"];
    
    NSArray *customerList = [[NSMutableArray alloc] initWithContentsOfFile:plistPath];
    
    for (NSDictionary *customer in customerList) {
        [self createCustomerDetails:customer];
    }
}



#pragma mark - DateFormatter
-(NSDate*) dateFormatter:(NSString*) dateOfBirth {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *formattedDateOfBirth = [dateFormat dateFromString:[dateOfBirth substringToIndex:9]];
    
    return formattedDateOfBirth;
}


#pragma mark - Randomize customerid hihi

/**
 Randomize string value for customer id value
 */

- (NSString *)randomizeString {
    NSString *strings = @"abcdefghijklmnopqrstuvwxyz0123456789";
    NSMutableString *input = [strings mutableCopy];
    NSMutableString *output = [NSMutableString string];
    
    NSUInteger len = input.length;
    
    for (NSUInteger i = 0; i < len; i++) {
        NSInteger index = arc4random_uniform((unsigned int)input.length);
        [output appendFormat:@"%C", [input characterAtIndex:index]];
        [input replaceCharactersInRange:NSMakeRange(index, 1) withString:@""];
    }
    
    return output;
}

@end
