//
//  MainMenuViewController.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 10/30/15.
//
//

#import <UIKit/UIKit.h>

@class MainMenuViewController;

@protocol MainMenuViewControllerDelegate <NSObject>
@optional
- (void)mainMenuViewController:(MainMenuViewController *)sender userDidLogout:(id)params;

@end

@interface MainMenuViewController : UIViewController
@property (nonatomic, weak) id <MainMenuViewControllerDelegate> delegate;
@property (nonatomic, weak) IBOutlet UILabel *loggedUser;
- (IBAction)logOutPressed:(UIButton *)sender;

@end
