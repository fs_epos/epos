/**
*	Dashboard, Customer List Data Provider
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerListDataProvider = function() {
		this.options = $.extend({}, CustomerListDataProvider.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.CustomerListDataProvider';
	}
	
	//Define default attributes
	CustomerListDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	CustomerListDataProvider.prototype = {
		constructor : CustomerListDataProvider,
		getCustomerList: function(queryObject, successCallback, failureCallback) {
			//CORDOVA CALL
			cordova.exec(
				function(data){
					successCallback(data);
				},
				function(error){
					failureCallback(error);
				},
				this.PLUGIN_NAME,
				'getCustomerList',
				(queryObject ? queryObject : [])
			);
		}
		
	}
	//Register the object inside ePOS
	ePos.widget.CustomerListDataProvider = CustomerListDataProvider;
}(ePos, $));

