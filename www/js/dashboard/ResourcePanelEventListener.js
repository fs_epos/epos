/**
*	Dashboard, Resource Panel Event Listener
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var ResourcePanelEventListener = function() {
		this.options = $.extend({}, ResourcePanelEventListener.DEFAULTS);		
	}
	//Define default attributes
	ResourcePanelEventListener.DEFAULTS = {
		initializeData: true
	}
	
	ResourcePanelEventListener.prototype = {
		constructor : ResourcePanelEventListener,
		onOpenResource: function(information) {
			//phonegap
			console.log('onOpenResource data: ' + information);
		}			
	}	
	//Register the object inside ePOS
	ePos.widget.ResourcePanelEventListener = ResourcePanelEventListener;
}(ePos, $));

