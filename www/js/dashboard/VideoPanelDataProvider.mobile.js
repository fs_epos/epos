/**
*	Dashboard, Video Panel Data Provider Mobile
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var VideoPanelDataProvider = function() {
		this.options = $.extend({}, VideoPanelDataProvider.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.VideoPanelView';
	}
	
	//Define default attributes
	VideoPanelDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	VideoPanelDataProvider.prototype = {
		constructor : VideoPanelDataProvider,
		retrieveVideo: function(queryObject, successCallback, failureCallback){
			//CORDOVA CALL
			cordova.exec(
				function(data){
					successCallback(data);
				},
				function(error){
					failureCallback(error);
				},
				this.PLUGIN_NAME,
				'retrieveVideo',
				(queryObject ? queryObject : [])
			);
		}	
	}
	//Register the object inside ePOS
	ePos.widget.VideoPanelDataProvider = VideoPanelDataProvider;
}(ePos, $));

