//
//  GTKYCDVViewController.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import "GTKYCDVViewController.h"
#import "GTKYCDVPlugin.h"

@interface GTKYCDVViewController ()
@property (nonatomic, retain) CustomerDetails *customer;
@end

@implementation GTKYCDVViewController

- (id)initWithCustomer:(CustomerDetails *)customer
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
        _customer = customer;
        self.startPage = @"GTKY.html";
    }
    return self;
}

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    return [self initWithCustomer:nil];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Disable web view bouncing
    self.webView.scrollView.bounces = NO;
    
    GTKYCDVPlugin *plugin = [[GTKYCDVPlugin alloc] initWithCustomer:_customer];
    [self registerPlugin:plugin withPluginName:[GTKYCDVPlugin pluginName]];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:self.navigationController.navigationBar.frame];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setTextAlignment:NSTextAlignmentCenter];
    [titleLabelView setFont:[UIFont boldSystemFontOfSize:22]];
    titleLabelView.text = @"Getting to know you";
    
    self.navigationController.navigationBar.topItem.titleView = titleLabelView;

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [button setImage:[UIImage imageNamed:@"sideMenu"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(toggleMenu) forControlEvents:UIControlEventAllTouchEvents];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self.navigationController action:@selector(popViewControllerAnimated:)];
    
    [rightItem setTintColor:[UIColor whiteColor]];
    [self.navigationItem setRightBarButtonItem:rightItem];
    [self.navigationItem setLeftBarButtonItem:item];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.0f green:183.0f/255.0f blue:242.0f/255.0f alpha:1]];
}


- (void)toggleMenu {
    
    if (!self.navigationItem.leftBarButtonItem.enabled) {
        return;
    }
    
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success) {
        if (success) {
            [self.navigationItem.leftBarButtonItem setEnabled:YES];
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
