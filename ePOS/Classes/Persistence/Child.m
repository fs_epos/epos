//
//  Child.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/12/15.
//
//

#import "Child.h"

@implementation Child

@dynamic dateOfBirth;
@dynamic gender;
@dynamic givenname;
@dynamic surname;
@dynamic age;

@end
