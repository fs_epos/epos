/**
 * ButtonRange, provides a range of selection with plus minus button
 * © Accenture ePOS 2015
 * @author Neilbrian Bernardio
 */

// define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $){
	'use strict';
	
	// The default constructor
	function ButtonRange(widgetReference, options, callback){
		// used the current element instace if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $)? widgetReference: $('#' + widgetReference);
		
		this.options = $.extend({}, ButtonRange.DEFAULTS, options);
		
		this.callback = callback;
		
		// set to default
		this.isMax = false;
		this.isMin = false;
		
		this._buildInitialDOM();
		this._bindEvents();
	}
	
	// Define defaults
	ButtonRange.DEFAULTS = {
			min: 0,
			max: 10,
			step: 1,
			value: 0,
			className: '',
			unit : '',
			errMessage: '',				// set to empty string if you don't want to show any error message
			enableDisableButton: true,
			disabledTextbox: false,
			decimalPlaces: 0,			// set to zero if decimal place is not desired
			minusButtonText: '-',
			plusButtonText: '+'
		};
	
	// define class variables and methods
	ButtonRange.prototype = {
		constructor: ButtonRange,
		options: null,
		$widget: null,
		callback: null,
		
		// validator for button event
		isMax: null,
		isMin: null,
		
		// the html output for this widget
		_template: '<div class="button-range-input row">'
				+ '	<button class="minus-button pos-left"></button>'
				+ '	<input type="text" name="buttonrange" class="text-button-range pos-left">'
				+ '	<button class="plus-button pos-left"></button>'
				+ '</div>'
				+ '<div class="errmsg-buttonrange"></div>',

		/**
		* Inserts the template to the DOM and defines the necessary elements to be used
		*/
		_buildInitialDOM: function(){
			this.$widget.append(this._template);
			
			this.$minusButton = $('.minus-button', this.$widget).text(this.options.minusButtonText);
			this.$plusButton = $('.plus-button', this.$widget).text(this.options.plusButtonText);
			this.$inputText = $('.text-button-range', this.$widget);
			this.$errorMsg = $('.errmsg-buttonrange', this.$widget);
			
			this.$errorMsg.hide();
			
			this._initInputText();
		},
		
		/**
		* Initialize default state of the text input
		*/
		_initInputText: function(){
			if(this.options.disabledTextbox){
				this.$inputText.prop('disabled', true).addClass('transparent');
			}
			
			this.setValue(this.options.value);
		},
		
		/**
		* Define the events and listeners for the buttons and input
		*/
		_bindEvents: function(){
			var _self = this;
			
			this.$minusButton.on('click', function(){
				// if already minimum, don't do anything
				if(_self.isMin) return;
				
				var newVal = (_self.getValue() * 1) - _self.options.step;
				_self.setValue(newVal);
				
				_self.isMax = false;
				_self._updateButtonStatus();
			});
			
			this.$plusButton.on('click', function(){
				// if already maximum, don't do anything
				if(_self.isMax) return;
				
				var newVal = (_self.getValue() * 1) + _self.options.step;
				_self.setValue(newVal);
				
				_self.isMin = false;
				_self._updateButtonStatus();
			});
			
			this.$inputText.on('change', function(){
				var newVal = _self.getValue();
				_self.setValue(newVal);
				
				_self._updateButtonStatus();
			});
		},
		
		/**
		* Checks the button status, enable and disable them appropriately
		*/
		_updateButtonStatus: function(){
			if(this.options.enableDisableButton){
				if(this.isMin){
					this.$minusButton.addClass('disable');
				}else{
					this.$minusButton.removeClass('disable');
				}
				
				if(this.isMax){
					this.$plusButton.addClass('disable');
				}else{
					this.$plusButton.removeClass('disable');
				}
			}
		},
		
		/**
		* Shows the error message if user input is incorrect
		*/
		_showErrorStatus: function(){
			if(this.options.errMessage.length > 0){
				this.$errorMsg.text(this.options.errMessage);
				this.$errorMsg.show();
			}
			
			this.$inputText.addClass('has-error');
			
			// set to true in order to disable the buttons
			this.isMin = true;
			this.isMax = true;
		},
		
		/**
		* Removes error status if correct input condition is met
		*/
		_removeErrorStatus: function(){
			if(this.options.errMessage.length > 0){
				this.$errorMsg.hide();
			}
			
			this.$inputText.removeClass('has-error');
			
			// set to false to enable buttons
			this.isMin = false;
			this.isMax = false;
		},
		
		/**
		* Run the callback defined by the user
		*/
		_invokeCallback: function(){
			if(typeof this.callback == 'function'){
				this.callback();
			}
		},
		
		/**
		* Sets the value accordingly upon input or button press
		* @param val (int/float) the value set by the user
		*/
		setValue: function(val){
			// check if val is numeric
			if($.isNumeric(val) == false){
				this._showErrorStatus();
				return;	// show error but do not let it proceed
			} else {
				this._removeErrorStatus();
			}
			
			// check if default val doesn't exceed both min and max, if so, used the min and max value instead
			var value = val < this.options.min? this.options.min : (( val > this.options.max)? this.options.max :  val);
			var unit = this.options.unit.length > 0? ' '+this.options.unit: '';
			
			// set the decimal points
			if(value){
				value = parseFloat(value).toFixed(this.options.decimalPlaces);
			}
			
			this.$inputText.val(value + unit);
			
			// if new value is greater than or equal to max value, let the app know that it reach the maximum
			if(value >= this.options.max){
				this.isMax = true;
			}
			
			// if new value is lesser than or equal to min value, let the app know that it reach the minimum
			if(value <= this.options.min){
				this.isMin = true;
			}
			
			this._invokeCallback();
		},
		
		/**
		* Returns the value of the input text
		* @return string, the input value
		*/
		getValue: function(){
			return this.$inputText.val().toString().replace(this.options.unit, '').trim();
		}
	};
	
	// register the ButtonRange object on ePos
	ePos.widget.ButtonRange = ButtonRange;
}(ePos, $));