/**
*	Dashboard, Resource Panel Data Provider
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var ResourcePanelDataProvider = function() {
		this.options = $.extend({}, ResourcePanelDataProvider.DEFAULTS);
		this.defaults = {
			resourceId : "1001"
		};
	}
	
	//Define default attributes
	ResourcePanelDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	ResourcePanelDataProvider.prototype = {
		constructor : ResourcePanelDataProvider,
		retrieveResource: function(queryObject, successCallback, failureCallback) {
			if(typeof successCallback == 'function'){
    			console.log("retrieveResource execution")
    			successCallback(this.defaults.resourceId);
    		}
		}
	}
	//Register the object inside ePOS
	ePos.widget.ResourcePanelDataProvider = ResourcePanelDataProvider;
}(ePos, $));

