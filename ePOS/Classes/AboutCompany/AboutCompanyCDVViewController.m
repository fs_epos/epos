//
//  AboutCompanyCDVViewController.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import "AboutCompanyCDVViewController.h"

#import "VideoPanelCDVPlugin.h"
#import "ResourcePanelCDVPlugin.h"
#import "ResourceViewController.h"

@interface AboutCompanyCDVViewController () <VideoPanelCDVPluginDelegate, ResourcePanelCDVPluginDelegate>

@end

@implementation AboutCompanyCDVViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
        
        self.startPage = @"AboutUs.html";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [button setImage:[UIImage imageNamed:@"sideMenu"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(toggleMenu) forControlEvents:UIControlEventAllTouchEvents];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    [self.navigationItem setLeftBarButtonItem:item];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.0f green:183.0f/255.0f blue:242.0f/255.0f alpha:1]];
    
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:self.navigationController.navigationBar.frame];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setTextAlignment:NSTextAlignmentCenter];
    [titleLabelView setFont:[UIFont boldSystemFontOfSize:22]];
    titleLabelView.text = NSLocalizedString(@"About Us", @"");
    
    self.navigationController.navigationBar.topItem.titleView = titleLabelView;
    
    // Disable web view bouncing
    self.webView.scrollView.bounces = NO;
    
    VideoPanelCDVPlugin *videoPanelPlugin = [[VideoPanelCDVPlugin alloc] init];
    videoPanelPlugin.delegate = self;
    [self registerPlugin:videoPanelPlugin withPluginName:[VideoPanelCDVPlugin pluginName]];
    
    ResourcePanelCDVPlugin *resourcePlugin = [[ResourcePanelCDVPlugin alloc] init];
    resourcePlugin.delegate = self;
    [self registerPlugin:resourcePlugin withPluginName:[ResourcePanelCDVPlugin pluginName]];
}

- (void)toggleMenu {
    
    if (!self.navigationItem.leftBarButtonItem.enabled) {
        return;
    }
    
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success) {
        if (success) {
            [self.navigationItem.leftBarButtonItem setEnabled:YES];
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - VideoPanelCDVPluginDelegate

- (void)videoPanelCDVPluginDelegate:(VideoPanelCDVPlugin *)plugin onOpenVideo:(NSString *)params {
    
    ResourceViewController *viewController = [[ResourceViewController alloc] initWithResourceType:params];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
}

#pragma mark - ResourcePanelCDVPluginDelegate

- (void)resourcePanelCDVPlugin:(ResourcePanelCDVPlugin *)plugin onOpenResource:(NSString *)params {
    
    ResourceViewController *viewController = [[ResourceViewController alloc] initWithResourceType:params];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
    
}


@end
