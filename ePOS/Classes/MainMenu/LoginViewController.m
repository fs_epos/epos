//
//  ViewController.m
//  FS_Asset
//
//  Created by Apuli, Alexis E. on 10/20/15.
//  Copyright (c) 2015 Accenture_FS. All rights reserved.
//

#import "LoginViewController.h"

#define USERNAME_PLACEHOLDER @"Username"
#define PASSWORD_PLACEHOLDER @"Password"

@interface LoginViewController ()
{
    //used for keyboard visibility check
    BOOL 			keyboardVisible;
    
    IBOutlet UITextField *usernameTextField;
    IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UILabel *invalidPassword;
    
}

@property (nonatomic, strong) IBOutlet UIScrollView *scrollview;
- (IBAction)loginDidTap:(UIButton *)sender;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configurePasswordPlaceholder];
    [self configureUsernamePlaceholder];
    
    //set currentUserName
    if ([[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERNAME_KEY]) {
        usernameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERNAME_KEY];
//        passwordTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERNAME_KEY];
    }
    
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setHidden:YES];
    
    // Register for the events
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardWillShow:)
                                                 name: UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardWillHide:)
                                                 name: UIKeyboardWillHideNotification object:nil];
    
    //Initially the keyboard is hidden
    keyboardVisible = NO;	

    [invalidPassword setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Keyboard

-(void) keyboardWillShow: (NSNotification *)notif
{
    // If keyboard is visible, return
    if (keyboardVisible)
    {
        return;
    }
    
    //animate
    [UIView animateWithDuration:0.3 animations:^{
        // Resize the scroll view to make room for the keyboard
        CGRect viewFrame = _scrollview.frame;
        viewFrame.size.height += KEYBOARD_OFFSET;
        viewFrame.origin.y -= KEYBOARD_OFFSET;
        _scrollview.frame = viewFrame;
    }];
    
    
    // Keyboard is now visible
    keyboardVisible = YES;
}

- (void)keyboardWillHide:(NSNotification *)notif
{
    // Is the keyboard already shown
    if (!keyboardVisible)
    {
        return;
    }
    
    // Reset the frame scroll view to its original value
    _scrollview.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    
    // Keyboard is no longer visible
    keyboardVisible = NO;
}

#pragma mark - Text Field Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == usernameTextField)
    {
        [passwordTextField becomeFirstResponder];
    } else if (textField == passwordTextField)
    {
        [passwordTextField resignFirstResponder];
        [self loginBtnTap];
    }
    [invalidPassword setHidden:YES];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.placeholder = nil;
    [invalidPassword setHidden:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == usernameTextField)
    {
        [self configureUsernamePlaceholder];
    } else if (textField == passwordTextField)
    {
        [self configurePasswordPlaceholder];
    }
}

#pragma mark - Local Methods

- (void)loginBtnTap{

    if ([usernameTextField.text isEqualToString:passwordTextField.text] && ![usernameTextField.text isEqualToString:@""] && ![passwordTextField.text isEqualToString:@""]) {
        
        if (![[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERNAME_KEY] isEqualToString:usernameTextField.text]) {
            [[NSUserDefaults standardUserDefaults] setObject:usernameTextField.text forKey:CURRENT_USERNAME_KEY];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        [self.delegate loginViewController:self loginDidSucceed:nil];
        
    } else {
        [invalidPassword setHidden:NO];
    }
}

- (void)configureUsernamePlaceholder
{
    usernameTextField.placeholder = USERNAME_PLACEHOLDER;
    
    if ([usernameTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:0.0f green:183.0f/255.0f blue:242.0f/255.0f alpha:1];
        UIFont *font = [UIFont fontWithName:FONT_NAME size:FONT_SIZE];
        usernameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:usernameTextField.placeholder attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName : font}];
    }
}


- (void)configurePasswordPlaceholder
{
    passwordTextField.placeholder = PASSWORD_PLACEHOLDER;
    
    if ([passwordTextField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:0.0f green:183.0f/255.0f blue:242.0f/255.0f alpha:1];
        UIFont *font = [UIFont fontWithName:FONT_NAME size:FONT_SIZE];
        passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:passwordTextField.placeholder attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName : font}];
    }
}



- (IBAction)loginDidTap:(UIButton *)sender {
    [self loginBtnTap];
}

@end
