/**
*	GTKY, GTKY Data Provider Mobile
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var GTKYDataProvider = function() {
		this.options = $.extend({}, GTKYDataProvider.DEFAULTS);
		this.PLUGIN_NAME = 'ePos.widget.GTKYPanelView';
	}
	
	//Define default attributes
	GTKYDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	GTKYDataProvider.prototype = {
		constructor : GTKYDataProvider,
		retrieveGeneralInfo: function(queryObject, successCallback, failureCallback){
			//CORDOVA CALL
			cordova.exec(
				function(data){
					successCallback(data);
				},
				function(error){
					failureCallback(error);
				},
				this.PLUGIN_NAME,
				'retrieveGeneralInfo',
				(queryObject ? queryObject : [])
			);
		},
		
		getFinancialInfo: function(queryObject, successCallback, failureCallback){
			if(typeof successCallback == 'function'){
				//CORDOVA CALL
				cordova.exec(
					function(data){
						successCallback(data);
					},
					function(error){
						failureCallback(error);
					},
					this.PLUGIN_NAME,
					'getFinancialInfo',
					(queryObject ? queryObject : [])
				);
    		}
		}	
	}
	//Register the object inside ePOS
	ePos.widget.GTKYDataProvider = GTKYDataProvider;
}(ePos, $));