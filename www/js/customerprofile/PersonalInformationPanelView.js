/**
*	Personal Information, Personal Information form for the Customer or Prospect
*	Copyright Accenture Inc., ePOS 2015
*	@author Neil Patrick Conocono
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';
	function PersonalInformationPanelView(widgetReference, options, callback) {
		//use the current element instance if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.dataprovider = new ePos.widget.CustomerProfileDataProvider();
		this.eventlistener = new ePos.widget.CustomerProfileEventListener();
		this.validation = new ePos.widget.ValidationUtility();
		this.options = $.extend({}, PersonalInformationPanelView.DEFAULTS, options);
		this._personalInformation = [];
		this.callback = callback;
		this._buildInitialDOM();
	}
	
	//Define default attributes
	PersonalInformationPanelView.DEFAULTS = {
		initializeData: true
	}
	
	PersonalInformationPanelView.prototype = {
		constructor : PersonalInformationPanelView,
		options: null,
		$widget: null,
		callback: null,

		//html template
		_template: 	'<div class="personal-information-form">'
				+'		<div class="personal-information-view">'
				+'			<div class="client-surname clearfix"> <span class="client-surname-label text-label"> </span> '
				+'			<span class="client-surname-value text-value"> </span> </div>'
				+'			<div class="client-givenname clearfix"> <span class="client-givenname-label text-label"> </span> '
				+'			<span class="client-givenname-value text-value"> </span> </div>'
				+'			<div class="client-sex clearfix"> <span class="client-sex-label text-label"> </span> '
				+'			<span class="client-sex-value text-value"> </span> </div>'
				+'			<div class="client-dob clearfix"> <span class="client-dob-label text-label"> </span> '
				+'			<span class="client-dob-value text-value"> </span> </div>'
				+'			<div class="client-age clearfix"> <span class="client-age-label text-label"> </span> '
				+'			<span class="client-age-value text-value"> </span></div>'
				+'		</div>'
				+'		<div class="personal-information-edit">'
				+'			<div class="client-surname clearfix"> <span class="client-surname-label text-label"> </span> '
				+'			<span class="client-surname-value text-value"> <input data-req="1" data-name="Surname" name="surname" type="text" value="" /> </span> </div>'
				+'			<div class="client-givenname clearfix"> <span class="client-givenname-label text-label"> </span> '
				+'			<span class="client-givenname-value text-value"> <input data-req="1" data-name="Given Name" name="givenname" type="text" value="" /> </span> </div>'
				+'			<div class="client-sex clearfix"> <span class="client-sex-label text-label"> </span> '
				+'			<span class="client-sex-value text-value"> <input data-req="1" data-name="Sex" type="radio" id="gender-male" name="sex" value="MALE"> <label for="gender-male">Male</label> <input id="gender-female" data-req="1" data-name="Sex" type="radio" name="sex" value="FEMALE"> <label for="gender-female">Female</label> </span> </div>'
				+'			<div class="client-dob clearfix"> <span class="client-dob-label text-label"> </span> '
				+'			<span class="client-dob-value text-value"> <input data-req="1" data-name="Date of Birth" name="Date of Birth" type="date" value="" /> </span> </div>'
				+'			<div class="client-age clearfix"> <span class="client-age-label text-label"> </span> '
				+'			<span class="client-age-value text-value"> <input data-req="0" name="age" data-name="Age" type="number" value="" /> </span> </div>'
				+'		</div>'
				+'		<div class="personal-information-buttons">'
				+'			<span class="button-span button-edit"> <button id = "button-edit-personal-information" class="button positive" /></span>'
				+'			<span class="button-span button-save"> <button id = "button-save-personal-information" class="button positive" /></span>'
				+'			<span class="button-span button-cancel"> <button id = "button-cancel-personal-information" class="button positive" /></span>'
				+'		</div>'
				+'	</div>',

		_buildInitialDOM: function() {
			this.$widget.append(this._template);
			this._initializeLabels();
			this._initializeEvents();
			if(this.options.initializeData) 
				this.loadData();
		},
		
		loadData: function() {
			$('.personal-information-view', this.$widget).show();
			$('.personal-information-buttons .button-edit .button', this.$widget).show();
			$('.personal-information-edit', this.$widget).hide();
			$('.personal-information-buttons .button-save .button', this.$widget).hide();
			$('.personal-information-buttons .button-cancel .button', this.$widget).hide();
			this._loadPersonalInfo();
		},

		capitalizeString: function(string) {
        	return string.charAt(0).toUpperCase() + string.slice(1);
    	},
		
		_loadPersonalInfo: function(){
	    	var _self = this;
	    	this.dataprovider.retrievePersonalInfo(null,
				function(personalinfo){
					_self._personalInformation = personalinfo;
					_self._loadPersonalInfoView(personalinfo);
					_self._loadPersonalInfoEdit(personalinfo);
		    	},
		    	function(){
		    		console.log('_loadPersonalInfo: something went wrong');
		    	}
	    	);
   		},

		_initializeLabels: function() {
			// Labels
			$('.client-surname-label', this.$widget).text('Surname: ');
			$('.client-givenname-label', this.$widget).text('Given Name: ');
			$('.client-sex-label', this.$widget).text('Sex: ');
			$('.client-dob-label', this.$widget).text('Birthdate: ');
			$('.client-age-label', this.$widget).text('Age: ');
			$('.button-edit .button', this.$widget).text('EDIT');
			$('.button-save .button', this.$widget).text('SAVE');
			$('.button-cancel .button', this.$widget).text('CANCEL');

			//Form Elements
			this.$surname = $('.personal-information-edit .client-surname-value input', this.$widget);
   			this.$givenName = $('.personal-information-edit .client-givenname-value input', this.$widget);
   			this.$gender = $('.personal-information-edit .client-sex-value input:radio[name="sex"]', this.$widget);
   			this.$dateOfBirth = $('.personal-information-edit .client-dob-value input', this.$widget);
   			this.$age = $('.personal-information-edit .client-age-value input', this.$widget);
		}, 
		
		_initializeEvents: function() {
			var _self = this;
			$('.personal-information-buttons .button-edit .button', this.$widget).on('click', function(evt) {
				_self._transferToEditMode();
			});

			$('.personal-information-buttons .button-cancel .button', this.$widget).on('click', function(evt) {
				_self._transferToViewMode();
			});

			$('.personal-information-buttons .button-save .button', this.$widget).on('click', function(evt) {
				_self._savePersonalInfo();
			});
			
			this.$dateOfBirth.on('change', function(evt) {
				var computedAge = _self._computeAge(_self.$dateOfBirth.val());
				_self.$age.val(computedAge);
			});
		},
		
		_computeAge: function(dateOfBirth) {
	    	//Compute age from dateOfBirth
	    	var a = moment(dateOfBirth);
			var b = moment(new Date());
			var computedAge = b.diff(a, 'years');
			
			return computedAge;
		},

		_loadPersonalInfoView: function(data) {
   			$('.personal-information-view .client-surname-value', this.$widget).html(data.surname);
   			$('.personal-information-view .client-givenname-value', this.$widget).html(data.givenname);
   			$('.personal-information-view .client-sex-value', this.$widget).html(data.gender);
   			if(data.dateOfBirth)
   				$('.personal-information-view .client-dob-value', this.$widget).html(moment(data.dateOfBirth).format('LL'));
   			if(data.age && data.age !='');
   				$('.personal-information-view .client-age-value', this.$widget).html(data.age);
   		},

   		_loadPersonalInfoEdit: function(data) {
   			this.$surname.val(data.surname);
   			this.$givenName.val(data.givenname);
   			$('.personal-information-edit .client-sex-value input:radio[name="sex"][value='+data.gender+']').prop('checked', true);
   			this.$dateOfBirth.val(moment(data.dateOfBirth).format('YYYY-MM-DD'));
   			this.$age.val(data.age);
   		},
		
		_transferToViewMode: function() {
			$('.personal-information-edit', this.$widget).hide();
			$('.personal-information-buttons .button-edit .button', this.$widget).show();
			$('.personal-information-view', this.$widget).show();
			$('.personal-information-buttons .button-save .button', this.$widget).hide();
			$('.personal-information-buttons .button-cancel .button', this.$widget).hide();
		},

		_transferToEditMode: function() {
			$('.personal-information-edit', this.$widget).show();
			$('.personal-information-buttons .button-edit .button', this.$widget).hide();
			$('.personal-information-view', this.$widget).hide();
			$('.personal-information-buttons .button-save .button', this.$widget).show();
			$('.personal-information-buttons .button-cancel .button', this.$widget).show();
		},

		// transfer to EventListener
	    _savePersonalInfo: function(){
	    	var validationResult = this.validation.validateInput(this, $('input', this.$widget));
	    	var validateClientBirthdate = this.validation.validateBirthdate(this, this.$dateOfBirth, this.$age);
	    	
	    	if(validationResult && validateClientBirthdate){
	    		$('.personal-information-form', this.$widget).find('.error').parent().find('input').removeClass('error-field');
	    		var newPersonalInfoData = {};
    				newPersonalInfoData = {
    				'customerId': this._personalInformation.customerId,
    				'surname': this.capitalizeString(this.$surname.val().trim()),
    				'givenname': this.capitalizeString(this.$givenName.val().trim()),
    				'gender': $('input[name="sex"]:checked').val(),
    				'dateOfBirth': moment(this.$dateOfBirth.val()).format(),
    				'age': this.$age.val()
    			};
    			this.eventlistener.onSubmitPersonalForm(newPersonalInfoData);

    			this._loadPersonalInfoView(newPersonalInfoData);
				this._loadPersonalInfoEdit(newPersonalInfoData);
				this._transferToViewMode();
	    	}else{
	    		console.log('_savePersonalInfo: failed validation');
	    	}
	    }
	}
	//Register the object inside ePOS
	ePos.widget.PersonalInformationPanelView = PersonalInformationPanelView;
}(ePos, $));
