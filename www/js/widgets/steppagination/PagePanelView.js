/**
* PagePanelView Implementation
*/
var PagePanelView = function(){

	/**
	 * @constructs
	 */
	this.init= function(widgetReference, options){
		this.defaults = {
			activeIndex: 0,
			totalPages: 0,
			errorClass: 'field-error',
			errorIndex: null
		}
		//this._super(widgetReference, $.extend({}, this.defaults, options));
		this.settings = $.extend({}, this.defaults, options);
		this.$widget = $(widgetReference);
		this._buildInitialDOM();
		this.slideEndChangeHandler(this.settings.activeIndex);
		
		//tds.util.Log.t(this.TAG, '['+this.uuid+'] > Initialized');
	};
	
	this.TAG= "clientprofile.common.util.ui.steppagination.PagePanelView",
	
	this._template = 	'<div class="page-view-slide row">'
					+'<div class="page-prev swipe-button"><span></span></div>'
						+'<div class="page-container-slide swipe-panel-wrapper swiper-container">'
						+'	<ul class="swipe-panel swiper-wrapper"></ul>'
						+'</div>'
					+'<div class="page-next swipe-button"><span></span></div>'
				+'</div>',
	
	 /**
	 * Builds all HTML elements to the DOM elements needed to display
	 * @private
	 */
	this._buildInitialDOM= function() {
		var _self = this;
		this.$widget.append(this._template);
		
		this.$pageContainer = $('.page-container-slide ul', this.$widget);
		this.$pageNext = $('.page-next', this.$widget);
		this.$pagePrev = $('.page-prev', this.$widget);
		this.$swipeButton = $('.swipe-button', this.$widget);
		this.$pageUl = $('.page-container-slide > ul',this.$widget);
		this.$pageAnimate = $('.page-container-slide > ul:not(:animated)',this.$widget);
		this.get$ActivePage = 0;
		//FastClick.attach(document.body);
		this.swipeInstance = new Swiper ('.swiper-container', {
			mode:'horizontal',
	        loop: false,
	        noSwiping: true,
	        noSwipingClass: 'swiper-no-swiping',
	        centeredSlides: true,
			simulateTouch: true, // In the desktop the user cannot use the mouse to swipe
			shortSwipes: false, 
			longSwipesMs: 0,
			longSwipesRatio: 0.1,
        	onSlideChangeEnd: function(swiper) {        		
        		_self.slideEndChangeHandler(swiper.activeIndex);
	        },
	        onLongNextSwipeRatioReached: this._onSliderMoveHandler.bind(this)
		 });
		
		var action = ('ontouchstart' in window) ? "tap" : "click";
		//when user clicks the next       
		this.$pageNext.stop(true,true).on(action, function(evt){
			var action = _self.getAction(this);
			_self.doAction(action);
		});
		
		//when user clicks the previous       
		this.$pagePrev.stop(true,true).on(action, function(evt){
			var action = _self.getAction(this);
			_self.doAction(action);
		});
		
	};
	
	/**
	 * Handler called when user touch and move finger over Swiper and move it.
	 * 
	 * @param {Object} swiper the slider instance
	 * @param {Object} event the event
	 * 
	 * @private
	 */
	this._onSliderMoveHandler= function(swiper, event) {
		var _self = this;
		return _self.checkError(swiper.activeIndex);
	};
	
	this.checkError= function(index) {
		var slideHtml = this.swipeInstance.slides[index];
			
		if( $('.'+this.settings.errorClass, $(slideHtml)).length && this.swipeInstance.swipeDirection === 'next' && index >= 0 ) {
//			this.$stepTracker.find('li').removeClass('selected');
//			this.$stepTracker.find('li:eq('+index+')').addClass('selected');
			return false;
		} else {
			$('.info-msg', $(slideHtml)).text('');
			return true;
		}
	};
	
	this.getAction= function(el) {
		var action = 'right';
		if ($(el).hasClass('page-prev')) {
			action = 'left';
		}

		return action;
	};
	
	this.doAction= function(action) {
		switch (action) {
			case 'left':
				var $prevIndex = this.swipeInstance.activeIndex - 1;
//				this.$stepTracker.find('li').removeClass('selected');
//				this.$stepTracker.find('li:eq('+$prevIndex+')').addClass('selected');
				this.swipeInstance.slidePrev(false);
				this.slideEndChangeHandler(this.swipeInstance.activeIndex);
				break;
			case 'right':
				var slideHtml = this.swipeInstance.slides[this.swipeInstance.activeIndex];
				if( $('.'+this.settings.errorClass, $(slideHtml)).length < 1 ) {
					var $nextIndex = this.swipeInstance.activeIndex + 1;
//					this.$stepTracker.find('li').removeClass('selected');
//					this.$stepTracker.find('li:eq('+$nextIndex+')').addClass('selected');
					this.swipeInstance.slideNext(false);
					this.slideEndChangeHandler(this.swipeInstance.activeIndex);
				} 
				break;
			default:
		}
	};
	
	/**
	 * handler when slide change is ended
	 * @param  {[type]} index [description]
	 * @return {[type]}       [description]
	 */
	this.slideEndChangeHandler= function(index) {
		var i = index;
		this.setCurrStep(i);
		// hide arrow button
		if (i === 0) {
			this.$swipeButton.css('visibility','visible');
			this.$pagePrev.css('visibility','hidden');
		} else if (i === (this.settings.totalPages - 1)) {
			this.$swipeButton.css('visibility','visible');
			this.$pageNext.css('visibility','hidden');
		} else {
			this.$swipeButton.css('visibility','visible');
		}
		var $activeIndex = this.swipeInstance.activeIndex;
	};
	
	/**
	 * Set current step
	 * @param {[type]} index [description]
	 */
	this.setCurrStep= function(index) {
		this.settings.activeIndex = index;
	};
	
	/**
	 * Set total page count$('.page-container-slide > ul > li.step-page',this.$widget).length
	 * @param {[type]} index [description]
	 */
	this.setPageCount= function(total) {
		this.settings.totalPages = total;
	};
	
	/**
	 * show/hide proceed button
	 * @param  {Boolean} isShow [description]
	 * @return {[type]}         [description]
	 */
	this.showProceedbtn= function(isShow) {
		var $proceedLink = this.$el.find('.proceed-link');
		if (isShow) {
			$proceedLink.removeClass('hidden');
		} else {
			$proceedLink.addClass('hidden');
		}
	};
	
	/**
	 * Add a page to the page slide
	 * @param page - the page to be added
	 */
	this.addPage= function(page) {
		var _self = this;
		this.$pageContainer.append($("<li class='step-page item swiper-slide'></li>").append(page));
		if( $('.page-container-slide > ul > li.step-page',this.$widget).length == 1 )
			$('.page-container-slide > ul > li.step-page',this.$widget).addClass('active');
		this.swipeInstance.init();
		this.setPageCount($('.page-container-slide > ul > li.step-page',this.$widget).length);
	};

	/**
	 * Return the index to the selected page
	 * @param index
	 */
	this.getActiveIndex= function() {
		return this.settings.activeIndex;
	};
	
	/**
	 * Return the number of pages.
	 * @returns number - the number of pages
	 */
	this.getPageCount= function() {
		return this.settings.totalPages;
	};
	
};
