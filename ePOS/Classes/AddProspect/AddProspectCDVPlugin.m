//
//  AddProspectCDVPlugin.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/9/15.
//
//

#import "AddProspectCDVPlugin.h"
#import "CountryDAO.h"
#import "CustomerDetailsDAO.h"


//country
//term

#define COUNTRY_KEY @"country"
#define TERM_KEY    @"term"

@implementation AddProspectCDVPlugin

+ (NSString *)pluginName {
    return @"ePos.widget.PICSPanelView";
}

- (NSArray *)countries {
    //retrieve Countries
    CountryDAO *countryDao = [CountryDAO sharedPersistenceStore];
    return [countryDao loadAllCountries];
}

- (void)retrievePICS:(CDVInvokedUrlCommand *)command {

    NSArray *countries = [self countries];

    //retrieve pics
    NSDictionary *PICS = [NSDictionary dictionaryWithObjectsAndKeys:countries, COUNTRY_KEY, @"PICS INFO\n As a user I have agreed on all terms and conditions stated on this document. User(the prospect or customer), Agent (the representative of the company). As an agent, all of the infor provided by the user will be private and be confidential. I also agree to the agent to use my data for any related insurance info it may be used", TERM_KEY, nil];
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:PICS] callbackId:command.callbackId];
    
}

- (void)onSavePICSInfo:(CDVInvokedUrlCommand *)command {
    
    if (command.arguments.count != 1) {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
        return;
    }

    NSLog(@"saving customer %@",command.arguments[0]);
    
    CustomerDetailsDAO *customerDAO = [CustomerDetailsDAO sharedPersistenceStore];
    CustomerDetails *createdCustomer = [customerDAO createCustomerDetails:command.arguments[0]];
    
    //navigate to customer profile
    if (self.delegate && [self.delegate respondsToSelector:@selector(addProspectCDVPlugin:prospectAdded:)]) {
        [self.delegate addProspectCDVPlugin:self prospectAdded:createdCustomer];
    }
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

@end
