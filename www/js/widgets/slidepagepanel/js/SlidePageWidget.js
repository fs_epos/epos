/**
 * SlidePageWidget, toggles the page
 * © Accenture ePOS 2015
 * @author Neilbrian Bernardio
 */

// define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $){
	'use strict';
	
	// The default constructor
	function SlidePageWidget(widgetReference, options){
		// used the current instace of widget if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $)? widgetReference: $('#' + widgetReference);
		
		if(ePos.util.getObjectValue(options, 'position', '')){
			options.position = options.position.toString().toLowerCase();
		}
		
		this.options = $.extend({}, SlidePageWidget.DEFAULTS, options);
		
		this._buildInitialDOM();
		this._bindEvents();
	}
	
	SlidePageWidget.DEFAULTS = {
			position: 'right',
			show: false,
			duration: 500,
			beforeShowPage: null,
			afterShowPage: null,
			beforeHidePage: null,
			afterHidePage: null
		};
	
	SlidePageWidget.prototype = {
		constructor: SlidePageWidget,
		options: null,
		$widget: null,
		
		_template: '<div class="slide-arrow outer"><span></span></div>'
					+ '<div class="slide-panel">'
						+ '<div class="slide-arrow inner"><span></span></div>'
						+ '<div class="content-widget scroll"></div>'
					+ '</div>',

		_buildInitialDOM: function(){
			this.$widget.append(this._template);
			this.$widget.css('position', 'relative');
			
			this.$slidePanel = $('.slide-panel', this.$widget);
			this.$content = $('.content-widget', this.$widget);
			this.$outerArrow = $('.slide-arrow.outer', this.$widget);
			this.$innerArrow = $('.slide-arrow.inner', this.$widget);
			
			this.$outerArrow.css(this.options.position, '0px');
			this.$slidePanel.hide();
			
			var oppositePosition = (this.options.position == 'right')? 'left': 'right';
			this.$innerArrow.css(oppositePosition, '0px');
			this.$content.css( 'margin-' + oppositePosition, this.$outerArrow.width() + 'px');
		},
		
		_bindEvents: function(){
			var _self = this;
			this.$outerArrow.on('click', function(){
				if(typeof _self.options.beforeShowPage == 'function'){
					_self.options.beforeShowPage();
				}
				$(this).hide();
				_self.$slidePanel.show('slide', {direction: _self.options.position}, _self.options.duration, function(){
					if(typeof _self.options.afterShowPage == 'function'){
						_self.options.afterShowPage();
					}
				});
			});
			
			this.$innerArrow.on('click', function(){
				if(typeof _self.options.beforeHidePage == 'function'){
					_self.options.beforeHidePage();
				}
				_self.$slidePanel.hide('slide', {direction: _self.options.position}, _self.options.duration, function(){
						_self.$outerArrow.show();
						if(typeof _self.options.afterHidePage == 'function'){
							_self.options.afterHidePage();
						}
				});
			});
		},
		
		appendContent: function(content){
			this.$content.append(content);
		},
		
		clearContent: function(){
			this.$content.html('');
		}
	};
	
	ePos.widget.SlidePageWidget = SlidePageWidget;
}(ePos, $));