/**
 * StepPagePane Widget translation entries
 */
core.l10n.Localization.prototype.addDictionary(
{
	en : {
		values : {
			clientprofile : {
				common : {
					util : {
						ui: {
							steppagination: {
								StepPagePanelView : {
									label: {
										step: "Step",
										out: " out of "
									}
								}
							}
						}
					}
				}
			}
		}
	},
	
	'zh_HK' : {
		values : {
			clientprofile : {
				common : {
					util : {
						ui: {
							steppagination: {
								StepPagePanelView : {
									label: {
										step: "步驟",
										out: "/"
									}
								}
							}
						}
					}
				}
			}
		}
	}
});