/**
 * DragAndDrop, provides a range of selection with plus minus button
 * © Accenture ePOS 2015
 * @author Neilbrian Bernardio
 */

// define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $){
	'use strict';
	
	// The default constructor
	function DragAndDrop(widgetReference, options, callback){
		// used the current element instace if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $)? widgetReference: $('#' + widgetReference);
		
		this.options = $.extend({}, DragAndDrop.DEFAULTS, options);
		
		this.callback = callback;
		
		// item status 
		this.isSelectionFull = false;
		this.isInstanceOfClone = false;
		
		// list of selected item
		this._selectedItems = [];
		this._selectionOptions = [];
		
		this._buildInitialDOM();
		this._initCallbacks();
		this._initDragDropElements();
		this._bindEvents();

		// execute the callback once all the components have been initialized
		if(typeof this.callback === 'function') this.callback();
	}
	
	// Define defaults
	DragAndDrop.DEFAULTS = {
			/**
			* [Sample]
			*	items: [{
			*		name: 'male',
			*		value: 'gender_male',
			*		icon: 'image_exact_path',
			*		dragImage: '<img src="path_to_image">'	// must be a valid DOM element
			*   },{
			*		name: 'female',
			*		value: 'gender_female',
			*		icon: 'image_exact_path'
			*		dragImage: '<img src="path_to_image">' // must be a valid DOM element
			*	}],
			**/
			items: null,
			title: '',
			dragCursor: { top: 0, left: 0 },
			multipleSelect: false, // false disables the item selection automatically if there is already an item present on the stage
			selectionUnique: false,	// if true, only one instance of the item in the stage is permissible
			maxItemSelect: 0,	// limits the number of dropped items on the stage, zero means unlimited
			dropCallback: null,
			removeItemCallback: null
		};
	
	// define class variables and methods
	DragAndDrop.prototype = {
		constructor: DragAndDrop,
		options: null,
		$widget: null,
		callback: null,
		
		// item status 
		isSelectionFull: null,
		isInstanceOfClone: null,
		
		// list of selected item
		_selectedItems: null,
		_selectionOptions: null,
		
		// the html output for this widget
		_template: '<section class="item-area noselect"></section>'
					+ '<section class="stage-area">' 
						+ '<span class="title"></span>'
						+ '<div class="selection-container"></div>' 
					+ '</section>',
		
		// template for the icon
		_itemTemplate: '<div class="item-container">'
						+ '<div class="item-icon"></div>'
						+ '<span class="item-name"></span>'
						+ '<div class="draggable-image"></div>'
					+ '</div>',
		
		/**
		* Inserts the template to the DOM and defines the necessary elements to be used
		*/
		_buildInitialDOM: function(){
			this.$widget.append(this._template);
			this.$widget.addClass('noselect');
			this.$itemArea = $('.item-area', this.$widget);
			this.$stageTitle = $('.stage-area .title', this.$widget);
			this.$selectedItemContainer = $('.selection-container', this.$widget);
			
			this.$stageTitle.html(this.options.title);
		},
		
		_initCallbacks: function(){
			var _self = this;
			if(this.options.dropCallback == null){
				this.options.dropCallback = function(e, obj, item){
					_self._defaultDropCallback(e, obj, item);
				}
			}
			
			if(this.options.removeItemCallback == null){
				this.options.removeItemCallback = function(e, obj, item){
					_self._defaultRemoveItemCallback(e, obj, item);
				}
			}
		},
		
		/**
		* Instantiate elements that would be used for drag and drop events
		*/
		_initDragDropElements: function(){
			var _self = this;
			
			// add the items on the top area
			if(this.options.items instanceof Array){
				$(this.options.items).each(function(indx, item){
					var $itemTemplate = $(_self._itemTemplate);
					_self._selectionOptions.unshift($itemTemplate);
					
					// attached the item value to the element in order to differentiat with other selections (e.g. elem-pair)
					$itemTemplate.data('itemValue', item.value);
					$('.item-name', $itemTemplate).text(item.name);
					$('.item-icon', $itemTemplate).css('background-image', 'url(' + item.icon + ')');
					$('.draggable-image', $itemTemplate).hide().append($(item.dragImage));
					// this will be paired to "elem-pair" sampled above
					$('.draggable-image', $itemTemplate).data('itemValue', item.value);
					
					$itemTemplate.draggable({
						cursor: "pointer", 
						cursorAt: _self.options.dragCursor, 
						helper: function(e, ui){
							var $draggableImage = $('.draggable-image', this).clone(true);
							$draggableImage.show();
							return $draggableImage;
						},
						stop: function(e, ui){
							_self.isValidDrop = false;
						}
					});
					_self.$itemArea.append($itemTemplate);
				});
			}
			
			// make this div into a stage area
			this.$selectedItemContainer.droppable({
				activeClass: "ui-state-active",
				hoverClass: "ui-state-hover",
				drop: function(e, ui){
					_self.isValidDrop = true;
					
					// don't clone the item if it is already a clone itself
					var $clone = (_self.isInstanceOfClone)? ui.helper : ui.helper.clone(true);
					$(this).append($clone);
					
					// save the dropped item and update the item list
					if(_self.isInstanceOfClone == false){
						_self._selectedItems.unshift($clone);
						_self._makeItemRemovable($clone);
					}
					
					_self._arrangeItems();
					
					_self._updateItemSelection();
					
					_self.options.dropCallback(e, _self, $clone);
				}
			});
			
		},
		
		/**
		* Arrange the items on the stage accordingly
		*/ 
		_arrangeItems: function(){
			var _self = this;
			var totalSelected = this._selectedItems.length;
			
			// check if there are selected items, if none, do nothing
			if(totalSelected <= 0) return;
			
			
			var $firstItem = this._selectedItems[0];
				
			// place the item on the center stage
			var position = this._findCenterLocation($firstItem);
			
			if(totalSelected > 1){
				$(this._selectedItems).each(function(indx, $item){
					var selectedWidth = $item.width();
					var displacement = position.x - (selectedWidth * (totalSelected - 1) / 2);
					displacement = (displacement > 0)? displacement: 0;
					
					var xPosition = displacement + (selectedWidth*indx);
					
					$item.animate({'top':position.y, 'left':xPosition}, 500, 'swing');
				});
			} else {
				$firstItem.animate({'top':position.y, 'left':position.x}, 500, 'swing');
			}
		},
		
		/**
		* Calculate where to position the dragged item
		* @params draggedItem (obj) the item that is being dragged
		* @return object, x and y coordinate of the center
		*/
		_findCenterLocation: function($draggedItem){
			var stageWidth = this.$selectedItemContainer.width()/2;
			var stageHeight = this.$selectedItemContainer.height()/2;
			var itemWidth = $draggedItem.width()/2;
			var itemHeight = $draggedItem.height()/2;
			
			// offset with regards to parent div
			var offsetTop = this.$selectedItemContainer.offset().top - this.$selectedItemContainer.offsetParent().offset().top;
			
			var calculatedX = stageWidth - itemWidth;
			var calculatedY = stageHeight - itemHeight + offsetTop;
			
			return {x:calculatedX, y:calculatedY}
		},
		
		
		
		/**
		* Make clone element removable from stage
		* @param elem (object) the element that can be removed
		*/
		_makeItemRemovable: function(elem){
			var _self = this;
			
			elem.draggable({
				start: function(){
					_self.isInstanceOfClone = true;
				},
				stop: function(e, ui){
					_self.isInstanceOfClone = false;
					
					// remove the item if dropped outside the stage area, remove also from the list
					if(_self.isValidDrop == false){
						elem.remove();
						_self._selectedItems.splice(_self._selectedItems.indexOf(elem),1);
						_self._arrangeItems();
						_self._updateItemSelection();
						_self.options.removeItemCallback(e, _self, elem);
					}
					// restore to default value
					_self.isValidDrop = false;
				}
			});
		},
		
		/**
		* Identify if the selection needs to be disabled or enabled depending on the defined settings
		*/
		_updateItemSelection: function(){
			var _self = this;
			
			// enable the selection first
			$(this._selectionOptions).each(function(indx, $selectOption){
				 $selectOption.draggable('enable').removeClass('disable-option');
			});
			
			if(this._selectedItems.length > 0){
				// if not satisfied, disable all items once a selection has been made
				if(this.options.multipleSelect == true){
					// if not satisfied, disable all items once the maximum permitted selection has been made
					if(this.options.maxItemSelect > 0 && this._selectedItems.length >= this.options.maxItemSelect){
						this._disableSelection();
					}
					
					// disable the currently selected item only
					if(this.options.selectionUnique == true){
						this._disableSelection(true);
					}
				} else {
					this._disableSelection();
				}
			}
		},
		
		/**
		* Disables all the items in the selection
		*/
		_disableSelection: function(isSpecific){
				var _self = this;
				$(this._selectionOptions).each(function(indx, $selectOption){
					var bool = false;
					var $selectOptionItem = $('.draggable-image',$selectOption);
					if(isSpecific){
						$(_self._selectedItems).each(function(indx2, $item){
							if($selectOptionItem.data('itemValue') == $item.data('itemValue')){
								bool = true;
								return false;
							}
						});
					}else{
						bool = true;
					}
					if(bool) $selectOption.draggable('disable').addClass('disable-option');
				});
		},
		
		/**
		* Define the events and listeners for the buttons and input
		*/
		_bindEvents: function(){
			var _self = this;
			
		},
		
		/**
		* If there are no user defined callback, this wil be used
		*/
		_defaultDropCallback: function(e, obj, item){
			console.log('item dropped!');
		},
		
		/**
		* If there are no user defined callback, this wil be used
		*/
		_defaultRemoveItemCallback: function(e, obj, item){
			console.log('item has been removed!');
		},
		
		getSelectedItems: function(){
			return this._selectedItems;
		}
		
	};
	
	// register the DragAndDrop object on ePos
	ePos.widget.DragAndDrop = DragAndDrop;
}(ePos, $));