//
//  MainMenuCell.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/3/15.
//
//

#import "MainMenuCell.h"

@implementation MainMenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
