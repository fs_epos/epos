/**
*	Dashboard, Resource Panel Event Listener mobile
*	Copyright Accenture Inc., ePOS 2015
*	@author Ralph Justin Raymundo
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var ResourcePanelEventListener = function() {
		this.options = $.extend({}, ResourcePanelEventListener.DEFAULTS);	
		this.PLUGIN_NAME = 'ePos.widget.ResourcePanelView';
	}
	//Define default attributes
	ResourcePanelEventListener.DEFAULTS = {
		initializeData: true
	}
	
	ResourcePanelEventListener.prototype = {
		constructor : ResourcePanelEventListener,
		onOpenResource: function(information) {
			var self = this;
    		// cordova call
    		cordova.exec(
				function(success){
					console.log('Resource Panel: Resource opened successfully.');
				},
				function(error){
					console.log('Resource Panel: Something went wrong during the opening.');
				},
				this.PLUGIN_NAME,
				'onOpenResource',
				(information ? [information] : [])
    		);
		}			
	}	
	//Register the object inside ePOS
	ePos.widget.ResourcePanelEventListener = ResourcePanelEventListener;
}(ePos, $));

