/**
* Navigational Overlay Widget
*/
navigationalOverlayWidget = function() {
	this.$widget = $('#navigationaloverlay');
	this.settings = {};	
	this._template = '<div class="navigation-modal"> </div>';
	
	this.init = function(listener, provider) {
		var defaults = {
			eventlistener: listener,
			dataprovider: provider,
			initializeData: true
		};
		
		this.settings = defaults;
		this._buildInitialDOM();
		
		if(this.settings.initializeData) {
			this.initializeData()
		}
	};

	this._buildInitialDOM = function() {
		this.$widget.append(this._template);
		this.$navigationoverlay = $('.navigation-modal', this.$widget);
	};
	
	this.initializeData = function() {
		if(this.settings.dataprovider && typeof this.settings.dataprovider.getLinks === 'function') {
			var _self = this;
			this.settings.dataprovider.getLinks( 
				function(item) {
					_self.loadData(item);
				}
			);
		}
	};
	
	/**
	 * @param {Array of Objects} links - Links to show on the navigational overlay
	 * @param {String} links.linkText - the text of the link to be shown in the navigational overlay
	 * @param {String} links.id - unique identifier of the link. To be used by the iOS backend to validate which page to redirect to
	 */
	this.loadData = function(obj) {
		if(obj !== null && obj !== undefined) {
			this.$widget.attr('title', obj.title);
			// this.$navigationoverlay.append($('<div class="navilink title" id="title"> '+obj.title+'  </div>'));
			if(obj.links !== null && obj !== undefined && obj.links.length > 0) {
				for(var ctr=0; ctr<obj.links.length; ctr++){
					this.$navigationoverlay.append($('<div class="navilink" id="'+ obj.links[ctr].id+'"> '+ obj.links[ctr].linkText+'  </div>'));
				}
			}
		} 
		this._attachEventListener();
	};
	
	this._attachEventListener = function() {
		var _self = this;
		this.$navilinks = $('.navilink', this.$widget);
		this.$navilinks.on('click',function(evt) {
			_self._navigate(this.id);
		});
	};
	
	this._navigate = function(page) {
		if(this.settings.eventlistener && typeof this.settings.eventlistener.onNavigate == 'function')
			this.settings.eventlistener.onNavigate(this, page);
	};
};