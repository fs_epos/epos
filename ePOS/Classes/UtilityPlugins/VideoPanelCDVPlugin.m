//
//  VideoPanelCDVPlugin.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import "VideoPanelCDVPlugin.h"

@implementation VideoPanelCDVPlugin

+ (NSString *)pluginName {
    return @"ePos.widget.VideoPanelView";
}

- (void)retrieveVideo:(CDVInvokedUrlCommand *)command {
    
}

- (void)onOpenVideo:(CDVInvokedUrlCommand *)command {
    NSLog(@"onOpenVideo");
    if (self.delegate && [self.delegate respondsToSelector:@selector(videoPanelCDVPluginDelegate:onOpenVideo:)]) {
        [self.delegate videoPanelCDVPluginDelegate:self onOpenVideo:@"video"];
    }
}


@end
