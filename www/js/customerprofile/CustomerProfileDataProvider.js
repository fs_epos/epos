/**
*	Contact Information, Contact Information Data Provider
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var CustomerProfileDataProvider = function() {
		this.options = $.extend({}, CustomerProfileDataProvider.DEFAULTS);
		this.defaults = {
		contactlInfo : { 
			customer: {
				"id" : '200',
				"mobile" : "09123456789",
				"email" : "john.doe@email.com",
				"country" : "101",
				"countrySelectedID": 101,
				"homeAddress": "I.T Park, Cebu City",
				"workAddress": "Libis Eastwood, Quezon City"
			},
			countries: [
						{
							"value": 101,
							"label": 'Philippines'
						},
						{
							"value": 102,
							"label": 'Italy'
						},
						{
							"value": 103,
							"label": 'Hong Kong'
						}
					]
							
			},
			personalInfo : {
				"id" : '200',
				"surname" : "Doe",
				"givenname" : "John",
				"gender": "MALE",
				"dateOfBirth": "1990-05-20T23:11:57.056-07:00",
				"age": 25,
			}	
		};
	}
	
	//Define default attributes
	CustomerProfileDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	CustomerProfileDataProvider.prototype = {
		constructor : CustomerProfileDataProvider,
		retrieveContactInfo: function(queryObject, successCallback, failureCallback){
			if(typeof successCallback == 'function'){
    			console.log("retrieveContactInfo execution")
    			successCallback(this.defaults.contactlInfo);
    		}
		},
		retrievePersonalInfo: function(queryObject, successCallback, failureCallback){
			if(typeof successCallback == 'function'){
    			console.log("retrievePersonalInfo execution")
    			successCallback(this.defaults.personalInfo);
    		}
		}	
	}
	//Register the object inside ePOS
	ePos.widget.CustomerProfileDataProvider = CustomerProfileDataProvider;
}(ePos, $));