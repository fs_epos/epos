//constructor

function Carousel(slidesData, options){
	this.slidesData = slidesData;
	this. options = options;
	
	this.carouselContainer = $('<div class="swiper-container"></div>');
	this.slidesContainer = $('<div class="swiper-wrapper"></div>');
	this.tabContainer = $('<div id="tabContainer"></div>');
	
}

//class methods
Carousel.prototype = {
	constructor: Carousel,
	buildSlides:function(){
		
		var _self = this;
		
		var slides = _self.slidesData;
		var slideOptions = _self.options;
		
		for (var s = 0; s < slides.length; s++) {
			var swiperSlide = $('<div class="swiper-slide"></div>');
			swiperSlide.append(slides[s]);
			_self.slidesContainer.append(swiperSlide);
		}
		
		_self.carouselContainer.append(_self.slidesContainer);
		
		var numOfSlidesPerView = slideOptions.slidesPerView;
		var loopSlides = slideOptions.loop;
		var loopAdditionalSlides = slideOptions.additionalLoopSlides;
		
		//initialize swiper library
		initializeSwiper(_self.carouselContainer, numOfSlidesPerView, loopSlides, loopAdditionalSlides)
		
		return $(_self.carouselContainer);
	}

}

 function initializeSwiper(swiperContainer, numOfSlidesPerView, loopSlides, loopAdditionalSlides){
	
	var _self = this;
	
	_self.swipeInstance = new Swiper (swiperContainer, {
		  pagination: '.swiper-pagination',
		  observer: true,
		  observeParents: true,
	      slidesPerView: numOfSlidesPerView,
	      loop: loopSlides,
	      loopAdditionalSlides: loopAdditionalSlides,
	      paginationHide: true,
	      paginationClickable: false,
	      spaceBetween: 30,
	      centeredSlides: true,
	      preventClicks: true,
	      calculateHeight: true,
	      preventClicksPropagation: true,
	      slideActiveClass: 'highlighted-policy',
	      slidePrevClass: 'previous-slideCard',
	      slideNextClass: 'next-slideCard'
	 });
	
	return _self.swipeInstance;

}



