/* GTKY Data Provider */

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';

	var GTKYDataProvider = function() {
		this.options = $.extend({}, GTKYDataProvider.DEFAULTS);
		this.defaults = {
			generalInfo : {
				"age": 25,							
				"customerId" : 200,
				"country": 101,
				"dateOfBirth": "1990-05-20T23:11:57.056-07:00",
				"children" : [
	                {
		                "dateOfBirth" : "2007-06-05T23:11:57.056-07:00",
		                "gender" : "FEMALE",
		                "givenname" : "Ritsu",
		                "surname" : "Wang"
	                },
	                {
		                "dateOfBirth" : "2007-06-05T23:11:57.056-07:00",
		                "gender" : "MALE",
		                "givenname" : "Pik",
		                "surname" : "Nik"
	                }
	            ],
				"email" : "john.doe@email.com",
				"gender": "MALE",
				"givenname" : "John",
				"homeAddress": "I.T Park, Cebu City",
				"mobile" : "09123456789",
	            "numberOfChildren": 2,
				"surname" : "Doe",
				"workAddress": "Libis Eastwood, Quezon City"
			},
			financialInfo: {
				defaults: {
					income: {
						max: 100000,
						min: 0
					},
					target: {
						max: 100000,
						min: 0
					},
					age : {
						min : 0,
						max : 99
						
					}
				},
				values : {
					occupation : "Professional",
					target : 100000,
					age : 25,
					targetage : 99,
					monthlyincome : 25000,
				}
			},
			
			country: [
				{
					"value": 101,
					"label": 'Philippines'
				},
				{
					"value": 102,
					"label": 'Italy'
				},
				{
					"value": 103,
					"label": 'Hong Kong'
				}
			],
		};
	}
	
	//Define default attributes
	GTKYDataProvider.DEFAULTS = {
		initializeData: true
	}
	
	GTKYDataProvider.prototype = {
		constructor : GTKYDataProvider,
		retrieveGeneralInfo: function(queryObject, successCallback, failureCallback){
			if(typeof successCallback == 'function'){
    			console.log("retrieveGeneralInfo execution")
    			successCallback(this.defaults);
    		}
		},
		getFinancialInfo: function(queryObject, successCallback, failureCallback){
			if(typeof successCallback == 'function'){
    			console.log("retrieveGeneralInfo execution")
    			successCallback(this.defaults.financialInfo);
    		}
		}	
	}
	//Register the object inside ePOS
	ePos.widget.GTKYDataProvider = GTKYDataProvider;
}(ePos, $));
