/**
*	Contact Information, Contact Information form for the Customer or Prospect
*	Copyright Accenture Inc., ePOS 2015
*	@author Ray Son
*/

//define namespace
var ePos = ePos || {};
ePos.widget = ePos.widget || {};

(function(ePos, $) {
	'use strict';
	function ContactInformationPanelView(widgetReference, options, callback) {
		//use the current element instance if defined, otherwise, get the object by id
		this.$widget = (widgetReference instanceof $) ? widgetReference : $('#' + widgetReference);
		this.dataprovider = new ePos.widget.CustomerProfileDataProvider();
		this.eventlistener = new ePos.widget.CustomerProfileEventListener();
		this.validation = new ePos.widget.ValidationUtility();
		this.options = $.extend({}, ContactInformationPanelView.DEFAULTS, options);
		this._contactInformation = [];
		this.callback = callback;
		this._buildInitialDOM();
	}
	
	//Define default attributes
	ContactInformationPanelView.DEFAULTS = {
		initializeData: true
	}
	
	ContactInformationPanelView.prototype = {
		constructor : ContactInformationPanelView,
		options: null,
		$widget: null,
		callback: null,

		//html template
		_template: 	'<div class="contact-information-form">'
				+'		<div class="contact-information-view">'
				+'			<div class="client-mobile clearfix"> <span class="client-mobile-label text-label"> </span> '
				+'			<span class="client-mobile-value text-value"> </span> </div>'
				+'			<div class="client-email clearfix"> <span class="client-email-label text-label"> </span> '
				+'			<span class="client-email-value text-value"> </span> </div>'
				+'			<div class="client-country clearfix"> <span class="client-country-label text-label"> </span> '
				+'			<span class="client-country-value text-value"> </span> </div>'
				+'			<div class="client-home-address clearfix"> <span class="client-home-address-label text-label"> </span> '
				+'			<span class="client-home-address-value text-value"> </span> </div>'
				+'			<div class="client-work-address clearfix"> <span class="client-work-address-label text-label"> </span> '
				+'			<span class="client-work-address-value text-value"> </span></div>'
				+'		</div>'
				+'		<div class="contact-information-edit">'
				+'			<div class="client-mobile clearfix"> <span class="client-mobile-label text-label"> </span> '
				+'			<span class="client-mobile-value text-value"> <input data-req="1" data-name="Mobile Number" type="tel" value="" /> </span> </div>'
				+'			<div class="client-email clearfix"> <span class="client-email-label text-label"> </span> '
				+'			<span class="client-email-value text-value"> <input data-req="1" data-name="Email" type="email" value="" /> </span> </div>'
				+'			<div class="client-country clearfix"> <span class="client-country-label text-label"> </span> '
				+'			<span class="client-country-value text-value"> <div id="client-country" class="dropdown"></div> </span> </div>'
				+'			<div class="client-home-address clearfix"> <span class="client-home-address-label text-label"> </span> '
				+'			<span class="client-home-address-value text-value"> <textarea type="textarea" data-req="1" data-name="Home Address"> </textarea></span> </div>'
				+'			<div class="client-work-address clearfix"> <span class="client-work-address-label text-label"> </span> '
				+'			<span class="client-work-address-value text-value"> <textarea type="textarea" data-req="0" data-name="Work Address"> </textarea> </span> </div>'
				+'		</div>'
				+'		<div class="contact-information-buttons">'
				+'			<span class="button-span button-edit"> <button id = "button-edit-contact-information" class="button positive" /></span>'
				+'			<span class="button-span button-save"> <button id = "button-save-contact-information" class="button positive" /></span>'
				+'			<span class="button-span button-cancel"> <button id = "button-cancel-contact-information" class="button positive" /></span>'
				+'		</div>'
				+'	</div>',
		
		_buildInitialDOM: function() {
			this.$widget.append(this._template);
			this._initializeLabels();
			this._initializeEvents();
			if(this.options.initializeData) 
				this.loadData();
		},
		
		loadData: function() {
			$('.contact-information-view', this.$widget).show();
			$('.contact-information-buttons .button-edit .button', this.$widget).show();
			$('.contact-information-edit', this.$widget).hide();
			$('.contact-information-buttons .button-save .button', this.$widget).hide();
			$('.contact-information-buttons .button-cancel .button', this.$widget).hide();
			this._loadContactInfo();
		},

		capitalizeString: function(string) {
        	return string.charAt(0).toUpperCase() + string.slice(1);
    	},

		_initializeLabels: function() {
			// Labels
			$('.client-mobile-label', this.$widget).text('Mobile: ');
			$('.client-email-label', this.$widget).text('Email: ');
			$('.client-country-label', this.$widget).text('Country: ');
			$('.client-home-address-label', this.$widget).text('Home Address: ');
			$('.client-work-address-label', this.$widget).text('Work Address: ');
			$('.button-edit .button', this.$widget).text('EDIT');
			$('.button-save .button', this.$widget).text('SAVE');
			$('.button-cancel .button', this.$widget).text('CANCEL');

			//Form Elements
			this.$mobile = $('.contact-information-edit .client-mobile-value input', this.$widget);
   			this.$email = $('.contact-information-edit .client-email-value input', this.$widget);
   			this.$country = $('.contact-information-edit .client-country-value input', this.$widget);
   			this.$homeAddr = $('.contact-information-edit .client-home-address-value textarea', this.$widget);
   			this.$workAddr = $('.contact-information-edit .client-work-address-value textarea', this.$widget);
   			
   			//Dropdown	
			this.dropDown = new dropDownWidget('client-country');

		}, 
		
		_initializeEvents: function() {
			var _self = this;
			$('.contact-information-buttons .button-edit .button', this.$widget).on('click', function(evt) {
				_self._transferToEditMode();
			});

			$('.contact-information-buttons .button-cancel .button', this.$widget).on('click', function(evt) {
				_self._transferToViewMode();
			});

			$('.contact-information-buttons .button-save .button', this.$widget).on('click', function(evt) {
				_self._saveContactInfo();
			});
		},
		
		_loadContactInfo: function(){
	    	var _self = this;
	    	this.dataprovider.retrieveContactInfo(null,
				function(contactinfo){
					_self._contactInformation = contactinfo.customer;
					_self.countries = _self._convertCountryPair(contactinfo.countries);
					_self.dropDown.init(contactinfo.countries, "Select");
					_self.dropDown.setSelectedByValue(contactinfo.customer.country);
					_self._loadContactInfoView(contactinfo.customer);
					_self._loadContactInfoEdit(contactinfo.customer);
		    	},
		    	function(){
		    		console.log('_loadContactInfo: something went wrong');
		    	}
	    	);
   		},

		_loadContactInfoView: function(data) {
   			$('.contact-information-view .client-mobile-value', this.$widget).text(data.mobile);
   			$('.contact-information-view .client-email-value', this.$widget).text(data.email);
   			$('.contact-information-view .client-country-value', this.$widget).text(this.dropDown.getSelected().label);
   			$('.contact-information-view .client-home-address-value', this.$widget).text(data.homeAddress);
   			$('.contact-information-view .client-work-address-value', this.$widget).text(data.workAddress);
   		},

   		_loadContactInfoEdit: function(data) {
   			this.$mobile.val(data.mobile);
   			this.$email.val(data.email);
   			this.dropDown.setSelectedByValue(this.dropDown.getSelected().value);
   			this.$homeAddr.val(data.homeAddress);
   			this.$workAddr.val(data.workAddress);
   		},
		
		_transferToViewMode: function() {
			$('.contact-information-edit', this.$widget).hide();
			$('.contact-information-buttons .button-edit .button', this.$widget).show();
			$('.contact-information-view', this.$widget).show();
			$('.contact-information-buttons .button-save .button', this.$widget).hide();
			$('.contact-information-buttons .button-cancel .button', this.$widget).hide();
		},

		_transferToEditMode: function() {
			$('.contact-information-edit', this.$widget).show();
			$('.contact-information-buttons .button-edit .button', this.$widget).hide();
			$('.contact-information-view', this.$widget).hide();
			$('.contact-information-buttons .button-save .button', this.$widget).show();
			$('.contact-information-buttons .button-cancel .button', this.$widget).show();
		},

		// transfer to EventListener
	    _saveContactInfo: function(){
	    	var validationResult = this.validation.validateInput(this, $('input, textarea', this.$widget));
	    	if(validationResult){
	    		$('.contact-information-form', this.$widget).find('.error').parent().find('input').removeClass('error-field');
	    		var newContactInfoData = {};
    				newContactInfoData = {
    				'customerId': this._contactInformation.customerId,
    				'mobile': this.$mobile.val().trim(),
    				'email': this.$email.val().trim(),
    				'country': this.dropDown.getSelected().value,
    				'homeAddress': this.$homeAddr.val(),
    				'workAddress': this.$workAddr.val()
    			};
    			this.eventlistener.onSubmitContactForm(newContactInfoData);

    			this._loadContactInfoView(newContactInfoData);
				this._loadContactInfoEdit(newContactInfoData);
				this._transferToViewMode();
	    	}else{
	    		console.log('_saveContactInfo: failed validation');
	    	}
	    },
	    
	    _convertCountryPair: function(array) {
	    	var converted = [];
	    	for(var ctr=0; ctr<array.length; ctr++) {
	    		converted[array[ctr].value] = array[ctr];
	    	}
	    	return converted;
	    }
	}
	//Register the object inside ePOS
	ePos.widget.ContactInformationPanelView = ContactInformationPanelView;
}(ePos, $));