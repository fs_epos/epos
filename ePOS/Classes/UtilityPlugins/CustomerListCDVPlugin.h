//
//  CustomerListCDVPlugin.h
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/10/15.
//
//

#import <Cordova/CDVPlugin.h>

@interface CustomerListCDVPlugin : CDVPlugin

+ (NSString *)pluginName;
- (void)getCustomerList:(CDVInvokedUrlCommand *)command;

@end
