//
//  CustomerListFlowLayout.m
//  ePOS
//
//  Created by Karen Roque on 11/3/15.
//
//

#import "CustomerListFlowLayout.h"

@implementation CustomerListFlowLayout

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {

    UICollectionViewLayoutAttributes *layoutAttributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    layoutAttributes.frame = CGRectMake(0,0,0,0);
    
    return layoutAttributes;

}


@end
