//
//  CustomerProfileCDVPlugin.m
//  ePOS
//
//  Created by Apuli, Alexis E. on 11/9/15.
//
//

#import "CustomerProfileCDVPlugin.h"
#import "CountryDAO.h"

@implementation CustomerProfileCDVPlugin

+ (NSString *)pluginName {
    return @"ePos.widget.CustomerProfilePanelView";
}


//retrieveCurrentCustomer
- (void)retrieveContactInfo:(CDVInvokedUrlCommand *)command {
    
    CustomerDetailsDAO *customerDAO = [CustomerDetailsDAO sharedPersistenceStore];
    NSDictionary *customer = [customerDAO retrieveCutomerWithCustomerId:_customerDetails.customerId];
    
    NSDictionary *contactInfo = [NSDictionary dictionaryWithObjectsAndKeys:customer, @"customer", [self countries], @"countries", nil];
    
    NSLog(@"sending customerdata :\n%@",contactInfo);
    
    if (customer) {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:contactInfo] callbackId:command.callbackId];
    } else {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
    }
}

//retrievePersonalInfo
- (void)retrievePersonalInfo:(CDVInvokedUrlCommand *)command {
    
    CustomerDetailsDAO *customerDAO = [CustomerDetailsDAO sharedPersistenceStore];
    NSDictionary *customer = [customerDAO retrieveCutomerWithCustomerId:_customerDetails.customerId];
    
    if (customer) {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:customer] callbackId:command.callbackId];
    } else {
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
    }
}

- (NSArray *)countries {
    //retrieve Countries
    CountryDAO *countryDao = [CountryDAO sharedPersistenceStore];
    return [countryDao loadAllCountries];
}

//update prospect
- (void)onSubmitPersonalForm:(CDVInvokedUrlCommand *)command {
    
    if (command.arguments.count != 1) {
        NSLog(@"onSubmitPersonalForm Invalid parameter");
        return;
    }
    
    NSDictionary *customer = command.arguments[0];
    NSLog(@"Received Customer Data %@",customer);

    CustomerDetailsDAO *customerDAO = [CustomerDetailsDAO sharedPersistenceStore];
    NSError *err = nil;
    
    CustomerDetails *customerDetails = [customerDAO updateCustomerDetails:customer error:err];
    
    if (!err) {
        NSLog(@"updated successfully %@",customerDetails);
//        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
    } else {
        NSLog(@"failed to update %@",err);
//        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
    }
}


//update prospect
- (void)onSubmitContactForm:(CDVInvokedUrlCommand *)command {
    
    if (command.arguments.count != 1) {
        NSLog(@"onSubmitContactForm Invalid parameter");
        return;
    }
    
    NSDictionary *customer = command.arguments[0];
    NSLog(@"Received Customer Data %@",customer);
    
    CustomerDetailsDAO *customerDAO = [CustomerDetailsDAO sharedPersistenceStore];
    NSError *err = nil;
    
    CustomerDetails *customerDetails = [customerDAO updateCustomerDetails:customer error:err];
    
    if (!err) {
        NSLog(@"updated successfully %@",customerDetails);
//        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
    } else {
        NSLog(@"failed to update %@",err);
//        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
    }
}

@end
