//
//  CustomerListViewController.h
//  ePOS
//
//  Created by Karen Roque on 11/3/15.
//
//

#import <UIKit/UIKit.h>

@interface CustomerListViewController : UIViewController
- (id)initWithTitle:(NSString *)title;
@end
