//
//  CountryDAO.m
//  ePOS
//
//  Created by Karen Roque on 11/4/15.
//
//

#import "CountryDAO.h"
#import "Country.h"
#import "ManagedObjectEncoder.h"

@implementation CountryDAO

+(instancetype) sharedPersistenceStore {
    
    static CountryDAO *sharedPersistenceStore;
    
    if (!sharedPersistenceStore) {
        sharedPersistenceStore = [[self alloc] init];
    }
    
    return sharedPersistenceStore;
}

-(AppDelegate*) appDelegate {
    return [[UIApplication sharedApplication] delegate];
}

-(NSEntityDescription*) entityDescription {
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Country" inManagedObjectContext:[[self appDelegate]managedObjectContext]];
    
    return entity;
}

-(Country*) getCountryWithCountryCode:(NSString*) countryCode {
    
    Country *countryDetails;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];

    NSPredicate *byCountryCode = [NSPredicate predicateWithFormat:@"countryCode = %@", countryCode];
    
    request.entity = [self entityDescription];
    request.predicate = byCountryCode;
    
    NSError *error = nil;
    
    NSArray *countries = [[[self appDelegate] managedObjectContext] executeFetchRequest:request error:&error];
    
    if (countries.count > 0) {
        countryDetails = [countries lastObject];
    }
    
    return countryDetails;
}
/**
 Fetch all countries @MO
 */
-(NSArray*) loadAllCountriesMO {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"countryName" ascending:YES];
    
    request.entity = [self entityDescription];
    request.sortDescriptors = @[sortDesc];
    
    NSError *error = nil;
    
    NSArray *countries = [[[self appDelegate] managedObjectContext] executeFetchRequest:request error:&error];
    
    if (countries.count > 0) {
        return countries;
    }
    
    return nil;
}

/**
 Fetch all countries @json
 */
-(NSArray*) loadAllCountries {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"countryName" ascending:YES];
    
    request.entity = [self entityDescription];
    request.sortDescriptors = @[sortDesc];
    
    NSError *error = nil;
    
    NSArray *countries = [[[self appDelegate] managedObjectContext] executeFetchRequest:request error:&error];
    
    if (countries.count > 0) {
        
        countries = [self encodeManagedObject:countries];
        
        return countries;
    }
    
    return nil;
}

/**
 Create countries
 */
-(void) createCountry:(NSDictionary*) countryDetails {
    NSError *error = nil;
    
    static int ctr = 0;
    
    Country *country = [self getCountryWithCountryCode:countryDetails[@"countryCode"]];
    
    if (!country) {
        Country *newCountry = [NSEntityDescription insertNewObjectForEntityForName:@"Country" inManagedObjectContext:[[self appDelegate] managedObjectContext]];
        
        [newCountry setCountryCode:countryDetails[@"countryCode"]];
        [newCountry setCountryName:countryDetails[@"countryName"]];
        [newCountry setValue:[NSString stringWithFormat:@"%d",ctr++]];
        [newCountry setLabel:countryDetails[@"countryName"]];
        
        
        [[[self appDelegate] managedObjectContext] save:&error];
        
        if (!error) {
            NSLog(@"Creation successful");
        }
    } else {
        NSLog(@"Country already exists in the database");
    }
    
}

/**
 For initial data loading upon app launch
 */
-(void) createInitialCountryList {
    NSMutableArray *countries = [[NSMutableArray alloc] initWithObjects:
                                 @{@"countryCode" : @"HK", @"countryName" : @"Hong Kong", @"value" : @"1", @"label" : @"Hong Kong"},
                                 @{@"countryCode" : @"JP", @"countryName" : @"Japan", @"value" : @"2", @"label" : @"Japan"},
                                 @{@"countryCode" : @"US", @"countryName" : @"United States", @"value" : @"3", @"label" : @"United States"},
                                 @{@"countryCode" : @"PH", @"countryName" : @"Philippines", @"value" : @"4", @"label" : @"Philippines"}, nil];
    
    for (NSDictionary *country in countries) {
        [self createCountry:country];
    }
}

#pragma mark - Encoder
/**
 To return an array of json
 */
-(NSArray*) encodeManagedObject:(NSArray*) objects {
    NSMutableArray *encodedObjects = [[NSMutableArray alloc] init];
    if (objects.count > 0) {
        for (Country *country in objects) {
            NSArray *keys = [[[self entityDescription] attributesByName] allKeys];
            NSDictionary *encodedObject = [country dictionaryWithValuesForKeys:keys];
            [encodedObjects addObject:encodedObject];
        }
    }
    return encodedObjects;
}

/**
 To return json
 */
-(NSDictionary*) encodeManagedObjectToJSon:(Country*) objects {
    
    NSArray *keys = [[[self entityDescription] attributesByName] allKeys];
    NSDictionary *encodedObject = [objects dictionaryWithValuesForKeys:keys];
    
    return encodedObject;
}


@end
